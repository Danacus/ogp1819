package jumpingalien.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.model.handler.AnimationHandler;
import jumpingalien.model.handler.MovementHandler;
import jumpingalien.model.shark.SharkAnimationHandler;
import jumpingalien.model.shark.SharkMovementHandler;
import jumpingalien.model.world.Collision;
import jumpingalien.util.Bounds;
import jumpingalien.util.Sprite;
import jumpingalien.model.shark.SharkCollisionHandler;
import jumpingalien.util.Vector;
import jumpingalien.util.exception.GameObjectException;


/**
 * A class for sharks
 *
 * @invar | hasHandler(SharkAnimationHandler.class)
 * @invar | hasHandler(SharkMovementHandler.class)
 * @invar | hasHandler(SharkCollisionHandler.class)
 *
 */
public class Shark extends GameObject {

    /**
     * Creates a new Shark
     *
     * @param pixelLeftX     The x pixel position
     * @param pixelBottomY   The y pixel position
     * @param sprites        The sprites
     *
     * @effect | super()
     * @effect | addHandler(new SharkAnimationHandler(this, sprites))
     * @effect | addHandler(new SharkMovementHandler(this, new Vector<>(pixelLeftX, pixelBottomY).pixelToActual()))
     * @effect | addHandler(new SharkCollisionHandler(this))
     * @effect | setHitPoints(100);
     * @effect | setOrientation(OrientationState.Front);
     */
    public Shark(int pixelLeftX, int pixelBottomY, Sprite... sprites) throws GameObjectException {
        super();
        addHandler(new SharkAnimationHandler(this, sprites));
        addHandler(new SharkMovementHandler(this, new Vector<>(pixelLeftX, pixelBottomY).pixelToActual()));
        addHandler(new SharkCollisionHandler(this));
        setHitPoints(100);
        setOrientation(OrientationState.Front);
    }

    /**
     * Returns the top bounds of the shark
     *
     * @return | result.getSize() == new Vector<>(animationHandler.getCurrentSprite().getWidth(), 1)
     *         | result.getPosition() == getHandler(MovementHandler.class).getPixelPosition()
     *         |                            .add(new Vector<>(0, animationHandler.getCurrentSprite().getHeight() - 1))
     */
    public Bounds getTopBounds() {
        AnimationHandler animationHandler = getHandler(AnimationHandler.class);
        return new Bounds(
                getHandler(MovementHandler.class).getPixelPosition().add(new Vector<>(0, animationHandler.getCurrentSprite().getHeight() - 1)),
                new Vector<>(animationHandler.getCurrentSprite().getWidth(), 1)
        );
    }

    @Override
    public void update(double dt){
        if(getOrientation() == OrientationState.Front) setOrientation(OrientationState.Left);
        super.update(dt);
    }

    /**
     * @return | result == Collision.Category.SHARK
     */
    @Basic
    @Immutable
    @Override
    public byte getCategory() {
        return Collision.Category.SHARK;
    }

    /**
     * @return | result == Collision.Mask.SHARK
     */
    @Basic
    @Immutable
    @Override
    public byte getMask() {
        return Collision.Mask.SHARK;
    }

    /**
     * @return | result == true
     */
    @Override
    public boolean isSolid() {
        return true;
    }
}
