package jumpingalien.model.plant;

import jumpingalien.model.Plant;
import jumpingalien.util.exception.IllegalPositionException;
import jumpingalien.model.handler.KinematicMovementHandler;
import jumpingalien.util.Vector;

/**
 * A class for the movement of plants
 *
 * @invar | getGameObject() instanceof Plant
 */
public abstract class PlantMovementHandler extends KinematicMovementHandler {

    /**
     * Creates a new Movement Handler for Plants
     *
     * @param plant the plant for this PlantMovementHandler.
     * @param position the position of the plant for this PlantMovementHandler.
     *
     * @effect | super(plant)
     * @effect | setPosition(position)
     * @effect | setVelocity(Vector.ACTUAL_ZERO)
     * @effect | setAcceleration(Vector.ACTUAL_ZERO)
     */
    public PlantMovementHandler(Plant plant, Vector<Double> position) throws IllegalPositionException {
        super(plant);
        setPosition(position);
        setVelocity(Vector.ACTUAL_ZERO);
        setAcceleration(Vector.ACTUAL_ZERO);
    }

    /**
     * Variable used to count the time from the previous orientation switch
     */
    private double directionSwitchCount = 0;

    /**
     * Updates the movement of the plant
     *
     * @param dt The time that has passed since the last update.
     */
    @Override
    public void pixelUpdate(double dt) {
        if (directionSwitchCount + dt >= 0.5) {
            double firstdt = 0.5 - directionSwitchCount;
            super.pixelUpdate(firstdt);
            switchOrientation();
            dt -= firstdt;
            directionSwitchCount = dt;
        } else {
            directionSwitchCount += dt;
        }

        super.pixelUpdate(dt);
    }

    /**
     * Called when the orientation must switch
     */
    public void switchOrientation() {
        getGameObject().setOrientation(getGameObject().getOrientation().inverted());
    }

}
