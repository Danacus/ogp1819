package jumpingalien.model.plant;

import jumpingalien.model.Plant;
import jumpingalien.model.Skullcab;
import jumpingalien.model.Sneezewort;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.gameobject.ICollidable;
import jumpingalien.model.handler.CollisionHandler;
import jumpingalien.model.Mazub;

/**
 * A handler for handling the collision of plants
 */
public class PlantCollisionHandler extends CollisionHandler {
    /**
     * Creates a new PlantCollisionHandler for the given plant
     *
     * @param plant  The plant
     *
     * @effect | super(plant)
     */
    public PlantCollisionHandler(Plant plant) {
        super(plant);
    }

    /**
     * Event when the plant is colliding with another ICollidable object
     *
     * @param other The other collidable
     *
     * @effect | if (canInteract() && other instanceof Mazub) then
     *         |    startInteraction()
     *         |    getGameObject().takeDamage(1)
     *         |    if (getGameObject().getHitPoint() <= 0) then
     *         |        getGameObject().terminate()
     */
    @Override
    public void onCollision(ICollidable other) {
        if (!canInteract())
            return;
        if (other instanceof Mazub) {
            startInteraction();
            getGameObject().takeDamage(1);
            if (getGameObject().getHitPoints() <= 0)
                getGameObject().terminate();
        }
    }

    /**
     * Event when the plant enters collision with another ICollidable object
     *
     * @param other The other collidable
     *
     * @effect | if (other instanceof Mazub) then
     *         |    allowInteraction()
     */
    @Override
    public void onCollisionEnter(ICollidable other) {
        if (other instanceof Mazub) {
            allowInteraction();
        }
    }

    @Override
    public void onCollisionExit(ICollidable other) {

    }
}
