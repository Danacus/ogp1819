package jumpingalien.model.plant;

import jumpingalien.model.Plant;
import jumpingalien.util.exception.IllegalPositionException;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.util.Vector;

/**
 * A class for plants moving horizontally
 */
public class HorizontalPlantMovementHandler extends PlantMovementHandler {
    /**
     * @param plant    the plant for this PlantMovementHandler.
     * @param position the position of the plant for this PlantMovementHandler.
     * @param speed    the speed of the plant
     *
     * @effect | super(plant, position, speed)
     * @post | new.getVelocity() == new Vector<>(-speed, 0.0)
     * @effect | getGameObject().getHandler(AnimationHandler.class).setOrientationState(OrientationState.Left)
     */
    public HorizontalPlantMovementHandler(Plant plant, Vector<Double> position, double speed) throws IllegalPositionException {
        super(plant, position);
        setVelocity(new Vector<>(-speed, 0.0));
        getGameObject().setOrientation(OrientationState.Left);
    }

    /**
     * Switches the orientation of the plant
     *
     * @effect | super.switchOrientation()
     * @effect | setVelocity(getVelocity().setX(getVelocity().getX() * -1));
     */
    @Override
    public void switchOrientation() {
        super.switchOrientation();
        setVelocity(getVelocity().setX(getVelocity().getX() * -1));
    }
}
