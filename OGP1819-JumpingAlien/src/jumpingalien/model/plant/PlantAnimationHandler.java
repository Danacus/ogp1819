package jumpingalien.model.plant;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.Plant;
import jumpingalien.util.exception.InvalidSpriteException;
import jumpingalien.model.handler.AnimationHandler;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.util.Sprite;

/**
 * A class that handles the sprites of plants
 *
 * @invar | getGameObject() instanceof Plant
 */
public class PlantAnimationHandler extends AnimationHandler {
    /**
     * Creates a new animation handler for the given plant
     *
     * @param plant   The plant
     * @param sprites The sprites for the plant
     *
     * @effect | super(plant, sprites)
     */
    public PlantAnimationHandler(Plant plant, Sprite[] sprites) throws InvalidSpriteException {
        super(plant, sprites);
    }

    /**
     * Returns the current sprite for the plant
     *
     * @return | if (getGameObject().getOrientation() == OrientationState.Left || getGameObject().getOrientation() == OrientationState.Top) then
     *         |    result == getSprites()[0]
     *         | else
     *         |    result == getSprite()[1]
     */
    @Basic
    @Override
    public Sprite getCurrentSprite() {
        if(getGameObject().getOrientation() == OrientationState.Left || getGameObject().getOrientation() == OrientationState.Top)
            return getSprites()[0];
        return getSprites()[1];
    }

    /**
     * Checks whether this plant can have the given sprites as its sprites
     *
     * @param  sprites The sprites to check
     *
     * @return | super.canHaveAsSprites(sprites) && sprites.length == 2
     */
    @Override
    public boolean canHaveAsSprites(Sprite[] sprites) {
        return super.canHaveAsSprites(sprites) && sprites.length == 2;
    }
}
