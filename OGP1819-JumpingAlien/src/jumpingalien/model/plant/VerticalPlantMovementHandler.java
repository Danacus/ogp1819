package jumpingalien.model.plant;

import jumpingalien.model.Plant;
import jumpingalien.util.exception.IllegalPositionException;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.util.Vector;

/**
 * A class for plants moving vertically
 */
public class VerticalPlantMovementHandler extends PlantMovementHandler {
    /**
     * @param plant    the plant for this PlantMovementHandler.
     * @param position the position of the plant for this PlantMovementHandler.
     * @param speed    the speed of the plant
     *
     * @effect | super(plant, position, speed)
     * @post | new.getVelocity() == new Vector<>(0.0, speed)
     * @effect | getGameObject().getHandler(AnimationHandler.class).setOrientationState(OrientationState.Left)
     */
    public VerticalPlantMovementHandler(Plant plant, Vector<Double> position, double speed) throws IllegalPositionException {
        super(plant, position);
        setVelocity(new Vector<>(0.0, speed));
        getGameObject().setOrientation(OrientationState.Top);
    }

    /**
     * Switches the orientation of the plant
     *
     * @effect | super.switchOrientation()
     * @effect | setVelocity(getVelocity().setY(getVelocity().getY() * -1));
     */
    @Override
    public void switchOrientation() {
        super.switchOrientation();
        setVelocity(getVelocity().setY(getVelocity().getY() * -1));
    }
}
