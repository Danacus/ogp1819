package jumpingalien.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.util.exception.GameObjectException;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.model.handler.WorldCollisionHandler;
import jumpingalien.model.slime.SlimeAnimationHandler;
import jumpingalien.model.slime.SlimeCollisionHandler;
import jumpingalien.model.slime.SlimeMovementHandler;
import jumpingalien.model.world.*;
import jumpingalien.util.Sprite;
import jumpingalien.util.Vector;

/**
 * A class for slimes
 *
 * @invar | hasHandler(SlimeMovementHandler.class)
 * @invar | hasHandler(SlimeAnimationHandler.class)
 * @invar | hasHandler(SlimeCollisionHandler.class)
 */
public class Slime extends GameObject {
    /**
     * Creates a new slime
     *
     * @param id            The id of the slime
     * @param pixelLeftX    The x position
     * @param pixelBottomY  The y position
     * @param school        The school of the slime
     * @param sprites       The sprites for the slime
     *
     * @post | new.getId() == id
     * @effect | SchoolManager.registerSlimeId(id)
     *
     * @effect | addHandler(new SlimeMovementHandler(this, new Vector<>(pixelLeftX,pixelBottomY).pixelToActual()))
     * @effect | addHandler(new SlimeAnimationHandler(this, sprites))
     * @effect | addHandler(new SlimeCollisionHandler(this))
     *
     * @post | new.getHitPoints() == 100
     * @post | new.getOrientation() == OrientationState.Right
     *
     * @effect
     *       | getHandler(WorldCollisionHandler.class).setEffect(GeologicalFeature.WATER, new DamageEffect(this, 0.4, false, 4))
     *       | getHandler(WorldCollisionHandler.class).setEffect(GeologicalFeature.GAS, new DamageEffect(this, 0.3, false, -2))
     *
     * @post | if (school != null) then school.addMember(this)
     *
     * @throws IllegalArgumentException
     *       | !SchoolManager.isValidId(id)
     */
    public Slime(long id, int pixelLeftX, int pixelBottomY, School school, Sprite... sprites) throws GameObjectException {
        this.id = id;

        if (!SchoolManager.isValidId(id)) throw new GameObjectException("Not a valid id!");
        SchoolManager.registerSlimeId(id);

        addHandler(new SlimeMovementHandler(this, new Vector<>(pixelLeftX,pixelBottomY).pixelToActual()));
        addHandler(new SlimeAnimationHandler(this, sprites));
        addHandler(new SlimeCollisionHandler(this));

        setHitPoints(100);
        setOrientation(OrientationState.Right);

        WorldCollisionHandler worldCollisionHandler = getHandler(WorldCollisionHandler.class);
        worldCollisionHandler.setEffect(GeologicalFeature.WATER, new DamageEffect(this, 0.4, false, 4));
        worldCollisionHandler.setEffect(GeologicalFeature.GAS, new DamageEffect(this, 0.3, false, -2));

        if (school != null)
            school.addMember(this);
    }

    /**
     * Terminates the slime
     *
     * @effect | super.terminate()
     *
     * @effect | if (getSchool() != null) then removeMember(this)
     */
    @Override
    public void terminate() {
        super.terminate();
        if (getSchool() != null)
            getSchool().removeMember(this);
    }

    /**
     * Switches the school of this Slime
     *
     * @param newSchool  The new school for the slime
     *
     * @effect
     *         | for each member in getSchool().getMembers() \ { this }:
     *         |     member.setHitPoints(member.getHitPoints() + 1)
     *         | for each member in newSchool.getMembers():
     *         |     member.setHitPoints(member.getHitPoints() - 1)
     *         | takeDamage(getSchool.getMembers().size() - 1 - newSchool.getMembers().size()
     *
     * @effect | getSchool().removeMember(this)
     *         | newSchool.addMember(this)
     *
     * @throws GameObjectException
     *      | getSchool() == null
     */
    public void switchSchool(School newSchool) throws GameObjectException {
        if (newSchool == null)
            throw new IllegalArgumentException("Invalid school");
        if (getSchool() == null)
            throw new GameObjectException("Slime not in a school");
        School oldSchool = getSchool();
        getSchool().removeMember(this);

        oldSchool.getMembers().stream().forEach(s->s.setHitPoints(s.getHitPoints() + 1));
        newSchool.getMembers().stream().forEach(s->s.setHitPoints(s.getHitPoints() - 1));

        takeDamage(oldSchool.getMembers().size() - newSchool.getMembers().size());
        newSchool.addMember(this);
    }

    /**
     * Make the slime take damage
     *
     * @param damage an integer containing the damage.
     *
     * @effect | super.takeDamage(damage)
     *
     * @effect | if (getSchool() != null) then
     *         |    for each member in getSchool().getMembers() \ { this }:
     *         |        member.setHitPoints(member.getHitPoints() - 1)
     */
    @Override
    public void takeDamage(int damage) {
        super.takeDamage(damage);

        if (getSchool() != null)
            getSchool().getMembers().stream().filter(s->s != this).forEach(s->s.setHitPoints(s.getHitPoints() - 1));
    }

    /**
     * Returns the id of this slime
     */
    @Basic
    @Immutable
    public long getId() {
        return this.id;
    }

    /**
     * Returns the school of this slime
     */
    @Basic
    public School getSchool() {
        return this.school;
    }

    /**
     * Sets the school of this slime
     *
     * @param school The new school
     *
     * @post | getSchool() == school
     */
    public void setSchool(School school) throws IllegalArgumentException {
        if (!canHaveAsSchool(school))
            throw new IllegalArgumentException("Invalid school for this slime");
        this.school = school;
    }

    /**
     * Checks whether this slime can have the given school as its school
     *
     * @param school  The school to check
     *
     * @return | school == null || school.hasMember(this)
     */
    public boolean canHaveAsSchool(School school) {
        return school == null || school.hasMember(this);
    }

    /**
     * Variable storing the school of this slime
     */
    private School school;

    /**
     * Variable registering the id of this slime
     */
    private final long id;

    /**
     * @return | result == Collision.Category.SLIME
     */
    @Basic
    @Immutable
    @Override
    public byte getCategory() {
        return Collision.Category.SLIME;
    }

    /**
     * @return | result == Collision,Mask.SLIME
     */
    @Basic
    @Immutable
    @Override
    public byte getMask() {
        return Collision.Mask.SLIME;
    }

    /**
     * @return | result == true
     */
    @Override
    public boolean isSolid() {
        return true;
    }
}
