package jumpingalien.model;

import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.model.plant.HorizontalPlantMovementHandler;
import jumpingalien.model.plant.PlantMovementHandler;
import jumpingalien.util.Sprite;
import jumpingalien.util.Vector;
import jumpingalien.util.exception.GameObjectException;

import java.util.Arrays;

/**
 * A class for Sneezeworts
 *
 * @invar | hasHandler(HorizontalPlantMovementHandler.class)
 */
public class Sneezewort extends Plant {
    /**
     * Creates an new Sneezewort
     *
     * @param pixelLeftX x pixel coordinate for the new plant.
     * @param pixelBottomY y pixel coordinate fot he new plant.
     * @param sprites sprite array for the new plant.
     *
     * @effect | super(sprites, 10)
     * @effect | addHandler(new HorizontalPlantMovementHandler(this, new Vector<>(pixelLeftX, pixelBottomY).pixelToActual(), 0.5));
     * @post | new.getHitPoint() == 1
     */
    public Sneezewort(int pixelLeftX, int pixelBottomY, Sprite[] sprites) throws GameObjectException {
        super(sprites, 10);
        addHandler(new HorizontalPlantMovementHandler(this, new Vector<>(pixelLeftX, pixelBottomY).pixelToActual(), 0.5));
        setHitPoints(1);
    }

    /**
     * Checks whether this Sneezewort can have the given orientation as its orientation
     *
     * @param orientation The orientation to check
     *
     * @return | orientation.isHorizontal()
     */
    @Override
    public boolean canHaveAsOrientation(OrientationState orientation) {
        return orientation.isHorizontal();
    }
}
