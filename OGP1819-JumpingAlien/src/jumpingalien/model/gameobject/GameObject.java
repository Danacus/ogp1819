package jumpingalien.model.gameobject;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.util.*;
import jumpingalien.util.exception.GameObjectException;
import jumpingalien.model.handler.AnimationHandler;
import jumpingalien.model.handler.CollisionHandler;
import jumpingalien.model.handler.Handler;
import jumpingalien.model.Mazub;
import jumpingalien.model.World;
import jumpingalien.model.handler.MovementHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * A class for GameObjects, with a ClassTreeMAp of Handlers, a reference to its World,
 * a complete implementation of ICollidable, hit points and automatic handler updating for
 * both update and pixelUpdate.
 *
 * @invar This GameObject can have its handlers as its handlers
 *      | for each handler in getHandlers():
 *      |   canHaveAsHandler(handler)
 * @invar This GameObject can have its world as its world
 *      | canHaveAsWorld(getWorld())
 * @invar The hit points of this GameObject will always be below or equal to the maximum amount of hit points
 *      | getHitPoints() <= getMaxHitPoints()
 * @invar Each GameObject can have its orientation as its orientation
 *      | canHaveAsOrientation(getOrientation())
 *
 * @invar This GameObject has a MovementHandler
 *      | hasHandler(MovementHandler.class)
 * @invar This GameObject has an AnimationHandler
 *      | hasHandler(AnimationHandler.class)
 */
@SuppressWarnings("unchecked")
public abstract class GameObject implements ICollidable {
    public GameObject() throws GameObjectException {}

    /**
     * Adds a Handler to the Map
     *
     * @param handler The handler to be added to the map
     * @param <T> The type of the given handler, must be a class type that extends Handler
     *
     * @post The handler will be added
     *      | new.getHandler(handler.getClass()) == handler
     */
    protected  <T extends Handler> void addHandler(T handler) throws IllegalArgumentException {
        if (!canHaveAsHandler(handler))
            throw new IllegalArgumentException("Invalid handler for this GameObject!");
        handlers.add(handler);
    }

    /**
     * Checks whether this GameObject can have the given handler as one of its handlers
     *
     * @param handler The given handler
     * @param <T> The type of the given handler
     *
     * @return True if and only if the GameObject of the given Handler is equal to this
     */
    @Raw
    public <T extends Handler> boolean canHaveAsHandler(T handler) {
        return handler.getGameObject() == this;
    }

    /**
     * Gets the handler of a given class name
     *
     * @param handlerClass The class name of the Handler you would like to get
     * @param <T> The type of the Handler to be returned (will be used for casting)
     *
     * @return The handler for the given class name. If the dynamic type of the found object is
     *         incompatible with the requested type, null shall be returned.
     */
    @Basic
    public <T extends Handler> T getHandler(Class<T> handlerClass) {
        try {
            return (T) handlers.get(handlerClass);
        } catch (ClassCastException e) {
            return null;
        }
    }

    /**
     * Checks whether there is a Handler with the given class name as type in the Map.
     *
     * @param handlerClass The class name of the Handler
     */
    @Basic
    public boolean hasHandler(Class<? extends Handler> handlerClass) {
        return handlers.containsKey(handlerClass);
    }

    /**
     * Returns a map with all Handlers of this GameObject
     */
    @Basic
    public Map<Class<? extends Handler>, Handler> getHandlers() {
        return new HashMap<>(handlers);
    }

    /**
     * A ClassTreeMap to store the handlers for this GameObject
     */
    private ClassTreeMap<Handler> handlers = new ClassTreeMap<>(Handler.class);

    /**
     * Gets the world in which this GameObject lives
     */
    @Basic
    public World getWorld() {
        return this.world;
    }

    /**
     * Sets the world for this GameObject
     *
     * @param world The world for this GameObject
     *
     * @post This world will be equal to the given World
     *     | new.getWorld() == world
     *
     * @throws IllegalArgumentException
     *     | !canHaveAsWorld(world)
     */
    public void setWorld(World world) throws IllegalArgumentException {
        if (!canHaveAsWorld(world))
            throw new IllegalArgumentException("The given world must have this GameObject as one of its GameObjects");

        this.world = world;
    }

    /**
     * Checks whether this GameObject can have the given world as its world
     *
     * @param world The world to check
     *
     * @return True if and only if the given world in not null and the world has this GameObject as one of
     *         its GameObjects, unless the current world is already null. In that case setting it to null
     *         again is allowed.
     *        | world != null && world.hasAsGameObject(this) || world == null && getWorld() != null
     */
    @Raw
    public boolean canHaveAsWorld(World world) {
        return world != null && world.hasAsGameObject(this) || world == null && getWorld() != null;
    }

    /**
     * A variable to store a reference to the world in which this GameObject lives
     */
    private World world;

    /**
     * The main time advancement loop that runs on irregular time step dt
     *
     * Will invoke update on each Handler of this GameObject
     *
     * @param dt The time to advance
     *
     * @throws IllegalArgumentException
     *        The given dt is not a double between 0 and 0.2
     *      | (dt <= 0 || dt > 0.2 || Double.isNaN(dt))
     */
    public void update(double dt) throws IllegalArgumentException {
        if (dt <= 0 || dt > 0.2 || Double.isNaN(dt))
            throw new IllegalArgumentException("Illegal time was given to update");
        if (isTerminated()) return;

        if (isDead()) {
            removeTimer -= dt;
            if (removeTimer <= 0)
                terminate();
        }

        double pixelDt = dt;
        double step = 0.01;

        if (hasHandler(MovementHandler.class)) {
            MovementHandler movementHandler = getHandler(MovementHandler.class);
            step = 0.01 / (movementHandler.getVelocity().magnitude() + movementHandler.getAcceleration().magnitude() * dt);
        }

        while (pixelDt >= step) {
            pixelDt -= step;

            pixelUpdate(step);
        }

        pixelUpdate(pixelDt);

        for (Handler handler : handlers.values()) {
            handler.update(dt);
        }
    }

    /**
     * The pixel update loop
     * Is usually invoked with a very small time step, to ensure
     * proper collision handling. The GameObject should only move
     * one pixel in the given dt.
     *
     * Will invoke pixelUpdate on each Handler of this GameObject
     *
     * @param dt The time to advance
     */
    public void pixelUpdate(double dt) {
        for (Handler handler : handlers.values()) {
            handler.pixelUpdate(dt);
        }
    }

    /**
     * Terminate this GameObject.
     *
     * @post This GameObject is terminated.
     *     | new.isTerminated()
     *     | new.isDead()
     * @effect If the world is not null, this GameObject will remove itself from the Set in world.
     *     | if (getWorld() != null) then
     *     |    getWorld().removeGameObject(this)
     */
    public void terminate() {
        this.isTerminated = true;
        this.isDead = true;
        if (getWorld() != null)
            getWorld().removeGameObject(this);
    }

    /**
     * Return a boolean indicating whether or not this GameObject
     * is terminated.
     */
    @Basic
    @Raw
    public boolean isTerminated() {
        return this.isTerminated;
    }

    /**
     * Variable registering whether this GameObject is terminated.
     */
    private boolean isTerminated = false;

    /**
     * Kill this GameObject.
     *
     * @post   This GameObject is dead.
     *       | new.isDead()
     */
    public void kill() {
        this.isDead = true;
    }

    /**
     * Return a boolean indicating whether or not this GameObject
     * is dead.
     */
    @Basic
    @Raw
    public boolean isDead() {
        return this.isDead;
    }

    /**
     * Variable registering whether this GameObject is dead.
     */
    private boolean isDead = false;

    /**
     * Returns the remove timer for this GameObject
     */
    public double getRemoveTimer() {
        return removeTimer;
    }

    /**
     * A number used to count down when this GameObject dies before it terminates itself.
     */
    private double removeTimer = 0.6;

    /**
     * Gets the hit points of this GameObject
     */
    @Basic
    @Raw
    public int getHitPoints() {
        return hitPoints;
    }

    /**
     * Sets the hit points of this GameObject
     *
     * @param hitPoints The given amount of hit points
     *
     * @post | new.hitPoints == Math.min(getHitPoints() + hitPoints, getMaximumHitPoints());
     * @effect | if (hitPoints <= 0) then this.terminate()
     */
    public void setHitPoints(int hitPoints) {
        this.hitPoints = Math.min(hitPoints, getMaximumHitPoints());
        if (this.hitPoints <= 0){
            if(this instanceof Mazub){
                if (getWorld() != null)
                    getWorld().endGame();
            }
            kill();
        }
    }

    /**
     * A variable registering the amount of hit points.
     */
    private int hitPoints;

    /**
     * Returns the maximum amount of hit points this GameObject can have
     */
    @Basic
    public int getMaximumHitPoints() {
        return maximumHitPoints;
    }

    /**
     * Checks whether this GameObject can have the given maximum hit points as its maximum hit points.
     *
     * @param maximumHitPoints The maximum hit points to check
     *
     * @return True if and only of the given maximum hit points is greater than 0
     *       | result == maximumHitPoints > 0
     */
    @Raw
    public boolean canHaveAsMaximumHitPoints(double maximumHitPoints) {
        return maximumHitPoints > 0;
    }

    /**
     * Sets the maximum hit points
     *
     * @param maximumHitPoints The new maximum hit points
     *
     * @post If this GameObject can have the given maximum hit points as its maximum hit points, the new
     *       maximum hit points will be equal to the given maximum hit points, otherwise it will be
     *       equal to the maximum integer value.
     *     | if (canHaveAsMaximumHitPoints(maximumHitPoints)) then
     *     |    new.getMaximumHitPoints() == maximumHitPoints
     *     | else
     *     |    new.getMaximumHitPoints() == Integer.MAX_VALUE
     * @post If the current hit points are greater than the new maximum hit points, the hit points
     *       will be set to the maximum hit points.
     *     | if (getHitPoints() > new.getMaximumHitPoints()) then
     *     |    new.getHitPoints() == new.getMaximumHitPoints()
     */
    protected void setMaximumHitPoints(int maximumHitPoints) {
        if (!canHaveAsMaximumHitPoints(maximumHitPoints))
            this.maximumHitPoints = Integer.MAX_VALUE;
        this.maximumHitPoints = maximumHitPoints;
        if (getHitPoints() > getMaximumHitPoints())
            setHitPoints(getMaximumHitPoints());
    }

    private int maximumHitPoints = Integer.MAX_VALUE;

    /**
     * Returns the bounds of this GameObject
     *
     * @return | result == getBounds(getHandler(MovementHandler.class).getPosition().actualToPixel())
     */
    public Bounds getBounds(){
        return getBounds(getHandler(MovementHandler.class).getPosition().actualToPixel());
    }

    /**
     * Returns the bounds of this GameObject
     *
     * @param pixelPos A vector containing a pixel position.
     * @return | result == getBounds(pixelPos, getHandler(AnimationHandler.class).getCurrentSprite())
     */
    public Bounds getBounds(Vector<Integer> pixelPos){
        return getBounds(pixelPos, getHandler(AnimationHandler.class).getCurrentSprite());
    }

    /**
     * Returns the bounds of this GameObject
     *
     * @param sprite A sprite.
     * @return | getBounds(getHandler(MovementHandler.class).getPixelPosition(), sprite);
     */
    public Bounds getBounds(Sprite sprite) {
        return getBounds(getHandler(MovementHandler.class).getPixelPosition(), sprite);
    }

    /**
     * Returns the bounds of this GameObject
     *
     * @param pixelPos A vector containing a pixel position.
     * @param sprite A sprite.
     * @return | result == new Rectangle((int)pixelPos.getX(), (int)pixelPos.getY(), sprite.getWidth(), sprite.getHeight());
     */
    public Bounds getBounds(Vector<Integer> pixelPos, Sprite sprite){
        return new Bounds(pixelPos, sprite);
    }

    /**
     * Returns the outer bounds of this GameObject
     *
     * @return | result == getOuterBounds(getHandler(MovementHandler.class).getPosition().actualToPixel())
     */
    @Override
    public Bounds getOuterBounds() {
        return getOuterBounds(getHandler(MovementHandler.class).getPosition().actualToPixel());
    }

    /**
     * Returns the outer bounds of this GameObject
     *
     * @param pixelPos A position in pixels
     *
     * @return | result == getOuterBounds(pixelPos, getHandler(AnimationHandler.class).getCurrentSprite());
     */
    @Override
    public Bounds getOuterBounds(Vector<Integer> pixelPos) {
        return getOuterBounds(pixelPos, getHandler(AnimationHandler.class).getCurrentSprite());
    }

    /**
     * Returns the outer bounds of this GameObject
     *
     * @param sprite A sprite
     *
     * @return | result == getBounds(getHandler(MovementHandler.class).getPixelPosition(), sprite);
     */
    @Override
    public Bounds getOuterBounds(Sprite sprite) {
        return getBounds(getHandler(MovementHandler.class).getPixelPosition(), sprite);
    }

    /**
     * Returns the outer bounds of this GameObject
     * Outer bounds have an offset of 1
     * @see Bounds#Bounds(Vector, Sprite, int)
     *
     * @param pixelPos A position in pixels
     * @param sprite A sprite
     *
     * @return | result == new Bounds(pixelPos, sprite, 1);
     */
    @Override
    public Bounds getOuterBounds(Vector<Integer> pixelPos, Sprite sprite) {
        return new Bounds(pixelPos, sprite, 1);
    }

    /**
     * Method to automatically redirect onCollision calls on ICollidable to the CollisionHandler of this GameObject
     *
     * @param other The other ICollidable
     * @effect If this GameObject has a CollisionHandler, invoke onCollision on that CollisionHandler
     *         | if (hasHandler(CollisionHandler.class))
     *         |    getHandler(CollisionHandler.class).onCollision(other)
     */
    @Override
    public void onCollision(ICollidable other) {
        if (hasHandler(CollisionHandler.class))
            getHandler(CollisionHandler.class).onCollision(other);
    }

    /**
     * Method to automatically redirect onCollisionEnter calls on ICollidable to the CollisionHandler of this GameObject
     *
     * @param other The other ICollidable
     * @effect If this GameObject has a CollisionHandler, invoke onCollisionEnter on that CollisionHandler
     *         | if (hasHandler(CollisionHandler.class))
     *         |    getHandler(CollisionHandler.class).onCollisionEnter(other)
     */
    @Override
    public void onCollisionEnter(ICollidable other) {
        if (hasHandler(CollisionHandler.class))
            getHandler(CollisionHandler.class).onCollisionEnter(other);
    }

    /**
     * Method to automatically redirect onCollisionExit calls on ICollidable to the CollisionHandler of this GameObject
     *
     * @param other The other ICollidable
     * @effect If this GameObject has a CollisionHandler, invoke onCollisionExit on that CollisionHandler
     *         | if (hasHandler(CollisionHandler.class))
     *         |    getHandler(CollisionHandler.class).onCollisionExit(other)
     */
    @Override
    public void onCollisionExit(ICollidable other) {
        if (hasHandler(CollisionHandler.class))
            getHandler(CollisionHandler.class).onCollisionExit(other);
    }

    /**
     * Checks whether this GameObject can have the given orientation as its orientation
     *
     * @param orientation The orientation to check
     *
     * @return Any orientation is allowed for GameObjects
     *       | result == orientation != null
     */
    public boolean canHaveAsOrientation(OrientationState orientation) {
        return orientation != null;
    }

    /**
     * Sets the orientation of this GameObject
     *
     * @param orientation The new orientation
     *
     * @pre This GameObject can have the given orientation as its orientation
     *    | canHaveAsOrientation(orientation)
     * @post The new orientation will be equal to the given orientation
     *    | new.getOrientation() == orientation
     */
    public void setOrientation(OrientationState orientation) {
        assert canHaveAsOrientation(orientation);
        this.orientation = orientation;
    }

    /**
     * Returns the orientation of this GameObject
     */
    @Basic
    @Raw
    public OrientationState getOrientation() {
        return orientation;
    }

    /**
     * Variable to store the orientation
     * Default value is OrientationState.Front
     */
    private OrientationState orientation = OrientationState.Front;

    /**
     * Changes the hit points when taking the given amount of damage
     * Negative damage will heal the GameObject
     *
     * @param damage an integer containing the damage.
     *
     * @effect The hit points will be set to the current hit points minus the given damage
     *      | setHitPoints(getHitPoints() - damage);
     */
    public void takeDamage(int damage) {
        setHitPoints(getHitPoints() - damage);
    }

    /**
     * The category for collision of this GameObject
     * By default nothing will collide with this GameObject
     *
     * @return | result == 0
     */
    @Override
    public byte getCategory() {
        return 0;
    }

    /**
     * The mask for collision of this GameObject
     * By default this GameObject wont collide with anything
     *
     * @return | result == 0
     */
    @Override
    public byte getMask() {
        return 0;
    }

    /**
     * Returns whether the GameObject is solid. Solid ICollidables block movement of GameObjects
     * that have a DynamicMovementHandler. By default a GameObject is not solid.
     *
     * @return | result == false
     */
    @Override
    public boolean isSolid() {
        return false;
    }
}
