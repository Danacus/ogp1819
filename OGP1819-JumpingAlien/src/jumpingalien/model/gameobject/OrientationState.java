package jumpingalien.model.gameobject;

import be.kuleuven.cs.som.annotate.Basic;

public enum OrientationState {
    Front(true, true, 0),
    Left(true, false, -1),
    Right(true, false, 1),
    Bottom(false, true, -1),
    Top(false, true, 1);

    /**
     * Initialize the orientation state.
     *
     * @param isHorizontal
     *          Set whether the orientation state is horizontal.
     * @param isVertical
     *          Set whether the orientation state is vertical.
     * @param number
     *          Set the index of the orientation state.
     */
    OrientationState(boolean isHorizontal, boolean isVertical, int number) {
        this.isHorizontal = isHorizontal;
        this.isVertical = isVertical;
        this.index = number;
    }

    /**
     * Returns the index of the orientation state.
     */
    @Basic
    public int getIndex() {
        return index;
    }

    /**
     * Returns whether the orientation state is horizontal.
     */
    @Basic
    public boolean isHorizontal() {
        return isHorizontal;
    }

    /**
     * A variable registering if the orientation state is horizontal.
     */
    private final boolean isHorizontal;

    /**
     * Returns whether the orientation state is vertical.
     */
    @Basic
    public boolean isVertical() {
        return isVertical;
    }

    /**
     * A variable registering if the orientation state is vertical.
     */
    private final boolean isVertical;

    /**
     * A variable registering the index of the orientation state.
     */
    private final int index;

    /**
     *
     * @return Returns the inverted orientation state.
     */
    public OrientationState inverted() {
        if (this == Left)
            return Right;
        if (this == Right)
            return Left;
        if (this == Top)
            return Bottom;
        if (this == Bottom)
            return Top;
        return Front;
    }
}
