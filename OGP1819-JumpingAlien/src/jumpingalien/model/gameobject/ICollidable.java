package jumpingalien.model.gameobject;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.util.Bounds;
import jumpingalien.util.Sprite;
import jumpingalien.util.Vector;

/**
 * Interface for objects that collide with each other
 */
public interface ICollidable {
    /**
     * Returns the bounds of this collidable in pixels
     */
    Bounds getBounds();
    Bounds getBounds(Vector<Integer> pixelPos);
    Bounds getBounds(Sprite sprite);
    Bounds getBounds(Vector<Integer> pixelPos, Sprite sprite);

    /**
     * Returns the outer bounds of this collidable
     * By default it uses getBounds
     */
    default Bounds getOuterBounds() { return getBounds(); };
    default Bounds getOuterBounds(Vector<Integer> pixelPos) { return getBounds(pixelPos); };
    default Bounds getOuterBounds(Sprite sprite) { return getBounds(sprite); };
    default Bounds getOuterBounds(Vector<Integer> pixelPos, Sprite sprite) { return getBounds(pixelPos, sprite); };

    /**
     * Event for continued collision between this GameObject and other collidable objects
     *
     * @param other The other collidable
     * @note   This method will only be invoked when th
     */
    void onCollision(ICollidable other);

    /**
     * Event when entering another collidable
     *
     * @param other
     */
    void onCollisionEnter(ICollidable other);

    /**
     * Event when leaving another collidable
     *
     * @param other
     */
    void onCollisionExit(ICollidable other);

    /**
     * Checks if 2 collidables intersect with each other
     *
     * @param other
     * @return | result == getBounds().intersects(other.getBounds())
     *
     * @deprecated Since the introduction of getOuterBounds, this method can behave unexpectedly or might not
     *             be very elaborate, as it does not offer a way to check intersection between outer bounds.
     *             Please use {@link Bounds#intersects(Bounds)} instead.
     */
    @Deprecated
    default boolean intersectsWith(ICollidable other) {
        return getBounds().intersects(other.getBounds());
    }

    /**
     * Returns the category of this collidable
     */
    @Basic
    @Immutable
    byte getCategory();

    /**
     * Returns the mask of this collidable
     */
    @Basic
    @Immutable
    byte getMask();

    /**
     * Returns whether the collidable is solid, in other words,
     * should this collidable block movement of other collidables?
     */
    @Basic
    @Immutable
    boolean isSolid();

    /**
     * Checks whether this collidable can collide with the given collidable
     *
     * @param other The other collidable
     *
     * @return True if and only if the result of the binary and operator on this mask and the other's category
     *         is different from 0
     *       | result == (getMask() & other.getCategory()) != 0
     */
    default boolean canCollideWith(ICollidable other) {
        return (getMask() & other.getCategory()) != 0;
    }
}
