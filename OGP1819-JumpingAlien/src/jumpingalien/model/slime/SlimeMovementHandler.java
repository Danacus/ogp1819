package jumpingalien.model.slime;

import jumpingalien.model.Slime;
import jumpingalien.util.exception.IllegalPositionException;
import jumpingalien.model.handler.DynamicMovementHandler;
import jumpingalien.util.Vector;

/**
 * A class for handling the movement of a Slime
 *
 * @invar | getGameObject() instanceof Slime
 */
public class SlimeMovementHandler extends DynamicMovementHandler {
    /**
     * Creates a new Slime Movement Handler
     *
     * @param slime     The slime
     * @param initPos   The initial position of the slime
     *
     * @effect | super(slime)
     * @effect | setPosition(initPos)
     * @effect | setAcceleration(new Vector<>(0.7,0.0))
     * @effect | setVelocity(Vector.ACTUAL_ZERO)
     * @effect | disableGravity()
     */
    public SlimeMovementHandler(Slime slime, Vector<Double> initPos) throws IllegalPositionException {
        super(slime);

        this.setPosition(initPos);
        this.setAcceleration(new Vector<>(0.7,0.0));
        this.setVelocity(Vector.ACTUAL_ZERO);

        disableGravity();
    }

    /**
     * Turns the slime
     *
     * @effect | getGameObject().setOrientation(getGameObject().getOrientation().inverted());
     */
    public void turn() {
        getGameObject().setOrientation(getGameObject().getOrientation().inverted());
    }

    /**
     * Returns the maximum velocity for a slime
     *
     * @return | result == new Vector<>(2.5, 0.0)
     */
    @Override
    public Vector<Double> getMaxVelocity() {
        return new Vector<>(2.5, 0.0);
    }

    /**
     * Returns the minimum velocity for a slime
     *
     * @return | result == Vector.ACTUAL_ZERO
     */
    @Override
    public Vector<Double> getMinVelocity() {
        return Vector.ACTUAL_ZERO;
    }

    /**
     * Updates the movement of the slime
     *
     * @param dt The time that has passed since the last update.
     */
    @Override
    public void pixelUpdate(double dt) {
        setAcceleration(new Vector<>(0.7 * getGameObject().getOrientation().getIndex(), getAcceleration().getY()));
        super.pixelUpdate(dt);
    }

    /**
     * Event when movement is blocked horizontally
     *
     * @param delta The difference between the current position and the new position that could not be applied
     *
     * @effect | setVelocity(getVelocity().setX(0.0));
     * @effect | setAcceleration(getAcceleration().setX(0.0));
     */
    @Override
    public void onBounceX(Vector<Double> delta) {
        setVelocity(getVelocity().setX(0.0));
        setAcceleration(getAcceleration().setX(0.0));
    }

    /**
     * Event when movement is blocked vertically
     *
     * @param delta The difference between the current position and the new position that could not be applied
     *
     * @effect | setVelocity(getVelocity().setY(0.0));
     * @effect | setAcceleration(getAcceleration().setY(0.0));
     */
    @Override
    public void onBounceY(Vector<Double> delta) {
        setVelocity(getVelocity().setY(0.0));
        setAcceleration(getAcceleration().setY(0.0));
    }
}
