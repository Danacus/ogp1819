package jumpingalien.model.slime;

import jumpingalien.model.Slime;
import jumpingalien.util.exception.InvalidSpriteException;
import jumpingalien.model.handler.AnimationHandler;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.util.Sprite;

/**
 * A class for handling the sprites of a Slime
 *
 * @invar | getGameObject() instanceof Slime
 */
public class SlimeAnimationHandler extends AnimationHandler {
    /**
     * Creates a new Slime Animation Handler
     *
     * @param slime     The slime
     * @param sprites   The sprites for the slime
     */
    public SlimeAnimationHandler(Slime slime, Sprite... sprites) throws InvalidSpriteException {
        super(slime, sprites);
    }

    /**
     * Checks whether the slime can have the given sprites as its sprites
     *
     * @param sprites  The sprites to check
     *
     * @return | result == return super.canHaveAsSprites(sprites) && sprites.length == 2
     */
    @Override
    public boolean canHaveAsSprites(Sprite[] sprites) {
        return super.canHaveAsSprites(sprites) && sprites.length == 2;
    }

    /**
     * Returns the current sprite for a slime
     */
    @Override
    public Sprite getCurrentSprite() {
        return getGameObject().getOrientation() == OrientationState.Left ? getSprites()[1] : getSprites()[0];
    }
}
