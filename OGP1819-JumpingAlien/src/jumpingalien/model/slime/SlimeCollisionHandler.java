package jumpingalien.model.slime;

import jumpingalien.model.Mazub;
import jumpingalien.model.Shark;
import jumpingalien.model.Slime;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.gameobject.ICollidable;
import jumpingalien.model.handler.MovementHandler;
import jumpingalien.model.handler.WorldCollisionHandler;
import jumpingalien.util.GameTimer;
import jumpingalien.model.world.GeologicalFeature;
import jumpingalien.model.world.Tile;
import jumpingalien.util.Vector;

/**
 * A class for handling the collision of a Slime
 *
 * @invar | getGameObject() instanceof Slime
 */
public class SlimeCollisionHandler extends WorldCollisionHandler {

    /**
     * Creates a new Slime Collision Handler
     *
     * @param slime  The slime
     *
     * @effect | super(slime)
     */
    public SlimeCollisionHandler(Slime slime) {
        super(slime);
    }

    public GameTimer slimeCollisionTimer = new GameTimer(0.6, false) {
        @Override
        public void onTick() {
            this.stop();
        }
    };

    public GameTimer getSlimeCollisionTimer(){
        return slimeCollisionTimer;
    }

    /**
     * Updates the slime collision handler
     *
     * @param dt   The amount of time to advance
     */
    @Override
    public void update(double dt) {
        super.update(dt);
        getSlimeCollisionTimer().update(dt);
    }

    /**
     * Event when entering collision with another collidable
     *
     * @param other The other collidable
     *
     * @effect | super.onCollisionEnter(other)
     * @effect
     *         | if (other instanceof Tile) then
     *         |     if (((Tile) other).getGeologicalFeature() == GeologicalFeature.MAGMA) then
     *         |        getGameObject().takeDamage(getGameObject().getHitPoints())
     *
     * @effect | if (other instanceof Slime && other != getGameObject()) then
     *         |    getGameObject().getHandler(SlimeMovementHandler.class).turn();
     *
     * @effect | if (((Slime) getGameObject()).getSchool() != null) then
     *         |    if (((Slime) getGameObject()).getSchool().getMembers().size()
     *         |        < ((Slime) other).getSchool().getMembers().size()
     *         |        && ((Slime) getGameObject()).getSchool() != ((Slime) other).getSchool())
     *         |        ((Slime) getGameObject()).switchSchool(((Slime) other).getSchool())
     *
     * @effect | if (other instanceof Shark && !((Shark) other).isDead()) then
     *         |    getGameObject().takeDamage(getGameObject().getHitPoints())
 `   *
     * @effect | if (other instanceof Mazub && !((Mazub) other).isDead() && !getSlimeCollisionTimer().isRunning()) then
     *         |    getSlimeCollisionTimer().start()
     *         |    getGameObject().takeDamage(30)
     */
    @Override
    public void onCollisionEnter(ICollidable other) {
        super.onCollisionEnter(other);
        if(other instanceof Tile){
            if(((Tile) other).getGeologicalFeature() == GeologicalFeature.MAGMA)
                getGameObject().takeDamage(getGameObject().getHitPoints());
        }

        if (other instanceof Slime && other != getGameObject()) {
            getGameObject().getHandler(SlimeMovementHandler.class).turn();

            if (((Slime) getGameObject()).getSchool() != null)
                if (((Slime) getGameObject()).getSchool().getMembers().size()
                        < ((Slime) other).getSchool().getMembers().size()
                        && ((Slime) getGameObject()).getSchool() != ((Slime) other).getSchool())
                            ((Slime) getGameObject()).switchSchool(((Slime) other).getSchool());

        }

        if (other instanceof Shark && !((Shark) other).isDead()) {
            getGameObject().takeDamage(getGameObject().getHitPoints());
        }

        if (other instanceof Mazub && !((Mazub) other).isDead() && !getSlimeCollisionTimer().isRunning()) {
            getSlimeCollisionTimer().start();
            getGameObject().takeDamage(30);
        }
    }
}
