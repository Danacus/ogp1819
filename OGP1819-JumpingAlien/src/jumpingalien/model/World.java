package jumpingalien.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.handler.MovementHandler;
import jumpingalien.model.world.*;
import jumpingalien.util.exception.IllegalPositionException;
import jumpingalien.util.ModelException;
import jumpingalien.util.Vector;

import java.util.*;

/**
 * A class representing the World in which all creatures (formally called GameObjects) live.
 *
 * @invar  Each World must have proper GameObjects.
 *        | hasProperGameObjects()
 * @invar  Each World can have its tiles as tiles .
 *       | canHaveAsTiles(this.getTiles())
 * @invar  The target tile is a valid target tile for this world
 *       | isValidTargetTile(getTargetTile)
 */
public class World {

    /**
     * Initialize this new World as a non-terminated World with
     * no GameObjects yet.
     *
     * @param  visibleWindowHeight
     *         The visible window height for this new world.
     * @param  visibleWindowWidth
     *         The visible window width for this new world.
     *
     * @post | getCollisionManager() == new CollisionManager(this)
     * @post | getWindowManager() == new WindowManager(this)
     * @effect The visible window position of this new World is set to
     *         the zero vector.
     *       | this.getWindowManager().setVisibleWindowPosition(Vector.ZERO)
     * @effect The visible window of this new world is set to
     *         the given visible window.
     *       | this.getWindowManager().setVisibleWindow(visibleWindow)
     * @post   This new World as no GameObjects yet.
     *       | new.getNbGameObjects() == 0
     * @post
     *       | for y in IntStream.range(0, nbTilesY):
     *       |      for x in IntStream.range(0, nbTilesX):
     *       |          new.tiles[x][y].getPosition() == new Vector(x * tileSize, y * tileSize).pixelToActual()
     *       |          if y * getNbTilesX() + x < geologicalFeatures.length then
     *       |              new.tiles[x][y].getGeologicalFeature() == GeologicalFeature.values()[geologicalFeatures[y * getNbTilesX() + x]]
     *       |          else
     *       |              new.tiles[x][y].getGeologicalFeature() == GeologicalFeature.AIR
     * @effect | setTargetTileIndex(Vector.fromIntergerArray(targetTileCoordinate))
     */
    public World(int tileSize, int nbTilesX, int nbTilesY, int[] targetTileCoordinate, int visibleWindowWidth, int visibleWindowHeight, int... geologicalFeatures) throws IllegalArgumentException {
        if(!isValidWorld(tileSize,nbTilesX,nbTilesY,targetTileCoordinate)) throw new IllegalArgumentException("Illegal world!");
        if(!isValidGeologicalFeatures(geologicalFeatures)) throw new IllegalArgumentException("Illegal geological features!");
        this.tileSize = tileSize;
        this.tiles = new Tile[nbTilesX][nbTilesY];

        GeologicalFeature[] features = GeologicalFeature.values();

        for (int y = 0; y < nbTilesY; y++) {
            for (int x = 0; x < nbTilesX; x++) {
                int index = y * nbTilesX + x;
                GeologicalFeature currentFeature = GeologicalFeature.AIR;
                if (index < geologicalFeatures.length)
                    if (0 <= geologicalFeatures[index] && geologicalFeatures[index] <= 5)
                        currentFeature = features[geologicalFeatures[index]];

                tiles[x][y] = new Tile(currentFeature, new Vector<>(x * tileSize, y * tileSize).pixelToActual(), new Vector<>(x, y), getTileSize());
            }
        }
        assert canHaveAsTiles(getTiles());

        setTargetTileIndex(Vector.fromIntegerArray(targetTileCoordinate));

        windowManager = new WindowManager(this);
        schoolManager = new SchoolManager(this);
        getWindowManager().setVisibleWindow(new Vector<>(visibleWindowWidth, visibleWindowHeight));
        getWindowManager().setVisibleWindowPosition(Vector.PIXEL_ZERO);

    }

    /**
     * Check if this world is valid.
     * @param tileSize The tile size.
     * @param nbTilesX The number of tiles on the x-axis.
     * @param nbTilesY The number of tiles on the y-axis.
     * @param targetTileCoordinate The target tile coordinate.
     * @return Returns whether the given world is a valid world.
     *          | return == !(tileSize <= 0 || nbTilesX < 0 || nbTilesY < 0 || targetTileCoordinate == null || targetTileCoordinate.length != 2)
     */
    private boolean isValidWorld(int tileSize, int nbTilesX, int nbTilesY, int[] targetTileCoordinate){
        return !(tileSize <= 0 || nbTilesX < 0 || nbTilesY < 0 || targetTileCoordinate == null || targetTileCoordinate.length != 2);
    }

    /**
     * Check if the given coordinate has a valid geological feature.
     * @param geologicalFeatures The coordinate of the tile.
     * @return Returns whether the target tile has a valid geological feature.
     *          return == !(geologicalFeatures == null)
     */
    private boolean isValidGeologicalFeatures(int[] geologicalFeatures){
        return !(geologicalFeatures == null);
    }

    /**
     * Variable registering whether the school manager.
     */
    private SchoolManager schoolManager;

    /**
     * Variable registering the school manager of this world.
     */
    @Basic
    public SchoolManager getSchoolManager() {
        return schoolManager;
    }

    /**
     * Returns whether the game has started.
     */
    @Basic @Raw
    public boolean hasStarted() {
        return this.hasStarted;
    }

    /**
     * Starts the game
     *
     * @post | then new.isStarted() == true
     *
     * @throws ModelException | getNbGameObjects() == 0
     */
    @Raw
    public void startGame() throws ModelException {
        if (getNbGameObjects() == 0)
            throw new ModelException("Cannot start game: no GameObjects");
        this.hasStarted = true;
    }

    /**
     * Variable registering whether the game is started
     */
    private boolean hasStarted;

    /**
     * Returns whether the game is over.
     */
    @Basic @Raw
    public boolean isGameOver() {
        return this.isGameOver;
    }

    /**
     * Starts the game.
     *
     * @post | new.isGameOVer() == true
     */
    @Raw
    public void endGame() throws ModelException {
        this.isGameOver = true;
    }

    /**
     * Variable registering whether the game is over
     */
    private boolean isGameOver;

    /**
     * Returns the window manager of this world.
     */
    @Raw
    @Basic
    @Immutable
    public WindowManager getWindowManager() {
        return windowManager;
    }

    /**
     * Variable registering the window manager of this world.
     */
    private final WindowManager windowManager;

    /**
     * Returns the feature with the given value if the value is a valid value,
     *
     * @param value The value
     *
     * @return | if (value >= 0 && value < GeologicalFeature.values().length)
     *         |    result == GeologicalFeature.values()[value]
     *         | else
     *         |    result == GeologicalFeature.AIR
     */
    public static GeologicalFeature getGeologicalFeatureFromValue(int value) {
        try {
            return GeologicalFeature.values()[value];
        } catch (IndexOutOfBoundsException e) {
            return GeologicalFeature.AIR;
        }
    }

    /**
     * Returns the feature of the tile at the given position in pixels
     *
     * @param position a vector containing a pixel position.
     * @return
     *      | if (getTileAtPosition(pixelPosition) != null) then
     *      |   result == getTileAtPosition(pixelPosition).getGeologicalFeature()
     *      | else
     *      |   result == GeologicalFeature.AIR
     */
    public GeologicalFeature getGeologicalFeature(Vector<Integer> position) {
        if (getTileAtPosition(position) != null)
            return getTileAtPosition(position).getGeologicalFeature();
        else
            return GeologicalFeature.AIR;
    }

    /**
     * Sets the geological feature at a give position
     *
     * @param pos The position of the tile.
     * @param geologicalFeature The new geological feature.
     *
     * @effect | setGeologicalFeature(pos, getGeologicalFeatureFromValue(geologicalFeature))
     */
    public void setGeologicalFeature(Vector<Integer> pos, int geologicalFeature){
        setGeologicalFeature(pos, getGeologicalFeatureFromValue(geologicalFeature));
    }

    /**
     * Sets the geological feature at a give position
     *
     * @param pos The position of the tile.
     * @param geologicalFeature The new geological feature.
     *
     * @post Set the new geological feature.
     *      | if (isValidTilePosition(pos))
     *      |   getTileFromPixelPos(pos).getGeologicalFeature() == geologicalFeature
     */
    public void setGeologicalFeature(Vector<Integer> pos, GeologicalFeature geologicalFeature) {
        if (!isValidTilePosition(pos))
            return;
        tiles[pos.getX() / getTileSize()][pos.getY() / getTileSize()] =
                getTileAtPosition(pos).setGeologicalFeature(geologicalFeature);
    }

    /**
     * Returns the tile at the given position
     *
     * @param pixelPosition a vector containing a pixel position.
     * @return
     *      | result == return getTileAtCoordinate(pixelPosition.scl(1 / getTileSize()))
     */
    public Tile getTileAtPosition(Vector<Integer> pixelPosition) {
        return getTileAtCoordinate(new Vector<>(
                pixelPosition.getX() / getTileSize(),
                pixelPosition.getY() / getTileSize()
        ));
    }

    /**
     * Returns the tile at the given coordinates
     *
     * @param coordinates a vector containing a coordinates
     * @return
     *      | if (isValidTilePosition(pixelPosition)) then
     *      |   result == getTiles()[coordinate.getX()][coordinate.getY()];
     *      | else result == null
     */
    public Tile getTileAtCoordinate(Vector<Integer> coordinates) {
        if (isValidTileCoordinate(coordinates))
            return getTiles()[coordinates.getX()][coordinates.getY()];
        else
            return null;
    }

    /**
     * Checks whether the given position is a valid position for a tile
     *
     * @param position The position to check
     *
     * @return | isValidTileCoordinate(position.scl(1 / getTileSize()))
     */
    public boolean isValidTilePosition(Vector<Integer> position) {
        return isValidTileCoordinate(new Vector<>(
                position.getX() / getTileSize(),
                position.getY() / getTileSize()
        ));
    }

    /**
     * Checks whether the given coordinates are valid coordinates for a tile
     *
     * @param coordinates The coordinates to check
     *
     * @return
     *      | result ==   ( coordinates.getX() < getNbTilesX()
     *      |            && coordinates.getY() < getNbTilesY()
     *      |            && coordinates.getX() >= 0
     *      |            && coordinates.getY() >= 0 );
     */
    public boolean isValidTileCoordinate(Vector<Integer> coordinates) {
        return   ( coordinates.getX() < getNbTilesX()
                && coordinates.getY() < getNbTilesY()
                && coordinates.getX() >= 0
                && coordinates.getY() >= 0 );
    }

    /**
     * Checks whether this world can have the given tiles as its tiles
     *
     * @param tiles  The tiles to check
     *
     * @return | result ==
     *         | (for each tile in tiles:
     *         |    isValidTilePosition(tile.getPosition().actualToPixel()) && isValidTileCoordinate(tile.getCoordinates())
     *         | )
     */
    public boolean canHaveAsTiles(Tile[][] tiles) {
        return Arrays.stream(tiles).allMatch(line ->
                Arrays.stream(line).allMatch(tile ->
                        isValidTilePosition(tile.getPosition().actualToPixel()) && isValidTileCoordinate(tile.getCoordinates())
                )
        );
    }

    /**
     * Sets the target tile at the given coordinates
     *
     * @param coordinates a vector.
     * @effect
     *      | setTargetTile(new Tile(GeologicalFeature.AIR, indexes.scl(tileSize).pixelToActual(), indexes, getTileSize()));
     */
    public void setTargetTileIndex(Vector<Integer> coordinates) {
        setTargetTile(new Tile(GeologicalFeature.AIR, coordinates.scl(tileSize).pixelToActual(), coordinates, getTileSize()));
    }

    /**
     * Sets the target tile of this world
     *
     * @param tile The target tile
     *
     * @pre  | isValidTargetTile(tile)
     * @post | if (isValidTargetTile(tile) then
     *       |      tile.isTargetTile()
     *       |      getTargetTile() == tile
     */
    public void setTargetTile(Tile tile) {
        assert isValidTargetTile(tile);
        this.targetTile = tile;
    }

    /**
     * Gets the target tile
     */
    @Basic
    public Tile getTargetTile() {
        return targetTile;
    }

    /**
     * Gets the target tile coordinates
     *
     * @pre | getTargetTile() != null
     *
     * @return | getTargetTile().getPosition()
     */
    public Vector<Double> getTargetTilePosition() {
        assert getTargetTile() != null;
        return getTargetTile().getPosition();
    }

    /**
     * Checks whether the given tile is a valid tile for a world
     *
     * @param tile The tile to check
     *
     * @return tile != null && tile.getGeologicalFeature().isPassable()
     */
    public static boolean isValidTargetTile(Tile tile) {
        return tile != null;
    }

    /**
     * A variable to store the target tile
     */
    private Tile targetTile;

    /**
     * Return the tile size of this World.
     */
    @Basic @Raw @Immutable
    public int getTileSize() {
        return this.tileSize;
    }

    /**
     * Variable registering the tile size of this World.
     */
    private final int tileSize;

    /**
     * Returns the dimensions of the world in pixels
     *
     * @return
     *      | result.getX() == getTileSize() * getNbTilesX()
     *      | result.getY() == getTileSize() * getNbTilesY()
     */
    public Vector<Integer> getWorldPixelDimensions() {
        return new Vector<>(getTileSize() * getNbTilesX(), getTileSize() * getNbTilesY());
    }

    /**
     * Returns the number of tiles on the x-axis.
     */
    @Basic
    public int getNbTilesX() {
        return tiles.length;
    }

    /**
     * Returns the number of tiles on the y-axis.
     */
    @Basic
    public int getNbTilesY() {
        return tiles[0].length;
    }

    /**
     * Return the tiles of this World.
     */
    @Basic @Raw @Immutable
    public Tile[][] getTiles() {
        return this.tiles;
    }

    /**
     * Variable registering the tiles of this World.
     */
    private final Tile[][] tiles;

    /**
     * Updates all GameObjects and Managers of this world
     *
     * @param dt The time to advance
     * @throws RuntimeException
     *      | !hasStarted()
     * @throws IllegalArgumentException
            | dt <= 0 || dt > 0.2 || Double.isNaN(dt)
     */
    public void update(double dt) throws RuntimeException, IllegalArgumentException {
        // Potential bug in tests caused us to disable the following line
//        if (!hasStarted()) throw new RuntimeException("Cannot update world, game not started");
        if (dt <= 0 || dt > 0.2 || Double.isNaN(dt))
            throw new ModelException("Illegal time was given to update");

        Iterator<GameObject> it = getGameObjects().iterator();
        while (it.hasNext()) {
            GameObject gameObject = it.next();
            gameObject.update(dt);
        }

        getWindowManager().update(dt);
    }

    /**
     * Terminate this World.
     *
     * @post   This World  is terminated.
     *       | new.isTerminated()
     * @effect   The GameObjects of this World are terminated
     *       | for each gameObject in gameObjects:
     *       |   gameObject.terminate()
     */
    public void terminate() {
        Iterator<GameObject> iter = getGameObjects().iterator();
        while (iter.hasNext()) {
            GameObject gameObject = iter.next();
            gameObject.terminate();
        }
        this.isTerminated = true;
    }

    /**
     * Return a boolean indicating whether or not this World
     * is terminated.
     */
    @Basic @Raw
    public boolean isTerminated() {
        return this.isTerminated;
    }

    /**
     * Variable registering whether this person is terminated.
     */
    private boolean isTerminated = false;


    /**
     * Check whether this World has the given GameObject as one of its
     * GameObjects.
     *
     * @param  gameObject
     *         The GameObject to check.
     */
    @Basic
    @Raw
    public boolean hasAsGameObject(@Raw GameObject gameObject) {
        return gameObjects.contains(gameObject);
    }

    /**
     * Check whether this World can have the given GameObject
     * as one of its GameObjects.
     *
     * @param  gameObject
     *         The GameObject to check.
     * @return True if and only if the given GameObject is effective
     *         and that GameObject is a valid GameObject for a World.
     *       | result ==
     *       |   (gameObject != null) &&
     *       |   GameObject.canHaveAsWorld(this)
     */
    @Raw
    public boolean canHaveAsGameObject(GameObject gameObject) {
        if (gameObject == null || gameObject.isTerminated())
            return false;
        if (gameObject instanceof Mazub && getMazub() != null)
            return false;

        return !hasStarted() && !isTerminated() && (gameObject.getWorld() == this || gameObject.getWorld() == null) && (getNbGameObjects() < 100 || gameObject instanceof Mazub);
    }

    /**
     * Checks whether the GameObject is able to spawn at its position in this world
     *
     * @param gameObject The GameObject
     *
     * @return  | let
     *          |   movementHandler == gameObject.getHandler(MovementHandler.class)
     *          | in
     *          |   if (!isInBoundsOfWorld(gameObject)) then result == false
     *          |   else if (!movementHandler.canHaveAsPosition(movementHandler.getPosition()) then result == false
     *          |   else result == true
     */
    public boolean canGameObjectSpawn(GameObject gameObject) {
        assert gameObject.getWorld() == this;
        if(!isInBoundsOfWorld(gameObject))
            return false;

        // Try to call setPosition the check if it's allowed to spawn in this world at its position
        try {
            gameObject.getHandler(MovementHandler.class).setPosition(gameObject.getHandler(MovementHandler.class).getPosition());
        } catch (IllegalPositionException e) {
            return false;
        }

        return true;
    }

    /**
     * Check whether this World has proper GameObjects attached to it.
     *
     * @return True if and only if this World can have each of the
     *         GameObjects attached to it as one of its GameObjects,
     *         and if each of these GameObjects references this World as
     *         the World to which they are attached.
     *       | for each gameObject in gameObjects:
     *       |   if (hasAsGameObject(gameObject))
     *       |     then canHaveAsGameObject(gameObject) &&
     *       |          (gameObject.getWorld() == this)
     */
    public boolean hasProperGameObjects() {
        for (GameObject gameObject : gameObjects) {
            if (!canHaveAsGameObject(gameObject))
                return false;
            if (gameObject.getWorld() != this)
                return false;
        }
        return true;
    }

    /**
     * Return the number of GameObjects associated with this World.
     *
     * @return  The total number of GameObjects collected in this World.
     *        | result ==
     *        |   card({gameObject:GameObject | hasAsGameObject({gameObject)})
     */
    public int getNbGameObjects() {
        return gameObjects.size();
    }

    /**
     * Add the given GameObject to the set of GameObjects of this World.
     *
     * @param  gameObject
     *         The GameObject to be added.
     * @post   This World has the given GameObject as one of its GameObjects.
     *       | new.hasAsGameObject(gameObject)
     * @post
     *       | (new gameObject).getWorld() == this
     * @post
     *       | if (this.getMazub() == null && gameObject instanceof Mazub) then
     *       |      new.getMazub() == (Mazub) gameObject
     *       |      new.getWindowManager().getTarget() == gameObject
     * @throws IllegalArgumentException
     *       | !canHaveAsGameObject(gameObject)
     */
    public void addGameObject(GameObject gameObject) throws IllegalArgumentException {
        if (!canHaveAsGameObject(gameObject))
            throw new IllegalArgumentException("GameObject must not be null and its world must not be assigned yet.");

        gameObjects.add(gameObject);
        gameObject.setWorld(this);

        if (!canGameObjectSpawn(gameObject))
            throw new IllegalArgumentException("Illegal spawning position");

        if (getMazub() == null && gameObject instanceof Mazub) {
            mazub = (Mazub) gameObject;
            getWindowManager().setTarget(getMazub());
        }
    }

    /**
     * Check whether the gameObject is in the bounds of the world.
     * @param gameObject
     * @return
     *      | let
     *      |   pixelPosition = gameObject.getHandler(MovementHandler,class).getPixelPosition()
     *      | in
     *      |   result == pixelPosition.getX() >= 0 && pixelPosition.getY() >= 0
     *      |   && pixelPosition.getX() < getNbTilesX() * getTileSize()
     *      |   && pixelPosition.getY() < getNbTilesY() * getTileSize()
     */
    private boolean isInBoundsOfWorld(GameObject gameObject){
        Vector<Integer> pixelPosition = gameObject.getHandler(MovementHandler.class).getPixelPosition();
        return pixelPosition.getX() >= 0 && pixelPosition.getY() >= 0
                && pixelPosition.getX() < getNbTilesX() * getTileSize()
                && pixelPosition.getY() < getNbTilesY() * getTileSize();
    }

    /**
     * Remove the given GameObject from the set of GameObjects of this World.
     *
     * @param  gameObject
     *         The GameObject to be removed.
     * @post   This World no longer has the given GameObject as
     *         one of its GameObjects.
     *       | ! new.hasAsGameObject(gameObject)
     * @post
     *       | (new gameObject).getWorld() == null
     * @post
     *       | if (getMazub() == gameObject) then
     *       |      new.getMazub() == null
     * @throws IllegalArgumentException
     *       | !this.hasAsGameObject(gameObject)
     */
    @Raw
    public void removeGameObject(GameObject gameObject) throws ModelException {
        if (!this.hasAsGameObject(gameObject))
            throw new ModelException("This World must have the given GameObject as a GameObject");
        gameObjects.remove(gameObject);
        if (getMazub() == gameObject)
            mazub = null;
        gameObject.setWorld(null);
    }

    /**
     * Returns a set of all game objects in this world.
     */
    @Basic
    public Set<GameObject> getGameObjects() {
        return new LinkedHashSet<>(gameObjects);
    }

    /**
     * Variable referencing a set collecting all the GameObjects
     * of this World.
     *
     * @invar  The referenced set is effective.
     *       | gameObjects != null
     * @invar  Each GameObject registered in the referenced list is
     *         effective and not yet terminated.
     *       | for each gameObject in gameObjects:
     *       |   ( (gameObject != null) &&
     *       |     (! gameObject.isTerminated()) )
     */
    private final Set<GameObject> gameObjects = new LinkedHashSet<GameObject>();

    /**
     * Return the Mazub of this World.
     */
    @Basic @Raw
    public Mazub getMazub() {
        return this.mazub;
    }

    /**
     * Variable registering the Mazub of this World.
     */
    private Mazub mazub;

    /**
     * Returns a string containing a representation of this world
     */
    @Override
    public String toString(){
        String ret = "";
        for (Tile[] x : getTiles()) {
            for (Tile y : x) {
                ret += y.getGeologicalFeature() + " ";
            }
            ret += '\n';
        }
        return ret;
    }
}
