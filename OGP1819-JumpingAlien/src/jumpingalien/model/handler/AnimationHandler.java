package jumpingalien.model.handler;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.util.exception.InvalidSpriteException;
import jumpingalien.util.Sprite;

import java.util.Arrays;
import java.util.Objects;

/**
 * A Handler that handles animation and sprites of GameObjects
 *
 * @invar  Each AnimationHandler can have its sprites as sprites.
 *       | canHaveAsSprites(this.getSprites())
 */
public abstract class AnimationHandler extends Handler {

    /**
     * Initialize this new AnimationHandler with given GameObject.
     *
     * @param  gameObject
     *         The GameObject for this new AnimationHandler.
     * @param sprites
     *         The array of sprites for this new AnimationHandler.
     *
     * @effect | super(gameObject)
     * @post   This AnimationHandler will have the given sprites as its sprites
     *         | new.getSprites() == sprites
     * @throws InvalidSpriteException
     *        The sprite array is an invalid array.
     *        | !canHaveAsSprites(sprites)
     */
    public AnimationHandler(GameObject gameObject, Sprite[] sprites) throws InvalidSpriteException {
        super(gameObject);

        if (!canHaveAsSprites(sprites))
            throw new InvalidSpriteException("Invalid sprites array");
        this.sprites = sprites.clone();
    }


    public abstract Sprite getCurrentSprite();

    /**
     * Has the same effect as getCurrentSprite by default
     * @return | result == getCurrentSprite()
     */
    public Sprite getCurrentSpriteNoCalc() { return getCurrentSprite(); }

    /**
     * Check whether this AnimationHandler can have the given sprites as its sprites.
     *
     * @param  sprites
     *         The sprites to check.
     */
    @Raw
    public boolean canHaveAsSprites(Sprite[] sprites) {
        return sprites != null && Arrays.stream(sprites).allMatch(Objects::nonNull);
    }

    /**
     * Return the sprites of this GameObject.
     */
    @Basic
    @Raw
    @Immutable
    public Sprite[] getSprites() {
        return this.sprites.clone();
    }

    /**
     * Variable registering the sprites of this GameObject.
     */
    protected final Sprite[] sprites;
}
