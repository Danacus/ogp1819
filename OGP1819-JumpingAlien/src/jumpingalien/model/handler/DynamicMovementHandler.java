package jumpingalien.model.handler;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.gameobject.ICollidable;
import jumpingalien.util.exception.IllegalPositionException;
import jumpingalien.util.Vector;

/**
 * A class for GameObjects that move around in the world and that are affected by gravity and collision
 *
 * @invar  Each DynamicMovementHandler can have its velocity as velocity.
 *       | canHaveAsVelocity(this.getVelocity())
 */
public abstract class DynamicMovementHandler extends MovementHandler {
    public DynamicMovementHandler(GameObject gameObject) throws IllegalPositionException {
        super(gameObject);
    }

    /**
     * Return the velocity of this GameObject.
     */
    @Basic
    @Raw
    public Vector<Double> getVelocity() {
        return this.velocity;
    }

    /**
     * Set the velocity of this GameObject to the given velocity.
     *
     * @param velocity The velocity for this new GameObject.
     * @post If the given velocity is a valid velocity for any GameObject,
     * the velocity of this new GameObject is equal to the given
     * velocity. Otherwise, the velocity of this new GameObject remains untouched.
     * | if (canHaveAsVelocity(velocity))
     * |   then new.getVelocity() == velocity
     * |   else new.getVelocity() == this.getVelocity()
     */
    @Raw
    public void setVelocity(Vector<Double> velocity) {
        if (Math.abs(velocity.getX()) >= getMaxVelocity().getX()) {
            velocity = new Vector<>(Math.signum(velocity.getX()) * getMaxVelocity().getX(), velocity.getY());
        }
        if (velocity.getY() >= getMaxVelocity().getY()) {
            velocity = new Vector<>(velocity.getX(), getMaxVelocity().getY());
        }

        if (canHaveAsVelocity(velocity))
            this.velocity = velocity;
    }

    /**
     * Check whether this Mazub can have the given velocity as its velocity.
     *
     * @param velocity The velocity to check.
     * @return True if and only this Mazub can have the given velocity's x as x and y as y.
     * | result == canHaveAsXVelocity(velocity.getX()) && canHaveAsYVelocity(velocity.getY())
     */
    @Raw
    public boolean canHaveAsVelocity(Vector<Double> velocity) {
        return canHaveAsXVelocity(velocity.getX()) && canHaveAsYVelocity(velocity.getY());
    }

    /**
     * Check whether this movement handler can have the given value as x velocity
     *
     * @param velocity The value to check
     * @return True if and only if the given value is either 0 or between the minimum and maximum x velocity
     * | result == Math.abs(velocity) >= getMinVelocity().getX()
     * |       && Math.abs(velocity) <= getMaxVelocity().getX()
     * |       || velocity == 0;
     */
    @Raw
    public boolean canHaveAsXVelocity(double velocity) {
        Vector<Double> min = getMinVelocity();
        Vector<Double> max = getMaxVelocity();

        return Math.abs(velocity) >= min.getX()
                && Math.abs(velocity) <= max.getX()
                || velocity == 0;
    }

    /**
     * Check whether this movement handler can have the given value as y velocity
     *
     * @param velocity The value to check
     * @return True if and only if the given value is below the maximum value
     * | result == velocity <= getMaxVelocity().getY()
     */
    @Raw
    public boolean canHaveAsYVelocity(double velocity) {
        Vector<Double> max = getMaxVelocity();

        return velocity <= max.getY();
    }

    /**
     * Gets the maximum velocity of this GameObject
     */
    @Basic
    @Raw
    public abstract Vector<Double> getMaxVelocity();

    /**
     * Gets the minimum velocity of this GameObject
     */
    @Basic
    @Raw
    public abstract Vector<Double> getMinVelocity();

    /**
     * Variable registering the velocity of this GameObject.
     */
    protected Vector<Double> velocity;

    /**
     * Return the GameObject of this acceleration.
     */
    @Basic
    @Raw
    public Vector<Double> getAcceleration() {
        return this.acceleration;
    }

    /**
     * Set the GameObject of this acceleration to the given GameObject.
     *
     * @param acceleration The new GameObject for this acceleration.
     * @post The GameObject of this acceleration is equal to the given
     * GameObject.
     * | new.getAcceleration() == acceleration
     */
    @Raw
    public void setAcceleration(Vector<Double> acceleration) {
        this.acceleration = acceleration;
    }

    /**
     * Variable registering the GameObject of this acceleration.
     */
    protected Vector<Double> acceleration;

    /**
     * Enabled gravity for this GameObject
     *
     * @post | isGravityEnabled() == true
     */
    public void enableGravity() {
        gravity = true;
    }

    /**
     * Disables gravity for this GameObject
     *
     * @post | isGravityEnabled() == false
     */
    public void disableGravity() {
        gravity = false;
    }

    /**
     * Inverts the gravity enabled boolean
     *
     * @post | new.isGravityEnabled() == !isGravityEnabled()
     */
    public void toggleGravity() {
        gravity = !gravity;
    }

    /**
     * Checks whether this GameObject is affected by gravity
     */
    @Basic
    public boolean isGravityEnabled() {
        return gravity;
    }

    /**
     * Variable registering whether the GameObject should be affected by gravity
     * True by default
     */
    private boolean gravity = true;

    /**
     * Update the position, velocity and acceleration of the GameObject.
     *
     * @param dt The time that has passed since the last update.
     */
    public void pixelUpdate(double dt) {
        if (getGameObject().isDead()) return;

        if (isGravityEnabled()) {
            setAcceleration(new Vector<>(getAcceleration().getX(), -10.0));
        } else {
            setAcceleration(new Vector<>(getAcceleration().getX(), 0.0));
        }

        Vector<Double> newPos = getPosition().add(getVelocity().scl(dt)).add(getAcceleration().scl(Math.pow(dt, 2) / 2));
        setVelocity(getVelocity().add(getAcceleration().scl(dt)));
        moveTo(newPos);
    }

    /**
     * Tries to move the GameObject to the given position
     * If unsuccessful, onBounceX and/or onBounceY will be invoked
     *
     * @param newPos The position you want to move to
     */
    public void moveTo(Vector<Double> newPos) {
        Vector<Double> oldPos = getPosition();
        Vector<Double> newPosX = new Vector<>(newPos.getX(), oldPos.getY());
        try {
            setPosition(newPosX);
        } catch (IllegalPositionException e) {
            setPosition(oldPos);
            onBounceX(oldPos.add(newPosX.scl(-1.0)));
        }

        oldPos = getPosition();
        Vector<Double> newPosY = new Vector<>(oldPos.getX(), newPos.getY());
        try {
            setPosition(newPosY);
        } catch (IllegalPositionException e) {
            setPosition(oldPos);
            onBounceY(oldPos.add(newPosY.scl(-1.0)));
        }
    }

    /**
     * Checks whether the GameObject can have the given position as its position
     *
     * @param pos A vector containing a position.
     *
     * @return | if (getGameObject,hasHandler(CollisionHandler.class) &&
     *         |    getGameObject().getHandler(CollisionHandler.class)
     *         |    .getCollidersAt(pos.actualToPixel(), GameObject::getBounds)
     *         |    .stream().anyMatch(ICollidable::isSolid)) then
     *         |    result == false
     *         | else
     *         |    result == super.canHaveAsPosition(pos)
     */
    @Override
    public boolean canHaveAsPosition(Vector<Double> pos) {
        if (getGameObject().hasHandler(CollisionHandler.class)) {
            if (getGameObject().getHandler(CollisionHandler.class).getCollidersAt(pos.actualToPixel(), GameObject::getBounds).stream().anyMatch(ICollidable::isSolid))
                return false;
        }

        return super.canHaveAsPosition(pos);
    }

    /**
     * Method invoked when the GameObject unsuccessfully tried to move horizontally
     *
     * @param delta The difference between the current position and the new position that could not be applied
     */
    public void onBounceX(Vector<Double> delta) {}

    /**
     * Method invoked when the GameObject unsuccessfully tried to move vertically
     *
     * @param delta The difference between the current position and the new position that could not be applied
     */
    public void onBounceY(Vector<Double> delta) {}
}
