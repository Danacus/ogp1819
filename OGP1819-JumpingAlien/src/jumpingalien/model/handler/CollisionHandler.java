package jumpingalien.model.handler;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Model;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.gameobject.ICollidable;
import jumpingalien.model.world.Tile;
import jumpingalien.model.World;
import jumpingalien.util.Bounds;
import jumpingalien.util.GameTimer;
import jumpingalien.util.Sprite;
import jumpingalien.util.Vector;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * A Handler that handles event based collision between GameObjects and
 * other objects implementing {@link ICollidable}
 */
public abstract class CollisionHandler extends Handler {

    public CollisionHandler(GameObject gameObject) {
        super(gameObject);
        this.interactionTimer = new GameTimer(0.6) {
            @Override
            public void onTick() {
                stop();
            }
        };
    }

    /**
     * The CollisionHandler will check collision every pixel update
     */
    public void pixelUpdate(double dt) {
        getInteractionTimer().update(dt);
        checkCollision(getGameObject().getHandler(MovementHandler.class).getPixelPosition());
    }

    /**
     * Copies a tile section from the given tile array to a new one
     *
     * @param worldTiles The array of tiles
     * @param startX
     * @param endX
     * @param startY
     * @param endY
     * @return A section of the given array
     */
    public static Tile[][] copyTileSection(Tile[][] worldTiles, int startX, int endX, int startY, int endY) {
        Tile[][] tiles = new Tile[endX - startX + 1][endY - startY + 1];

        for (int x = startX; x <= endX; x++) {
            for (int y = startY; y <= endY; y++) {
                tiles[endX - x][endY - y] = worldTiles[x][y];
            }
        }

        return tiles;
    }

    public Tile[][] getIntersectingTiles(Vector<Integer> pixelPosition){
        AnimationHandler animationHandler = getGameObject().getHandler(AnimationHandler.class);
        return getIntersectingTiles(pixelPosition, animationHandler.getCurrentSprite());
    }

    public Tile[][] getIntersectingTiles() {
        return getIntersectingTiles(getGameObject().getHandler(AnimationHandler.class).getCurrentSprite());
    }

    public Tile[][] getIntersectingTiles(Sprite sprite) {
        MovementHandler movementHandler = getGameObject().getHandler(MovementHandler.class);
        Vector<Integer> pixelPosition = movementHandler.getPosition().actualToPixel();
        return getIntersectingTiles(pixelPosition, sprite);
    }

    public Tile[][] getIntersectingTiles(Bounds bounds) {
        return getIntersectingTiles(bounds.getPosition(), bounds);
    }

    /**
     * Gets the tiles that intersect with the given sprite at the given position
     * @param pixelPosition
     * @param sprite
     * @return
     */
    public Tile[][] getIntersectingTiles(Vector<Integer> pixelPosition, Sprite sprite) {
        return getIntersectingTiles(pixelPosition, getGameObject().getBounds(sprite));
    }

    public Tile[][] getIntersectingTiles(Vector<Integer> pixelPosition, Bounds bounds) {
        World world = getGameObject().getWorld();

        if (world == null)
            return new Tile[][] {};

        int startX = (int)Math.floor((double)(pixelPosition.getX()) / (double)(world.getTileSize()));
        int endX = (int)Math.floor((double)(pixelPosition.getX() + bounds.getWidth() - 1) / (double)(world.getTileSize()));
        startX = Math.max(startX, 0);
        endX = Math.min(endX, world.getNbTilesX() - 1);
        startX = Math.min(startX, endX);

        int startY = (int)Math.floor((double)(pixelPosition.getY() + 1) / (double)(world.getTileSize()));
        int endY = (int)Math.floor((double)(pixelPosition.getY() + bounds.getHeight() - 1) / (double)(world.getTileSize()));
        startY = Math.max(startY, 0);
        endY = Math.min(endY, world.getNbTilesY() - 1);
        startY = Math.min(startY, endY);

        return copyTileSection(world.getTiles(), startX, endX, startY, endY);
    }

    public Collection<GameObject> getIntersectingGameObjects(Function<GameObject, Bounds> boundsFunction) {
        return getIntersectingGameObjects(getGameObject().getHandler(AnimationHandler.class).getCurrentSprite(), boundsFunction);
    }

    public Collection<GameObject> getIntersectingGameObjects(Vector<Integer> pixelPos, Function<GameObject, Bounds> boundsFunction) {
        return getIntersectingGameObjects(pixelPos, getGameObject().getHandler(AnimationHandler.class).getCurrentSprite(), boundsFunction);
    }

    public Collection<GameObject> getIntersectingGameObjects(Sprite sprite, Function<GameObject, Bounds> boundsFunction) {
        return getIntersectingGameObjects(new Vector<>((int)getGameObject().getBounds().getX(), (int)getGameObject().getBounds().getY()), sprite, boundsFunction);
    }

    public Collection<GameObject> getIntersectingGameObjects(Vector<Integer> pixelPos, Sprite sprite, Function<GameObject, Bounds> boundsFunction) {
        World world = getGameObject().getWorld();
        List<GameObject> gameObjectList = new ArrayList<>();

        if (world == null)
            return gameObjectList;

        for (GameObject other : world.getGameObjects()) {
            if (other == getGameObject())
                continue;
            if (boundsFunction.apply(other).intersects(getGameObject().getBounds(pixelPos, sprite)))
                gameObjectList.add(other);
        }

        return gameObjectList;
    }

    /**
     * Checks for collision and calls the {@link ICollidable#onCollision(ICollidable)},
     * {@link ICollidable#onCollisionEnter(ICollidable)} and {@link ICollidable#onCollisionExit(ICollidable)}
     * methods of the collidable GameObject for each other GameObject or Tile that collides with this GameObject if
     * the result of the bitwise and on the mask of the current GameObject and the category of the
     * other collidable is not equal to 0.
     */
    public void checkCollision(Vector<Integer> pixelPos) {
        if (getGameObject().getWorld() == null)
            return;

        oldColliders = new HashSet<>(colliders);
        colliders = getCollidersAt(pixelPos, (GameObject::getOuterBounds));

        for (ICollidable collidable : colliders) {
            if (!oldColliders.contains(collidable)) {
                getGameObject().onCollisionEnter(collidable);
            }
            getGameObject().onCollision(collidable);
        }

        for (ICollidable collidable : oldColliders) {
            if (!colliders.contains(collidable)) {
                getGameObject().onCollisionExit(collidable);
            }
        }
    }

    public Collection<ICollidable> getCollidersAt(Vector<Integer> pixelPos, Function<GameObject, Bounds> boundsFunction) {
        Collection<GameObject> gameObjects = getIntersectingGameObjects(pixelPos, boundsFunction);
        Collection<ICollidable> newColliders = new HashSet<>(gameObjects);

        Arrays.stream(getIntersectingTiles(pixelPos)).forEach(line -> newColliders.addAll(Arrays.asList(line)));
        return newColliders.stream().filter(other -> getGameObject().canCollideWith(other)).collect(Collectors.toSet());
    }

    /**
     * Returns a collection of ICollidable objects that currently intersect this GameObject
     */
    @Basic
    public Collection<ICollidable> getColliders() {
        return colliders;
    }

    private Collection<ICollidable> colliders = new HashSet<>();
    private Collection<ICollidable> oldColliders = new HashSet<>();

    /**
     * Start the interaction
     *
     * @pre | canInteract()
     * @effect | getInteractionTimer().start()
     */
    public void startInteraction() {
        assert canInteract();
        getInteractionTimer().start();
    }

    /**
     * Check whether the GameObject is allowed to interact
     */
    @Basic
    public boolean canInteract() {
        return !getInteractionTimer().isRunning();
    }

    /**
     * Allow interaction
     *
     * @post | new.canInteract() == true
     */
    public void allowInteraction() {
        getInteractionTimer().stop();
    }

    @Basic
    @Model
    @Immutable
    private GameTimer getInteractionTimer() {
        return interactionTimer;
    }

    private final GameTimer interactionTimer;

    /**
     * Event for continued collision between this GameObject and other collidable objects
     *
     * @param other The other collidable
     * @note   This method will only be invoked when the 2 collidables can collide
     *         ({@link ICollidable#canCollideWith(ICollidable)})
     *         Also worth noting that this method gets invoked by {@link GameObject},
     *         and not directly from {@link CollisionHandler}. CollisionHandler
     *         only invokes onCollision on ICollidable objects like GameObject.
     */
    public abstract void onCollision(ICollidable other);

    /**
     * Event when entering another collidable
     *
     * @param other The other collidable
     * @note   This method will only be invoked when the 2 collidables can collide
     *         ({@link ICollidable#canCollideWith(ICollidable)})
     *         Also worth noting that this method gets invoked by {@link GameObject},
     *         and not directly from {@link CollisionHandler}. CollisionHandler
     *         only invokes onCollisionEnter on ICollidable objects like GameObject.
     */
    public abstract void onCollisionEnter(ICollidable other);

    /**
     * Event when leaving another collidable
     *
     * @param other The other collidable
     * @note   This method will only be invoked when the 2 collidables can collide
     *         ({@link ICollidable#canCollideWith(ICollidable)})
     *         Also worth noting that this method gets invoked by {@link GameObject},
     *         and not directly from {@link CollisionHandler}. CollisionHandler
     *         only invokes onCollisionExit on ICollidable objects like GameObject.
     */
    public abstract void onCollisionExit(ICollidable other);

}
