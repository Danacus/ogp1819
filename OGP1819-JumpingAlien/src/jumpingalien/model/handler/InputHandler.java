package jumpingalien.model.handler;

import jumpingalien.model.gameobject.GameObject;

/**
 * A handler that handles input form the user
 */
public abstract class InputHandler extends Handler {
    public InputHandler(GameObject gameObject) {
        super(gameObject);
    }
}
