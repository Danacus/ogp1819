package jumpingalien.model.handler;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.util.exception.IllegalPositionException;
import jumpingalien.util.Vector;

/**
 * @invar This MovementHandler can have its position as its position.
 *      | this,canHaveAsPosition(this.getPosition())
 * @invar
 *      \ this.isWithinBounds(this.getPosition()) || getGameObject().isTerminated()
 */
public abstract class MovementHandler extends Handler {
    public MovementHandler(GameObject gameObject) throws IllegalPositionException {
        super(gameObject);
    }

    public abstract Vector<Double> getVelocity();
    public abstract void setVelocity(Vector<Double> velocity);

    public abstract Vector<Double> getAcceleration();
    public abstract void setAcceleration(Vector<Double> acceleration);

    /**
     * Returns the pixel position of this movement handler.
     * @return | result == getPosition().actualToPixel()
     */
    public Vector<Integer> getPixelPosition() {
        return getPosition().actualToPixel();
    }

    /**
     * Checks whether this is a valid position
     *
     * @param pos A vector containing a position.
     * @return | result == !Double.isNaN(pos.getX()) && !Double.isNaN(pos.getY());
     */
    public boolean canHaveAsPosition(Vector<Double> pos) {
        if (Double.isNaN(pos.getX()) || Double.isNaN(pos.getY()))
            return false;

        return true;
    }

    /**
     * Check whether this position is within the bounds of the world.
     *
     * @param pos The position to check.
     *
     * @return True if and only if this position is within the bounds of the world or the world is still undefined or nonexistent.
     *       | if (getGameObject().getWorld() != null) then
     *       |      result == y >= 0 &&
     *       |           y <= getGameObject().getWorld().getWorld().pixelToActual().getY() &&
     *       |           x >= 0 &&
     *       |           x <= getGameObject().getWorld().pixelToActual().getX()
     *       | else
     *       |      result == true
     */
    public boolean isWithinBounds(Vector<Double> pos) {
        double x = pos.getX();
        double y = pos.getY();

        Vector<Double> MIN_POSITION = Vector.ACTUAL_ZERO;
        Vector<Double> MAX_POSITION = Vector.ACTUAL_INFINITY;

        if (getGameObject().getWorld() != null)
            MAX_POSITION = getGameObject().getWorld().getWorldPixelDimensions().pixelToActual();

        if (y < MIN_POSITION.getY()
                || y > MAX_POSITION.getY()
                || x < MIN_POSITION.getX()
                || x > MAX_POSITION.getX())
            return false;

        return true;
    }

    /**
     * Return the position of this Mazub.
     */
    @Basic
    @Raw
    public Vector<Double> getPosition() {
        return this.position;
    }

    /**
     * Set the position of this GameObject to the given position.
     *
     * @param  position
     *         The new position for this GameObject.
     *
     * @post
     *      | if (isWithinBounds(position))
     *      |     this.position = position;
     *      | else
     *      |     getGameObject().terminate();
     * @throws IllegalPositionException
     *       | !canHaveAsPosition(position)
     */
    @Raw
    public void setPosition(Vector<Double> position) throws IllegalPositionException {
        if (!isWithinBounds(position)) {
            getGameObject().terminate();
            return;
        }
        if (!canHaveAsPosition(position))
            throw new IllegalPositionException("Invalid position");
        this.position = position;
    }

    /**
     * Variable registering the position of this GameObject.
     */
    private Vector<Double> position;
}
