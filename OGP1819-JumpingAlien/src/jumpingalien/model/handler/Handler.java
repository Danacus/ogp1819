package jumpingalien.model.handler;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.util.exception.GameObjectException;
import jumpingalien.model.gameobject.GameObject;

/**
 * The top level Handler that every handler inherits
 * It stores a reference to the {@link GameObject} that owns the handler.
 * It also serves as a "root class" of a the {@link jumpingalien.util.ClassTreeMap} stored in GameObjects.
 */
public abstract class Handler {
    /**
     * Creates a new Handler
     *
     * @param gameObject The GameObject that owns this Handler
     *
     * @post | new.getGameObject() == gameObject
     */
    public Handler(GameObject gameObject) throws GameObjectException {
        this.gameObject = gameObject;
    }

    /**
     * Main update loop for Handlers, automatically invoked by {@link GameObject}
     * @param dt   The amount of time to advance
     */
    public void update(double dt) {}

    /**
     * The GameObject that owns this Handler
     */
    private final GameObject gameObject;

    /**
     * Returns the GameObject that owns this Handler.
     */
    @Basic
    @Immutable
    public GameObject getGameObject() {
        return gameObject;
    }

    public void pixelUpdate(double dt) {
    }
}
