package jumpingalien.model.handler;

import be.kuleuven.cs.som.annotate.Model;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.gameobject.ICollidable;
import jumpingalien.model.world.GeologicalFeature;
import jumpingalien.model.world.Tile;
import jumpingalien.model.world.GeologicalEffect;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A class for handling the effects of GameObjects colliding with geological features
 *
 * @invar | hasProperEffects()
 */
public class WorldCollisionHandler extends CollisionHandler{
    public WorldCollisionHandler(GameObject gameObject) {
        super(gameObject);
    }

    /**
     * Adds a new effect to this world collision handler
     *
     * @param feature The geological feature that triggers the effect
     * @param effect  The geological effect
     *
     * @post | getEffect(feature) == effect
     */
    public void setEffect(GeologicalFeature feature, GeologicalEffect effect) {
        if (!canHaveAsEffect(effect))
            throw new IllegalArgumentException("Invalid effect");
        effectMap.put(feature, effect);
    }

    /**
     * Returns the effect of the given feature
     *
     * @param feature  A feature
     */
    public GeologicalEffect getEffect(GeologicalFeature feature) {
        return effectMap.get(feature);
    }

    /**
     * Checks whether this WorldCollisionHandler has an effect for the given feature
     *
     * @param feature The feature to check
     */
    public boolean hasFeature(GeologicalFeature feature) {
        return effectMap.containsKey(feature);
    }

    /**
     * Checks whether this WorldCollisionHandler has the given effect for a feature
     *
     * @param effect The effect to check
     */
    public boolean hasEffect(GeologicalEffect effect) {
        return effectMap.containsValue(effect);
    }

    /**
     * Checks whether this WorldCollisionHandler can have the given effect as its effect
     *
     * @param effect  The effect to check
     *
     * @return | effect != null
     */
    public boolean canHaveAsEffect(GeologicalEffect effect) {
        return effect != null;
    }

    /**
     * Checks whether this WorldCollisionHandler can have the given effect as its effect
     *
     * @return | for each effect in getEffectMap().values():
     *         |    canHaveAsEffect(effect)
     */
    public boolean hasProperEffects() {
        return effectMap.values().stream().allMatch(this::canHaveAsEffect);
    }

    /**
     * Returns the effect map
     */
    public Map<GeologicalFeature, GeologicalEffect> getEffectMap() {
        return new HashMap<>(effectMap);
    }

    /**
     * Variable for storing the effects of the different geological features on the GameObject
     */
    private Map<GeologicalFeature, GeologicalEffect> effectMap = new LinkedHashMap<>();

    /**
     * Updates all effects that should be active
     *
     * @param dt The time delta
     */
    @Override
    public void pixelUpdate(double dt) {
        super.pixelUpdate(dt);

        for (GeologicalEffect effect : effectMap.values()) {
            if (effect.getTileCount() > 0) {
                effect.update(dt);
                break;
            } else {
                effect.setActivated(false);
                effect.stop();
            }
        }
    }

    @Override
    public void onCollision(ICollidable other) {

    }

    /**
     * Event for entering collision with another collidable object
     *
     * @param other The other collidable
     *
     * @effect | let
     *         |    feature == ((Tile) other).getGeologicalFeature()
     *         | in
     *         |    if (hasFeature(feature)) then getEffect(feature).addTile()
     */
    @Override
    public void onCollisionEnter(ICollidable other) {
        if (other instanceof Tile) {
            GeologicalFeature feature = ((Tile) other).getGeologicalFeature();

            if (hasFeature(feature))
                getEffect(feature).addTile();
        }
    }

    /**
     * Event for exiting collision with another collidable object
     *
     * @param other The other collidable
     *
     * @effect | let
     *         |    feature == ((Tile) other).getGeologicalFeature()
     *         | in
     *         |    if (hasFeature(feature)) then getEffect(feature).removeTile()
     */
    @Override
    public void onCollisionExit(ICollidable other) {
        if (other instanceof Tile) {
            GeologicalFeature feature = ((Tile) other).getGeologicalFeature();

            if (hasFeature(feature))
                getEffect(feature).removeTile();
        }
    }
}
