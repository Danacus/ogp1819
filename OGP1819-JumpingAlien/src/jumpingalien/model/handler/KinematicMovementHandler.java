package jumpingalien.model.handler;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.util.exception.IllegalPositionException;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.util.Vector;

/**
 * A handler for GameObjects that move without getting blocked by other ICollidables and that are not
 * affected by gravity.
 */
public abstract class KinematicMovementHandler extends MovementHandler {
    public KinematicMovementHandler(GameObject gameObject) throws IllegalPositionException {
        super(gameObject);
    }

    /**
     * A variable to store the velocity of this plant.
     */
    private Vector<Double> velocity;

    /**
     * Returns the velocity.
     */
    @Override
    @Basic
    public Vector<Double> getVelocity() {
        return velocity;
    }

    /**
     * Set the velocity.
     *
     * @post | new.getVelocity() == velocity
     */
    @Override
    @Raw
    public void setVelocity(Vector<Double> velocity) {
        this.velocity = velocity;
    }

    /**
     * Returns the acceleration
     */
    @Override
    @Basic
    public Vector<Double> getAcceleration() {
        return acceleration;
    }

    /**
     * Set the acceleration of this plant.
     *
     * @param acceleration The new acceleration
     *
     * @post | new.getAcceleration() == acceleration
     */
    @Override
    @Raw
    public void setAcceleration(Vector<Double> acceleration) {
        this.acceleration = acceleration;
    }

    private Vector<Double> acceleration;

    /**
     * Update the position, velocity and acceleration of the GameObject.
     *
     * @param dt The time that has passed since the last update.
     *
     * @effect | if (!getGameObject().isDead()
     *         |    setPosition(getPosition().add(getVelocity().scl(dt)).add(getAcceleration().scl(Math.pow(dt, 2) / 2)));
     *         |    setVelocity(getVelocity().add(getAcceleration().scl(dt)));
     */
    public void pixelUpdate(double dt) {
        if (getGameObject().isDead()) return;

        setPosition(getPosition().add(getVelocity().scl(dt)).add(getAcceleration().scl(Math.pow(dt, 2) / 2)));
        setVelocity(getVelocity().add(getAcceleration().scl(dt)));
    }
}
