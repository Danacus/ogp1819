package jumpingalien.model.world;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.util.GameTimer;

/**
 * A class used to describe and apply various effects that might be caused by terrain interaction
 *
 * @invar The GameObject of this geological effect is not null
 *      | getGameObject() != null
 */
public abstract class GeologicalEffect extends GameTimer {
    /**
     * Creates a new geological effect
     *
     * @param gameObject  The GameObject
     * @param interval    The interval
     * @param tickFirst   Should this effect trigger immediately?
     *
     * @effect | super(interval, tickFirst)
     * @post   | new.getGameObject() == gameObject
     *
     * @throws IllegalArgumentException
     *       | gameObject == null
     */
    public GeologicalEffect(GameObject gameObject, double interval, boolean tickFirst) throws IllegalArgumentException {
        super(interval, tickFirst);
        if (gameObject == null)
            throw new IllegalArgumentException("GameObject must not be null!");
        this.gameObject = gameObject;
    }

    /**
     * Returns the GameObject that this effect is applied to
     */
    @Basic
    @Immutable
    protected GameObject getGameObject() {
        return gameObject;
    }

    /**
     * Variable to store the GameObject
     */
    private final GameObject gameObject;

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    /**
     * Returns whether the effect is activated
     * @return
     */
    @Basic
    public boolean isActivated() {
        return activated;
    }

    /**
     * Variable to store whether the timer is activated
     */
    private boolean activated;

    /**
     * Increments the amount of tiles of this feature intersecting the GameObject
     *
     * @post | new.getTileCount() == getTileCount() + 1
     */
    public void addTile() { tileCount++; }

    /**
     * Decrements the amount of tiles of this feature intersecting the GameObject
     *
     * @post | new.getTileCount() == getTileCount() - 1
     */
    public void removeTile() { tileCount--; }

    /**
     * Returns the amount of tiles of this feature intersecting the GameObject
     */
    @Basic
    public int getTileCount() {
        return tileCount;
    }

    /**
     * Variable to store the amount of tiles
     */
    private int tileCount;

    /**
     * Updates the effect timer
     *
     * @param dt The delta time
     *
     * @effect   | if (!isActivated())
     *           |    reset()
     *           |    setActivated(true)
     *           |    start()
     * @effect   | super.update(dt)
     */
    @Override
    public void update(double dt) {
        if (!isActivated()) {
             reset();
             setActivated(true);
             start();
        }

        super.update(dt);
    }
}
