package jumpingalien.model.world;

import jumpingalien.model.gameobject.GameObject;

public class DamageEffect extends GeologicalEffect {
    public DamageEffect(GameObject gameObject, double interval, boolean tickFirst, int damage) {
        super(gameObject, interval, tickFirst);
        this.damage = damage;
    }

    private final int damage;

    /**
     * @effect | gameObject.takeDamage(damage)
     */
    @Override
    public void onTick() {
        getGameObject().takeDamage(damage);
    }
}
