package jumpingalien.model.world;

/**
 * Class that stores all collision masks and categories
 */
public class Collision {
    public static class Mask {
        public static final byte MAZUB = Category.PLANT | Category.SHARK | Category.SLIME | Category.TILE;
        public static final byte PLANT = Category.MAZUB;
        public static final byte SHARK = Category.SLIME | Category.SHARK | Category.MAZUB | Category.TILE;
        public static final byte SLIME = Category.SHARK | Category.SLIME | Category.MAZUB | Category.TILE;
        public static final byte TILE  = Category.MAZUB | Category.SLIME | Category.SHARK;
    }

    public static class Category {
        public static final byte MAZUB = 0b00000001;
        public static final byte PLANT = 0b00000010;
        public static final byte SHARK = 0b00000100;
        public static final byte SLIME = 0b00001000;
        public static final byte TILE  = 0b00010000;
    }
}
