package jumpingalien.model.world;

public enum GeologicalFeature {
    AIR(0, true),
    SOLID_GROUND(1, false),
    WATER(2, true),
    MAGMA(3, true),
    ICE(4, false),
    GAS(5, true);

    GeologicalFeature(int value, boolean passable) {
        this.value = value;
        this.isPassable = passable;
    }

    public int getValue() {
        return value;
    }

    private final int value;

    public boolean isPassable() {
        return isPassable;
    }

    private final boolean isPassable;
}
