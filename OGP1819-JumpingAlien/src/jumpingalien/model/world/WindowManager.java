package jumpingalien.model.world;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.model.World;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.handler.MovementHandler;
import jumpingalien.util.Vector;

/**
 * Class that manages the visible window of the game
 *
 * @invar  The visible window of each world must be a valid visible window for any
 *         world.
 *       | isValidVisibleWindow(getVisibleWindow())
 * @invar  The visible window position of each World must be a valid visible window position for any
 *         World.
 *       | isValidVisibleWindowPosition(getVisibleWindowPosition())
 */
public class WindowManager {
    /**
     * Creates a new window manager
     *
     * @param world the world for this WindowManager.
     *
     * @post | new.getWorld() == world
     *
     * @throws IllegalArgumentException
     *      | world == null
     */
    public WindowManager(World world) throws IllegalArgumentException {
        if (world == null)
            throw new IllegalArgumentException("Illegal world");

        this.world = world;
    }

    /**
     * Returns the world of this WindowManager.
     */
    @Immutable
    @Basic
    @Raw
    public World getWorld() {
        return this.world;
    }

    private final World world;

    /**
     * Return the target tile.
     */
    @Immutable
    @Basic
    @Raw
    public GameObject getTarget() {
        return this.target;
    }

    /**
     *
     * @param target the object to set the target to.
     *
     * @post | if (target != null) new.target == target
     */
    public void setTarget(GameObject target) {
        if (target != null) {
            this.target = target;
            updateVisibleWindowPosition();
        }
    }

    /**
     * Variable registering the target object.
     */
    private GameObject target;

    /**
     * Updates the window manager
     *
     * @param dt the advanced time since last update.
     */
    public void update(double dt) {
        updateVisibleWindowPosition();
    }

    /**
     * Updates the position of the visible window
     */
    private void updateVisibleWindowPosition(){
        if (getTarget() == null) return;
        MovementHandler movementHandler = getTarget().getHandler(MovementHandler.class);
        if (movementHandler == null) return;

        try {
            setVisibleWindowPosition(new Vector<>(getVisibleWindowPosition().getX(), movementHandler.getPosition().actualToPixel().getY() - getVisibleWindow().getY()/2));
        } catch (Exception ignored) {}
        try {
            setVisibleWindowPosition(new Vector<>(movementHandler.getPosition().actualToPixel().getX() - getVisibleWindow().getX()/2, getVisibleWindowPosition().getY()));
        } catch (Exception ignored) {}
    }

    /**
     * Return the visible window of this world.
     */
    @Basic
    @Raw
    public Vector<Integer> getVisibleWindow() {
        return this.visibleWindow;
    }

    /**
     * Check whether the given visible window is a valid visible window for
     * any world.
     *
     * @param  visibleWindow
     *         The visible window to check.
     * @return
     *       | result == true
     */
    public static boolean isValidVisibleWindow(Vector<Integer> visibleWindow) {
        return true;
    }

    /**
     * Set the visible window of this world to the given visible window.
     *
     * @param  visibleWindow
     *         The new visible window for this world.
     * @post   The visible window of this new world is equal to
     *         the given visible window.
     *       | new.getVisibleWindow() == visibleWindow
     * @throws IllegalArgumentException
     *         The given visible window is not a valid visible window for any
     *         world.
     *       | ! isValidVisibleWindow(getVisibleWindow())
     */
    @Raw
    public void setVisibleWindow(Vector<Integer> visibleWindow)
            throws IllegalArgumentException {
        if (! isValidVisibleWindow(visibleWindow))
            throw new IllegalArgumentException();
        this.visibleWindow = visibleWindow;
    }

    /**
     * Variable registering the visible window of this world.
     */
    private Vector<Integer> visibleWindow;

    /**
     * Return the visible window position of this World.
     */
    @Basic @Raw
    public Vector<Integer> getVisibleWindowPosition() {
        return this.visibleWindowPosition;
    }

    /**
     * Check whether the given visible window position is a valid visible window position for
     * any World.
     *
     * @param  visibleWindowPosition
     *         The visible window position to check.
     * @return ...
     *       | result == visibleWindowPosition.getX() >= 0 &&
     *                 visibleWindowPosition.getY() >= 0 &&
     *                 visibleWindowPosition.getX() <= getWorldSize().getX() - getVisibleWindow().getX()&&
     *                 visibleWindowPosition.getY() <= getWorldSize().getY() - getVisibleWindow().getY()
     */
    public boolean isValidVisibleWindowPosition(Vector<Integer> visibleWindowPosition) {
        return visibleWindowPosition.getX() >= 0 &&
                visibleWindowPosition.getY() >= 0 &&
                visibleWindowPosition.getX() <= getWorld().getWorldPixelDimensions().getX() - getVisibleWindow().getX()&&
                visibleWindowPosition.getY() <= getWorld().getWorldPixelDimensions().getY() - getVisibleWindow().getY();
    }

    /**
     * Set the visible window position of this World to the given visible window position.
     *
     * @param  visibleWindowPosition
     *         The new visible window position for this World.
     * @post   The visible window position of this new World is equal to
     *         the given visible window position.
     *       | new.getVisibleWindowPosition() == visibleWindowPosition
     * @throws IllegalArgumentException
     *         The given visible window position is not a valid visible window position for any
     *         World.
     *       | ! isValidVisibleWindowPosition(getVisibleWindowPosition())
     */
    @Raw
    public void setVisibleWindowPosition(Vector<Integer> visibleWindowPosition)
            throws IllegalArgumentException {
        if (! isValidVisibleWindowPosition(visibleWindowPosition))
            throw new IllegalArgumentException("Illegal window position");
        this.visibleWindowPosition = visibleWindowPosition;
    }

    /**
     * Variable registering the visible window position of this World.
     */
    private Vector<Integer> visibleWindowPosition;
}
