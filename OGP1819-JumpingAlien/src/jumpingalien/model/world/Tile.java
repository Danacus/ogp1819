package jumpingalien.model.world;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import be.kuleuven.cs.som.annotate.Value;
import jumpingalien.model.gameobject.ICollidable;
import jumpingalien.util.Bounds;
import jumpingalien.util.Sprite;
import jumpingalien.util.Vector;

import java.awt.*;

/**
 * @invar  Each Tile can have its geological feature as geological feature .
 *       | canHaveAsGeologicalFeature(this.getGeologicalFeature())
 * @invar  Each Tile can have its position as position .
 *       | canHaveAsPosition(this.getPosition())
 * @invar  Each Tile can have its coordinates as coordinates .
 *       | canHaveAsCoordinates(this.getCoordinates())
 * @invar  Each Tile can have its tile size as its tile size
 *      | isValidTileSize(getTileSize())
 */
@Value
public class Tile implements ICollidable {
    /**
     * Initialize this new Tile with given geological feature and the given position.
     *
     * @param  feature
     *         The geological feature for this new Tile.
     * @param  position
     *         The position for this new Tile.
     * @param  coordinates
     *         The coordinates for this new Tile.
     * @param tileSize
     *         The size of the tile
     * @post   The new coordinates of this tile are equal to the given coordinates
     *       | new.getCoordinates() == coordinates
     * @post   If the given geological feature is a valid geological feature for any Tile,
     *         the geological feature of this new Tile is equal to the given
     *         geological feature. Otherwise, the geological feature of this new Tile is equal
     *         to GeologicalFeature.AIR.
     *       | if (isValidGeologicalFeature(geologicalFeature))
     *       |   then new.getGeologicalFeature() == geologicalFeature
     *       |   else new.getGeologicalFeature() == GeologicalFeature.AIR
     * @post   The new position of this tile is equal to the given position
     *       | new.getPosition() == position
     * @post   If the given tile size is a valid tile size for any tile,
     *         the tile size of this tile is set to the given tile size,
     *         otherwise it will be equal to 0
     *       | if (isValidTileSize(tileSize))
     *       |   then new.getTileSize() == tileSize
     *       |   else new.getTileSize() == 0
     */
    public Tile(GeologicalFeature feature, Vector<Double> position, Vector<Integer> coordinates, int tileSize) {
        if (!canHaveAsGeologicalFeature(feature))
            feature = GeologicalFeature.AIR;
        this.geologicalFeature = feature;
        this.coordinates = coordinates;
        this.position = position;

        if (!isValidTileSize(tileSize))
            tileSize = 0;
        this.tileSize = tileSize;
    }

    /**
     * Returns the size of this tile
     */
    @Basic
    @Immutable
    public int getTileSize() {
        return tileSize;
    }

    /**
     * Checks whether the given tile size is a valid size for this tile
     *
     * @param tileSize The given size
     *
     * @return  True if and only if the given size is greater than zero
     */
    public static boolean isValidTileSize(int tileSize) {
        return tileSize > 0;
    }

    /**
     * Variable to store the size of this tile
     */
    private final int tileSize;

    /**
     * Return the position of this Tile.
     */
    @Basic @Raw @Immutable
    public Vector<Double> getPosition() {
        return this.position;
    }

    /**
     * Variable registering the position of this Tile.
     */
    private final Vector<Double> position;

    /**
     * Return the coordinates of this Tile.
     */
    @Basic @Raw @Immutable
    public Vector<Integer> getCoordinates() {
        return this.coordinates;
    }

    /**
     * Variable registering the coordinates of this Tile.
     */
    private final Vector<Integer> coordinates;

    /**
     * Returns the geological feature of this Tile.
     */
    @Basic
    @Raw @Immutable
    public GeologicalFeature getGeologicalFeature() {
        return this.geologicalFeature;
    }

    /**
     * Creates a new tile based on this tile with the given geological feature
     *
     * @param geologicalFeature  The geological feature of this tile
     *
     * @return | result == new Tile(geologicalFeature, position, coordinates, tileSize)
     */
    @Basic
    @Raw
    public Tile setGeologicalFeature(GeologicalFeature geologicalFeature) {
        return new Tile(geologicalFeature, position, coordinates, tileSize);
    }

    /**
     * Check whether this Tile can have the given geological feature as its geological feature.
     *
     * @param  geologicalFeature
     *         The geological feature to check.
     * @return
     *       | result == geologicalFeature != null
     */
    @Raw
    public boolean canHaveAsGeologicalFeature(GeologicalFeature geologicalFeature) {
        return geologicalFeature != null;
    }

    /**
     * Variable registering the geological feature of this Tile.
     */
    private final GeologicalFeature geologicalFeature;

    @Override
    public String toString() {
        return getPosition().scl(1 / (double)tileSize / Vector.getPPM()).actualToPixel().toString();
    }

    /**
     *
     * @return | result == getBounds(getPosition().actualToPixel())
     */
    @Override
    public Bounds getBounds(){
        return getBounds(getPosition().actualToPixel());
    }

    /**
     *
     * @param pixelPos
     * @return | result == getBounds(pixelPos, null)
     */
    @Override
    public Bounds getBounds(Vector<Integer> pixelPos) {
        return getBounds(pixelPos, null);
    }

    /**
     *
     * @param sprite
     * @return | result == getBounds()
     */
    @Override
    public Bounds getBounds(Sprite sprite) {
        return getBounds();
    }

    /**
     *
     * @param pixelPos
     * @param sprite
     * @return | result == new Rectangle((int)pixelPos.getX(), (int)pixelPos.getY(), getTileSize(), getTileSize() - 1);
     */
    @Override
    public Bounds getBounds(Vector<Integer> pixelPos, Sprite sprite) {
        return new Bounds(pixelPos, new Vector<>(getTileSize(), getTileSize() - 1));
    }

    @Override
    public void onCollision(ICollidable other) {

    }

    @Override
    public void onCollisionEnter(ICollidable other) {

    }

    @Override
    public void onCollisionExit(ICollidable other) {

    }

    /**
     * @return | result == Collision.Category.TILE
     */
    @Basic
    @Immutable
    @Override
    public byte getCategory() {
        return Collision.Category.TILE;
    }

    /**
     * @return | result == Collision.Mask.TILE
     */
    @Basic
    @Immutable
    @Override
    public byte getMask() {
        return Collision.Mask.TILE;
    }

    /**
     * @return | !getGeologicalFeature().isPassable()
     */
    @Override
    public boolean isSolid() {
        return !getGeologicalFeature().isPassable();
    }
}
