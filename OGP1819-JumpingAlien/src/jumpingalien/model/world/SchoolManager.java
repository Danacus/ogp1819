package jumpingalien.model.world;

import jumpingalien.model.School;
import jumpingalien.model.World;

import java.util.*;

public class SchoolManager {

    public Set<School> getSchools() {
        return schools;
    }

    //TODO, max 10 schools
    // Alles met schools moet defensief
    private Set<School> schools = new HashSet<>();

    public static Set<Long> idSet = new HashSet<>();

    public static void registerSlimeId(long id){
        idSet.add(id);
    }

    public static boolean isValidId(long id){
        return !idSet.contains(id) && id >= 0;
    }

    public static void cleanAllId(){
        idSet.clear();
    }


    private World world;
    public SchoolManager(World world){
        this.world = world;
    }

    public void addSchool(School school) throws IllegalArgumentException{
        if(getSchools().size() >= 10) throw new IllegalArgumentException("School limit reached!");
        schools.add(school);
    }





}
