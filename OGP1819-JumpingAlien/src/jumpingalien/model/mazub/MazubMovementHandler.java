package jumpingalien.model.mazub;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.model.Mazub;
import jumpingalien.util.exception.IllegalPositionException;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.model.handler.DynamicMovementHandler;
import jumpingalien.util.Vector;

/**
 * A class for handling the position of a Mazub.
 *
 * @invar The GameObject of this Handler will always be a Mazub
 *     |  getGsmeObject() instanceof Mazub
 * @invar  Each MazubMovementHandler can have its velocity as velocity.
 *       | canHaveAsVelocity(this.getVelocity())
 *
 * @invar  Each MazubMovementHandler can have its maximum velocity while standing as maximum velocity while standing.
 *       | canHaveAsMaxVelocityStanding(this.getMaxVelocityStanding())
 *
 * @invar  Each MazubMovementHandler can have its minimum velocity while standing as minimum velocity while standing.
 *       | canHaveAsMinVelocityStanding(this.getMinVelocityStanding())
 *
 * @invar  Each MazubMovementHandler can have its maximum velocity while ducking as maximum velocity while ducking.
 *       | canHaveAsMaxVelocityDucking(this.getMaxVelocityDucking())
 *
 * @invar  Each MazubMovementHandler can have its minimum velocity while ducking as minimum velocity while ducking.
 *       | canHaveAsMinVelocityDucking(this.getMinVelocityDucking())
 */
public class MazubMovementHandler extends DynamicMovementHandler {

    /**
     * Initialize this movement handler with the following properties.
     *
     * @param initPos     The initial position of the Mazub.
     * @param initVel     The initial position of the Mazub.
     * @param initAcc     The initial acceleration of the Mazub.
     * @param minVelStand The minimum velocity of the Mazub while standing.
     * @param maxVelStand The maximum velocity of the Mazub while standing.
     * @param minVelDuck  The minimum velocity of the Mazub while ducking.
     * @param maxVelDuck  The maximum velocity of the Mazub while ducking.
     * @post If the Mazub can have the given minimum velocity while standing as minimum velocity while standing,
     * the minimum velocity while standing will be set to the given value,
     * otherwise it will be set to the zero vector.
     * | if (canHaveAsMinVelocityStanding(minVelStand))
     * |      new.getMinVelocityStanding() == minVelStand;
     * | else
     * |      new.getMinVelocityStanding() == Vector.ZERO
     * @post If the Mazub can have the given maximum velocity while standing as maximum velocity while standing,
     * the maximum velocity while standing will be set to the given value,
     * otherwise it will be set to the zero vector.
     * | if (canHaveAsMaxVelocityStanding(maxVelStand))
     * |      new.getMaxVelocityStanding() == maxVelStand;
     * | else
     * |      new.getMinVelocityStanding() == Vector.ZERO
     * @post If the Mazub can have the given minimum velocity while ducking as minimum velocity while ducking,
     * the minimum velocity while ducking will be set to the given value,
     * otherwise it will be set to the zero vector.
     * | if (canHaveAsMinVelocityDucking(minVelDuck))
     * |      new.getMinVelocityDucking() == minVelDuck;
     * | else
     * |      new.getMinVelocityStanding() == Vector.ZERO
     * @post If the Mazub can have the given maximum velocity while ducking as maximum velocity while ducking,
     * the maximum velocity while ducking will be set to the given value,
     * otherwise it will be set to the zero vector.
     * | if (canHaveAsMaxVelocityDucking(maxVelDuck))
     * |      new.getMaxVelocityDucking() == maxVelDuck;
     * | else
     * |      new.getMinVelocityStanding() == Vector.ZERO
     * @effect The initial position will be set to the given position
     * | this.setPosition(initPos)
     * @effect The initial velocity will be set to the given velocity
     * | this.setPosition(initVel)
     * @effect The initial acceleration will be set to the given acceleration
     * | this.setPosition(initAcc)
     */
    public MazubMovementHandler(Mazub mazub, Vector<Double> initPos, Vector<Double> initVel, Vector<Double> initAcc, Vector<Double> minVelStand, Vector<Double> maxVelStand, Vector<Double> minVelDuck, Vector<Double> maxVelDuck) throws IllegalPositionException {
        super(mazub);
        if (canHaveAsMinVelocityStanding(minVelStand))
            this.minVelocityStanding = minVelStand;
        else
            this.minVelocityStanding = Vector.ACTUAL_ZERO;

        if (canHaveAsMaxVelocityStanding(maxVelStand))
            this.maxVelocityStanding = maxVelStand;
        else
            this.maxVelocityStanding = Vector.ACTUAL_ZERO;

        if (canHaveAsMinVelocityDucking(minVelDuck))
            this.minVelocityDucking = minVelDuck;
        else
            this.minVelocityDucking = Vector.ACTUAL_ZERO;

        if (canHaveAsMaxVelocityDucking(maxVelDuck))
            this.maxVelocityDucking = maxVelDuck;
        else
            this.maxVelocityDucking = Vector.ACTUAL_ZERO;

        this.setPosition(initPos);
        this.setVelocity(initVel);
        this.setAcceleration(initAcc);
    }

    /**
     * Return the maximum velocity while standing of this Mazub.
     */
    @Basic
    @Raw
    @Immutable
    public Vector<Double> getMaxVelocityStanding() {
        return this.maxVelocityStanding;
    }

    /**
     * Check whether this Mazub can have the given maximum velocity while standing as its maximum velocity while standing.
     *
     * @param maxVelocityStanding The maximum velocity while standing to check.
     * @return True if and only if the given value's x component is greater than the minimum x velocity
     * and the given value's y component is greater than the minimum y velocity
     * | result == maxVelocityStanding.getX() >= getMinVelocityStanding().getX()
     * |        && maxVelocityStanding.getY() >= getMinVelocityStanding().getY()
     */
    @Raw
    public boolean canHaveAsMaxVelocityStanding(Vector<Double> maxVelocityStanding) {
        return maxVelocityStanding.getX() >= getMinVelocityStanding().getX()
                && maxVelocityStanding.getY() >= getMinVelocityStanding().getY();
    }

    /**
     * Variable registering the maximum velocity while standing of this Mazub.
     */
    private final Vector<Double> maxVelocityStanding;

    /**
     * Return the minimum velocity while standing of this Mazub.
     */
    @Basic
    @Raw
    @Immutable
    public Vector<Double> getMinVelocityStanding() {
        return this.minVelocityStanding;
    }

    /**
     * Check whether this Mazub can have the given minimum velocity while standing as its minimum velocity while standing.
     *
     * @param minVelocityStanding The minimum velocity while standing to check.
     * @return Always true
     * | result == true
     */
    @Raw
    public boolean canHaveAsMinVelocityStanding(Vector<Double> minVelocityStanding) {
        return true;
    }

    /**
     * Variable registering the minimum velocity while standing of this Mazub.
     */
    private final Vector<Double> minVelocityStanding;


    /**
     * Return the maximum velocity while standing of this Mazub.
     */
    @Basic
    @Raw
    @Immutable
    public Vector<Double> getMaxVelocityDucking() {
        return this.maxVelocityDucking;
    }

    /**
     * Check whether this Mazub can have the given maximum velocity while standing as its maximum velocity while standing.
     *
     * @param maxVelocityDucking The maximum velocity while standing to check.
     * @return | result == maxVelocityDucking.getX() >= getMinVelocityDucking().getX()
     */
    @Raw
    public boolean canHaveAsMaxVelocityDucking(Vector<Double> maxVelocityDucking) {
        return maxVelocityDucking.getX() >= getMinVelocityDucking().getX();
    }

    /**
     * Variable registering the maximum velocity while standing of this Mazub.
     */
    private final Vector<Double> maxVelocityDucking;

    /**
     * Return the minimum velocity while standing of this Mazub.
     */
    @Basic
    @Raw
    @Immutable
    public Vector<Double> getMinVelocityDucking() {
        return this.minVelocityDucking;
    }

    /**
     * Check whether this Mazub can have the given minimum velocity while standing as its minimum velocity while standing.
     *
     * @param minVelocityDucking The minimum velocity while standing to check.
     * @return | result == true
     */
    @Raw
    public boolean canHaveAsMinVelocityDucking(Vector<Double> minVelocityDucking) {
        return true;
    }

    /**
     * Variable registering the minimum velocity while standing of this Mazub.
     */
    private final Vector<Double> minVelocityDucking;

    public Vector<Double> getMaxVelocity() {
        return isDucking() ? getMaxVelocityDucking() : getMaxVelocityStanding();
    }
    public Vector<Double> getMinVelocity() {
        return isDucking() ? getMinVelocityDucking() : getMinVelocityStanding();
    }


    /**
     * Return the moving of this .
     */
//    @Basic
//    @Raw
//    public boolean isMoving() {
//        return this.getVelocity().getX() != 0;
//    }

    /**
     * Return the Mazub of this ducking.
     */
    @Basic
    @Raw
    public boolean isDucking() {
        return this.ducking;
    }

    /**
     * Set the Mazub of this ducking to the given Mazub.
     *
     * @param ducking The new Mazub for this ducking.
     * @post The Mazub of this new ducking is equal to
     * the given Mazub.
     * | new.isducking() == ducking
     * @post Set the horizontal velocity of the Mazub to 1 if the horizontal velocity of the Mazub is greater than 1.
     * | if(getVelocity().getX() > getMaxVelocityDucking().getX()) setVelocity(new Vector(1, getVelocity().getY()))
     */
    @Raw
    public void setDucking(boolean ducking) {
        if (getVelocity().getX() > getMaxVelocityDucking().getX()) setVelocity(new Vector<>(1.0, getVelocity().getY()));
        this.ducking = ducking;
    }

    public boolean isMoving() {
        return getVelocity().getX() != 0;
    }

    /**
     * Variable registering the Mazub of this ducking.
     */
    private boolean ducking;

    /**
     * Return the Mazub of this jumping.
     */
    @Basic
    @Raw
    public boolean isJumping() {
        return this.jumping;
    }

    /**
     * Set the Mazub of this jumping to the given Mazub.
     *
     * @param jumping The new Mazub for this jumping.
     * @post The Mazub of this new jumping is equal to
     * the given Mazub.
     * | new.isJumping() == jumping
     */
    @Raw
    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    /**
     * Variable registering the Mazub of this jumping.
     */
    private boolean jumping;

    @Override
    public void onBounceX(Vector<Double> delta) {
        getGameObject().setOrientation(OrientationState.Front);
        setVelocity(getVelocity().setX(0.0));
        setAcceleration(getAcceleration().setX(0.0));
    }

    @Override
    public void onBounceY(Vector<Double> delta) {
        setVelocity(getVelocity().setY(0.0));
        setAcceleration(getAcceleration().setY(0.0));
    }
}
