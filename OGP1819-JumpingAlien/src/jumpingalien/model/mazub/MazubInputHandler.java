package jumpingalien.model.mazub;

import jumpingalien.model.Mazub;
import jumpingalien.model.handler.AnimationHandler;
import jumpingalien.model.handler.InputHandler;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.model.handler.MovementHandler;
import jumpingalien.util.ModelException;
import jumpingalien.util.Vector;

/**
 * A class for handling player input for Mazub
 *
 * @invar The GameObject of this Handler will always be a Mazub
 *     |  getGameObject() instanceof Mazub
 */
public class MazubInputHandler extends InputHandler {

    /**
     * A new MazubInputHandler will be created
     *
     * @param mazub The Mazub that has this input handler
     *
     * @effect | super(mazub)
     */
    public MazubInputHandler(Mazub mazub) {
        super(mazub);
    }

    /**
     * Start moving to the left
     *
     * @pre This Mazub is currently not moving and is not dead.
     * 		| !this.getHandler(MazubMovementHandler.class).isMoving() && !getGameObject().isDead()
     *
     * @effect The horizontal acceleration will be set to -0.9
     *      | this.getHandler(MazubMovementHandler.class).setAcceleration(new Vector(-0.9, getAcceleration().getY()))
     * @effect The horizontal velocity will be set to -1
     *      | this.getHandler(MazubMovementHandler.class).setVelocity(new Vector(-1, getVelocity().getY()));
     * @effect The orientation state will be set to left
     *      | this.getHandler(MazubAnimationHandler.class).setOrientationState(MazubAnimationHandler.OrientationState.Left);
     */
    public void startMoveLeft() {
        assert !getGameObject().isDead() && getGameObject().getOrientation() == OrientationState.Front;
        MovementHandler movementHandler = getGameObject().getHandler(MovementHandler.class);
        getGameObject().setOrientation(OrientationState.Left);
        movementHandler.setAcceleration(movementHandler.getAcceleration().setX(-0.9));
        movementHandler.setVelocity(movementHandler.getVelocity().setX(-1.0));
    }

    /**
     * Start moving to the right
     *
     * @pre This Mazub is currently not moving and is not dead.
     * 		| !this.getHandler(MazubMovementHandler.class).isMoving() && !getGameObject().isDead()
     *
     * @effect The horizontal acceleration will be set to 0.9
     *      | this.getGameObject().getHandler(MazubMovementHandler.class).setAcceleration(new Vector(0.9, getAcceleration().getY()))
     * @effect The horizontal velocity will be set to 1
     *      | this.getGameObject().getHandler(MazubMovementHandler.class).setVelocity(new Vector(1, getVelocity().getY()));
     * @effect The orientation state will be set to right
     *      | this.getGameObject().getHandler(MazubAnimationHandler.class).setOrientationState(MazubAnimationHandler.OrientationState.Right);
     */
    public void startMoveRight() {
        assert !getGameObject().isDead() && getGameObject().getOrientation() == OrientationState.Front;
        MovementHandler movementHandler = getGameObject().getHandler(MovementHandler.class);
        getGameObject().setOrientation(OrientationState.Right);
        movementHandler.setAcceleration(movementHandler.getAcceleration().setX(0.9));
        movementHandler.setVelocity(movementHandler.getVelocity().setX(1.0));
    }

    /**
     * Stop moving
     *
     * @pre This Mazub is currently moving.
     * 		| this.getGameObject().getHandler(MazubMovementHandler.class).isMoving()
     * @effect The horizontal acceleration will be set to 0
     *      | this.getGameObject().getHandler(MazubMovementHandler.class).setAcceleration(new Vector(0, getGameObject().getHandler(MazubMovementHandler.class).getAcceleration().getY()))
     * @effect The horizontal velocity will be set to 0
     *      | this.getGameObject().getHandler(MazubMovementHandler.class).setVelocity(new Vector(0, getVelocity().getY()));
     * @effect The orientation state will be set to front
     *      | this.getGameObject().getHandler(MazubAnimationHandler.class).setOrientationState(MazubAnimationHandler.OrientationState.Front);
     */
    public void endMove() {
        assert getGameObject().getOrientation() != OrientationState.Front;
        MovementHandler movementHandler = getGameObject().getHandler(MovementHandler.class);
        getGameObject().setOrientation(OrientationState.Front);
        movementHandler.setAcceleration(movementHandler.getAcceleration().setX(0.0));
        movementHandler.setVelocity(movementHandler.getVelocity().setX(0.0));
    }

    /**
     * Event trigger for when the Mazub is starting to jump.
     *
     * @effect Set the jumping variable of the Mazub to true.
     *          | getGameObject().getHandler(MazubMovementHandler.class).setJumping(true)
     * @effect Set the y-component of the acceleration vector of the Mazub to -10.
     *          | getGameObject().getHandler(MazubMovementHandler.class).setAcceleration(new Vector(getGameObject().getHandler(MazubMovementHandler.class).getAcceleration().getX(), -10));
     * @effect Set the y-component of the velocity vector of the Mazub to 8.
     *          | getGameObject().getHandler(MazubMovementHandler.class).setVelocity(new Vector(getGameObject().getHandler(MazubMovementHandler.class).getVelocity().getX(), 8));
     *
     * @throws ModelException
     *          Throw a model exception if the Mazub is jumping or if it is dead.
     *          | getGameObject().getHandler(MazubMovementHandler.class).isJumping() || getGameObject().isDead()  || getGameObject().isTerminated()
     */
    public void startJump() throws ModelException {
        MazubMovementHandler movementHandler = getGameObject().getHandler(MazubMovementHandler.class);
        if(getGameObject().isDead() || getGameObject().isTerminated()) throw new ModelException("Dead objects can't switch orientation!");
        if(movementHandler.isJumping()) throw new ModelException("Already jumping!");
        movementHandler.setJumping(true);
        movementHandler.setAcceleration(new Vector<>(movementHandler.getAcceleration().getX(), -10.0));
        movementHandler.setVelocity(new Vector<>(movementHandler.getVelocity().getX(), 8.0));
    }

    /**
     * Event trigger for when the Mazub stops jumping.
     *
     * @effect If the y-component of the Mazub's velocity is smaller than 0, set the y-component of the Mazub's velocity vector to 0.
     *          | if (getGameObject().getHandler(MazubMovementHandler.class).getVelocity().getY() > 0)
     *          |   getGameObject().getHandler(MazubMovementHandler.class).setVelocity(new Vector(getGameObject().getHandler(MazubMovementHandler.class).getVelocity().getX(), 0))
     * @effect Set the jumping variable of the Mazub to false.
     *          | getGameObject().getHandler(MazubMovementHandler.class).setJumping(false)
     * @throws ModelException
     *          Throw a model exception if the Mazub is not jumping.
     *          | !getGameObject().getHandler(MazubMovementHandler.class).isJumping()
     */
    public void endJump() throws ModelException {
        MazubMovementHandler movementHandler = getGameObject().getHandler(MazubMovementHandler.class);
        if(!movementHandler.isJumping()) throw new ModelException("Not jumping!");
        if (movementHandler.getVelocity().getY() > 0)
            movementHandler.setVelocity(new Vector<>(movementHandler.getVelocity().getX(), 0.0));
        movementHandler.setJumping(false);
    }

    /**
     * Event trigger for when the Mazub starts ducking.
     *
     * @effect Set the ducking variable of the Mazub to true.
     *          | getGameObject().getHandler(MazubMovementHandler.class).setDucking(true)
     * @effect if the x-component of the Mazub's velocity vector is greater than 1, set the x-component of the Mazub's velocity vector to the sign function of the current x-component of the Mazub's velocity.
     *          | if (Math.abs(getGameObject().getHandler(MazubMovementHandler.class).getVelocity().getX()) > 1)
     *          |   getGameObject().getHandler(MazubMovementHandler.class).setVelocity(
     *          |       new Vector(Math.signum(getGameObject().getHandler(MazubMovementHandler.class).getVelocity().getX()), getGameObject().getHandler(MazubMovementHandler.class).getVelocity().getY())
     *          |   );
     * @throws ModelException
     *      | getGameObject().isDead()
     */
    public void startDuck(){
        if(getGameObject().isDead()) throw new ModelException("Dead objects can't switch orientation!");
        MazubMovementHandler movementHandler = getGameObject().getHandler(MazubMovementHandler.class);
        movementHandler.setDucking(true);
        if (Math.abs(movementHandler.getVelocity().getX()) > 1) {
            movementHandler.setVelocity(new Vector<>(
                    Math.signum(movementHandler.getVelocity().getX()),
                    movementHandler.getVelocity().getY()
            ));
        }
        getGameObject().getHandler(MazubAnimationHandler.class).setNextSprite();
    }

    /**
     * Event trigger for when the Mazub stops ducking.
     *
     * @effect Set the ducking variable of the Mazub to false, if this is a valid state -> isDucking() == true else isDucking() == false.
     *          | movementHandler.setDucking(false);
     *           if(getGameObject().getHandler(AnimationHandler.class).getCurrentSprite() == getGameObject().getHandler(AnimationHandler.class).getSprites()[1] ||
     *                 getGameObject().getHandler(AnimationHandler.class).getCurrentSprite() == getGameObject().getHandler(AnimationHandler.class).getSprites()[6] ||
     *                 getGameObject().getHandler(AnimationHandler.class).getCurrentSprite() == getGameObject().getHandler(AnimationHandler.class).getSprites()[7])
     *             movementHandler.setDucking(true);
     */
    public void endDuck() {
        MazubMovementHandler movementHandler = getGameObject().getHandler(MazubMovementHandler.class);
        MazubAnimationHandler animationHandler = getGameObject().getHandler(MazubAnimationHandler.class);
        movementHandler.setDucking(false);

        animationHandler.setNextSprite();
        if(animationHandler.getCurrentSprite() == animationHandler.getSprites()[1] ||
                animationHandler.getCurrentSprite() == animationHandler.getSprites()[6] ||
                animationHandler.getCurrentSprite() == animationHandler.getSprites()[7])
            movementHandler.setDucking(true);
    }
}
