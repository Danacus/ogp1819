package jumpingalien.model.mazub;


import jumpingalien.model.*;
import jumpingalien.model.gameobject.ICollidable;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.model.handler.MovementHandler;
import jumpingalien.model.handler.WorldCollisionHandler;
import jumpingalien.model.slime.SlimeCollisionHandler;
import jumpingalien.model.world.Tile;
import jumpingalien.util.Vector;

/**
 * A class for handling the collision of Mazub
 *
 * @invar The GameObject of this Handler will always be a Mazub
 *     |  getGameObject() instanceof Mazub
 */
public class MazubCollisionHandler extends WorldCollisionHandler {

    /**
     * @param mazub
     *      The mazub for this MazubCollisionHandler.
     * @post
     *      | super(mazub)
     */
    public MazubCollisionHandler(Mazub mazub) {
        super(mazub);
    }


    /**
     * Check for collision
     *
     * @effect   The WorldCollisionHandler will check for collision
     *         | super.checkCollision()
     * @effect   The game will and if the world exists and the Mazub has reached the target tile
     *         | if (getGameObject().getWorld() != null)
     *         |    && getGameObject().getBounds().intersects(getGameObject().getWorld().getTargetTile().getBounds())) then
     *         |    getGameObject().getWorld().endGame()
     */
    @Override
    public void checkCollision(Vector<Integer> pixelPos) {
        super.checkCollision(pixelPos);

        if (getGameObject().getWorld() == null)
            return;

        if (getGameObject().getBounds().intersects(getGameObject().getWorld().getTargetTile().getBounds())) {
            getGameObject().getWorld().endGame();
        }

    }

    /**
     * Mazub enters collision with another collidable
     *
     * @param other the other collidable object.
     *
     * @effect The WorldCollisionHandler will handle the collision
     *         | super.onCollisionEnter(other)
     * @post   If Mazub touches a solid collidable, he will look in front of him
     *         | if (other.isSolid()) then
     *         |    new.getGameObject().getOrientation == OrientationState.Front
     * @effect If the other object is a plant, Mazub will try to eat it
     *         | if (other instanceof Plant) then
     *         |    ((Mazub)getGameObject()).eatPlant((Plant) other)
     * @effect If the other object is a living slime, and Mqzub is not stationary, he will take damage
     *         | if (!((Slime) other).isDead()
     *              && !getGameObject().getHandler(MovementHandler.class).getVelocity().equals(Vector.ACTUAL_ZERO)) then
     *         |    getGameObject().takeDamage(20);
     * @effect If the other object is a living shark, Mazub will take damage
     *         | if (!((Shark) other).isDead()) then
     *         |    getGameObject().takeDamage(50);
     */
    @Override
    public void onCollisionEnter(ICollidable other) {
        super.onCollisionEnter(other);
        if (other.isSolid()) {
            getGameObject().setOrientation(OrientationState.Front);
        }

        if (other instanceof Plant) {
            allowInteraction();
        }

        if (other instanceof Slime && !((Slime) other).getHandler(SlimeCollisionHandler.class).getSlimeCollisionTimer().isRunning()){
            if (!((Slime) other).isDead() && !getGameObject().getHandler(MovementHandler.class).getVelocity().equals(Vector.ACTUAL_ZERO))
                getGameObject().takeDamage(20);
        }

        if (other instanceof Shark) {
            if (!((Shark) other).isDead())
                getGameObject().takeDamage(50);
        }

    }

    @Override
    public void onCollision(ICollidable other) {
        super.onCollision(other);
        if (!canInteract())
            return;

        if (other instanceof Plant) {
            eatPlant((Plant) other);
        }
    }

    /**
     * Eat a plant
     *
     * @param plant the plant to eat.
     *
     * @effect
     *      \ if (plant != null && getHitPoints() < 500 && !plant.isTerminated()) then
     *      |   if (!plant.isDead()) then
     *      |       setHitPoints(getHitPoints() + 50)
     *      |       plant.takeDamage(1)
     *      |   else
     *      |       setHitPoints(getHitPoints() - 20)
     */
    public void eatPlant(Plant plant) {
        if (plant != null && getGameObject().getHitPoints() < getGameObject().getMaximumHitPoints() && !plant.isTerminated()) {
            startInteraction();
            if (!plant.isDead()) {
                getGameObject().takeDamage(-50);
            } else {
                getGameObject().takeDamage(20);
            }
        }
    }
}
