package jumpingalien.model.mazub;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.model.Mazub;
import jumpingalien.util.exception.InvalidSpriteException;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.handler.AnimationHandler;
import jumpingalien.model.handler.CollisionHandler;
import jumpingalien.model.handler.MovementHandler;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.util.GameTimer;
import jumpingalien.util.*;

import java.util.Arrays;

/**
 * A class for handling the animation and sprites of a Mazub
 *
 * @invar The GameObject of this Handler will always be a Mazub
 *     |  getGsmeObject() instanceof Mazub
 */
public class MazubAnimationHandler extends AnimationHandler {
    
    /**
     * Initialize this new MazubAnimationHandler with given Mazub.
     *
     * @param  mazub
     *         The Mazub for this new MazubAnimationHandler.
     * @param sprites
     *         The array of sprites for this new MazubAnimationHandler.
     *
     * @post  The current sprite is the first sprite
     *      | new.getCurrentSprite() == sprites[0]
     * @post  The sprite timer is a new GameTimer
     *      | new.getSpriteTimer() == new GameTimer(0.075) {
     *      |    @Override
     *      |    public void onTick() {
     *      |       nextSprite();
     *      |    }
     *      | };
     * @effect  The sprite timer will be started
     *      | getSpriteTimer().start()
     */
    public MazubAnimationHandler(Mazub mazub, Sprite[] sprites) throws InvalidSpriteException {
        super(mazub, sprites);
        currentSprite = sprites[0];

        spriteTimer = new GameTimer(0.075) {
            @Override
            public void onTick() {
                nextSprite();
            }
        };

        getSpriteTimer().start();
    }

    /**
     * Returns the sprite timer
     */
    @Model
    @Basic
    private GameTimer getSpriteTimer() {
        return spriteTimer;
    }

    /**
     * A variable storing the sprite timer
     */
    private final GameTimer spriteTimer;

    /**
     * Check whether this Mazub can have the given sprites as its sprites.
     *
     * @param  sprites
     *         The sprites to check.
     *
     * @return Check whether the sprite array is null and is larger than 10 and has an even size, all elements mustn't be null.
     *       | result == sprites != null && sprites.length >= 10 && sprites.length % 2 == 0 && Arrays.stream(sprites).anyMatch(s -> s != null)
     */
    @Raw
    @Override
    public boolean canHaveAsSprites(Sprite[] sprites) {
        return super.canHaveAsSprites(sprites) && sprites.length >= 10 && sprites.length % 2 == 0;
    }

    /**
     * Variable registering the index of the sprite array of this MazubAnimationHandler.
     */
    private int ind = 8;

    /**
     * Set the index of the sprite array.
     */
    @Raw
    @Basic
    public void setSpriteInd(int i){
        this.ind = i;
    }

    /**
     * Check whether the sprite is a valid sprite.
     * @param sprite the sprite to check.
     * @return No doc needed
     */
    private boolean validCollidingPosition(Sprite sprite){
        if(!getGameObject().hasHandler(CollisionHandler.class) || getGameObject().getWorld() == null) return true;
        return Arrays.stream(getGameObject().getHandler(CollisionHandler.class).getIntersectingTiles(sprite)).noneMatch(tiles ->
                Arrays.stream(tiles).anyMatch(tile ->
                        (getGameObject().getMask() & tile.getCategory()) != 0 && tile.isSolid()
                )
        ) && getGameObject().getHandler(CollisionHandler.class).getIntersectingGameObjects(
                getGameObject().getHandler(MovementHandler.class).getPixelPosition(),
                sprite,
                gameObject ->
                        gameObject.hasHandler(CollisionHandler.class)
                                ? gameObject.getBounds(gameObject.getHandler(AnimationHandler.class).getCurrentSpriteNoCalc())
                                : new Bounds(Vector.PIXEL_ZERO, Vector.PIXEL_ZERO)
        ).stream().noneMatch(GameObject::isSolid);
    }

    /**
     * Variable registering the current sprite of this MazubAnimationHandler.
     */
    private Sprite currentSprite;

    @Override
    public void update(double dt) {

    }

    /**
     * Returns the current sprite.
     * @return no doc needed
     */
    public Sprite getCurrentSprite(){
        setNextSprite();
        return currentSprite;
    }

    /**
     * Returns the current sprite, but does not calculate the next sprite.
     * @return no doc needed
     */
    public Sprite getCurrentSpriteNoCalc(){
        return currentSprite;
    }


    /***
     * Returns the next sprite.
     * @return no doc needed
     */
    @Basic
    public Sprite calculateNextSprite(){

        MazubMovementHandler movementHandler = getGameObject().getHandler(MazubMovementHandler.class);
        OrientationState gameObjectOrientation = getGameObject().getOrientation();

        if (!movementHandler.isDucking()) {
            if (!movementHandler.isJumping()) {
                if (gameObjectOrientation == OrientationState.Front) {
                    if (getPrevOrientationState() == OrientationState.Right)
                        return getSprites()[2];
                    if (getPrevOrientationState() == OrientationState.Left)
                        return getSprites()[3];
                }
            } else {
                if (gameObjectOrientation == OrientationState.Right)
                    return getSprites()[4];
                else if (gameObjectOrientation == OrientationState.Left)
                    return getSprites()[5];
            }
        } else {
             if (getPrevOrientationState() == OrientationState.Right || gameObjectOrientation == OrientationState.Right)
                 return getSprites()[6];
             else if (getPrevOrientationState() == OrientationState.Left || gameObjectOrientation == OrientationState.Left)
                 return getSprites()[7];
             else
                 return getSprites()[1];
        }

        int n = getSprites().length-1;

        if (gameObjectOrientation == OrientationState.Right) {
            if (getPrevOrientationState() == gameObjectOrientation || ind > ((n-8)/2)+8) ind = 8;
            return getSprites()[ind];
        } else if (gameObjectOrientation == OrientationState.Left) {
            if (getPrevOrientationState() == gameObjectOrientation || ind > n || ind <= ((n-8)/2)+8) ind = ((n-8)/2)+9;
            return getSprites()[ind];
        }

        return getSprites()[0];
    }

    /**
     * Increment the sprite index.
     */
    @Raw
    public void nextSprite(){
        ind++;
    }

    /**
     * No doc needed
     */
    private double frontCount = 0;

    /**
     * No doc needed
     */
    public void resetAnimation(){
        spriteTimer.reset();
        frontCount = 0;
        if(getGameObject().getOrientation() == OrientationState.Left){
            setSpriteInd((((getSprites().length-1)-8)/2)+9);
        }
        if(getGameObject().getOrientation() == OrientationState.Right){
           setSpriteInd(8);
        }
    }

    /**
     * No doc needed
     */
    public void pixelUpdate(double dt){
        frontCount += dt;
        spriteTimer.update(dt);

        if (frontCount >= 1 && getGameObject().getOrientation() == OrientationState.Front){
            frontCount = 0;
            setPrevOrientationState(OrientationState.Front);
        }
        if(getGameObject().getOrientation() != OrientationState.Front) frontCount = 0;
    }

    public void setNextSprite() {
        Sprite nextSprite = calculateNextSprite();
        if(validCollidingPosition(nextSprite)) {
            currentSprite = nextSprite;
        }
    }

    /**
     * Return the prev orientation state of this animation handler.
     */
    @Basic @Raw
    public OrientationState getPrevOrientationState() {
        return this.prevOrientationState;
    }

    /**
     * Check whether the given prev orientation state is a valid prev orientation state for
     * any animation handler.
     *
     * @param  prevOrientationState
     *         The prev orientation state to check.
     * @return
     *       | result == prevOrientationState == {@link OrientationState}.Front || prevOrientationState == {@link OrientationState}.Left || prevOrientationState == {@link OrientationState}.Right
     */
    public static boolean isValidPrevOrientationState(OrientationState prevOrientationState) {
        return prevOrientationState.equals(OrientationState.Front) || prevOrientationState.equals(OrientationState.Left) || prevOrientationState.equals(OrientationState.Right);
    }

    /**
     * Set the prev orientation state of this animation handler to the given prev orientation state.
     *
     * @param  prevOrientationState
     *         The new prev orientation state for this animation handler.
     * @post   The prev orientation state of this new animation handler is equal to
     *         the given prev orientation state.
     *       | new.getPrevOrientationState() == prevOrientationState
     * @throws ModelException
     *         The given prev orientation state is not a valid prev orientation state for any
     *         animation handler.
     *       | ! isValidPrevOrientationState(getPrevOrientationState())
     */
    @Raw
    public void setPrevOrientationState(OrientationState prevOrientationState)
          throws ModelException {
        if (! isValidPrevOrientationState(prevOrientationState))
            throw new ModelException("Illegal orientation state");
        this.prevOrientationState = prevOrientationState;
    }

    /**
     * Variable registering the prev orientation state of this animation handler.
     */
    private OrientationState prevOrientationState;
}
