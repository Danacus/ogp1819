package jumpingalien.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.util.exception.GameObjectException;
import jumpingalien.util.exception.IllegalPositionException;
import jumpingalien.util.exception.InvalidSpriteException;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.model.handler.WorldCollisionHandler;
import jumpingalien.model.mazub.MazubAnimationHandler;
import jumpingalien.model.mazub.MazubCollisionHandler;
import jumpingalien.model.mazub.MazubInputHandler;
import jumpingalien.model.mazub.MazubMovementHandler;
import jumpingalien.model.world.Collision;
import jumpingalien.model.world.DamageEffect;
import jumpingalien.model.world.GeologicalFeature;
import jumpingalien.util.*;

/**
 * A class for Mazubs
 *
 * @invar  This mazub has a MazubInputHandler
 *       | hasHandler(MazubInputHandler.class)
 * @invar  This mazub has a MazubMovementHandler
 *       | hasHandler(MazubMovementHandler.class)
 * @invar  This mazub has a MazubAnimationHandler
 *       | hasHandler(MazubAnimationHandler.class)
 * @invar  This mazub has a MazubCollisionHandler
 *       | hasHandler(MazubCollisionHandler.class)
 */
public class Mazub extends GameObject {

    /**
     * Initialize this new Mazub with given position and sprites.
     *
     * @param  pixelLeftX
     * 		   The left pixel x-coordinate for the new Mazub.
     *
     * @param pixelBottomY
     * 		  The bottom pixel y-coordinate for the new Mazub.
     *
     * @param sprites
     * 		  The list of sprites for the new Mazub.
     *
     * @effect A new movement handler will be added to this Mazub
     *       | addHandler(new MazubMovementHandler(
     *       |      new Vector(pixelLeftX, pixelBottomY).pixelToActual(),
     *       |      Vector.ZERO,
     *       |      Vector.ZERO,
     *       |      new Vector(0, 0),
     *       |      new Vector(3, 8),
     *       |      new Vector(0, 0),
     *       |      new Vector(1, 8)
     *       | ));
     * @effect A new animation handler will be added to this Mazub
     *       | addHandler(new MazubAnimationHandler(this, sprites))
     * @effect A new input handler will be added to this Mazub
     *       | addHandler(new MazubInputHandler(this))
     * @effect A new collision handler will be added to this Mazub
     *       | addHandler(new MazubCollisionHandler(this))
     * @effect New effects will be added to the world collision handler of this Mazub
     *       | getHandler(WorldCollisionHandler.class).setEffect(GeologicalFeature.MAGMA, new GeologicalEffect(this, 0.2, 50, true));
     *       | getHandler(WorldCollisionHandler.class).setEffect(GeologicalFeature.GAS, new GeologicalEffect(this, 0.2, 4, true));
     *       | getHandler(WorldCollisionHandler.class).setEffect(GeologicalFeature.WATER, new GeologicalEffect(this, 0.2, 2, false));
     * @post The hitpoints of this Mazub will be equal to 100
     *       | new.getHitPoints() == 100
     * @post The maximum hitpoints of this Mazub will be equal to 500
     *       | new.getMaximumHitPoints() == 500
     *
     * @throws IllegalPositionException
     *         The given position is outside the universe
     *       | pixelLeftX < 0 || pixelBottomY < 0
     */
    public Mazub(int pixelLeftX, int pixelBottomY, Sprite... sprites) throws GameObjectException {
        super();

        if (pixelLeftX < 0 || pixelBottomY < 0)
            throw new IllegalPositionException("Position outside universe");

        addHandler(new MazubInputHandler(this));
        addHandler(new MazubMovementHandler(
                this,
                new Vector<>(pixelLeftX, pixelBottomY).pixelToActual(),
                Vector.ACTUAL_ZERO,
                Vector.ACTUAL_ZERO,
                new Vector<>(0.0, 0.0),
                new Vector<>(3.0, 8.0),
                new Vector<>(0.0, 0.0),
                new Vector<>(1.0, 8.0)
        ));
        addHandler(new MazubAnimationHandler(this, sprites));
        addHandler(new MazubCollisionHandler(this));

        WorldCollisionHandler worldCollisionHandler = getHandler(WorldCollisionHandler.class);
        worldCollisionHandler.setEffect(GeologicalFeature.MAGMA, new DamageEffect(this, 0.2, true, 50));
        worldCollisionHandler.setEffect(GeologicalFeature.GAS, new DamageEffect(this, 0.2, true, 4));
        worldCollisionHandler.setEffect(GeologicalFeature.WATER, new DamageEffect(this, 0.2, false, 2));

        setMaximumHitPoints(500);
        setHitPoints(100);
	}

    /**
     * Mazub will be terminated
     *
     * @effect  The GameObject will be terminated
     *      | super.terminate()
     * @effect  If Mazub exists in a World, the game will end and the player loses
     *      | if (getWorld() != null)
     *      |   getWorld().endGame()
     */
    @Override
    public void terminate() {
        if (getWorld() != null)
            getWorld().endGame();
        super.terminate();
    }

    /**
     * Checks whether this Mazub can have the given orientation as its orientation
     *
     * @param orientation  The orientation to check
     *
     * @return True if and only if the orientation is a horizontal orientation
     *      | orientation.isHorizontal()
     */
    @Override
    public boolean canHaveAsOrientation(OrientationState orientation) {
        return orientation.isHorizontal();
    }

    /**
     * Sets the orientation of this Mazub
     *
     * @param orientation The new orientation
     *
     * @effect  The previous orientation state of the animation handler will be set to the current orientation state
     *        | getHandler(MazubAnimationHandler.class).setPrevOrientationState(getOrientation())
     * @effect  The orientation of the GameObject will be set to the given orientation
     *        | super.setOrientation(orientation)
     */
    @Override
    public void setOrientation(OrientationState orientation) {
        getHandler(MazubAnimationHandler.class).setPrevOrientationState(getOrientation());
        super.setOrientation(orientation);
    }


    /**
     * Returns the category for collision
     *
     * @return | result == Collision.Category.MAZUB
     */
    @Basic
    @Immutable
    @Override
    public byte getCategory() {
        return Collision.Category.MAZUB;
    }

    /**
     * Returns the mask for collision
     *
     * @return | result == Collision.Mask.MAZUB
     */
    @Basic
    @Immutable
    @Override
    public byte getMask() {
        return Collision.Mask.MAZUB;
    }

    /**
     * Returns whether this Mazub is solid
     *
     * @return Every Mazub is solid
     *       | result == true
     */
    @Override
    public boolean isSolid() {
        return true;
    }
}
