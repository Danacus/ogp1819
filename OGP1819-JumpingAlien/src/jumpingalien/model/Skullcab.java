package jumpingalien.model;

import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.model.plant.VerticalPlantMovementHandler;
import jumpingalien.util.Direction;
import jumpingalien.util.GameTimer;
import jumpingalien.util.Sprite;
import jumpingalien.util.Vector;
import jumpingalien.util.exception.GameObjectException;

/**
 * A class for Skullcabs that move up and down
 *
 * @invar | hasHandler(VerticalPlantMovementHandler.class)
 */
public class Skullcab extends Plant {
    /**
     * Creates a new Skullcab
     *
     * @param pixelLeftX x pixel coordinate for the new plant.
     * @param pixelBottomY y pixel coordinate fot he new plant.
     * @param sprites sprite array for the new plant.
     *
     * @effect | super(pixelLeftX, pixelBottomY, sprites)
     * @effect | addHandler(new VerticalPlantMovementHandler(this, new Vector<>(pixelLeftX, pixelBottomY).pixelToActual(), 0.5));
     * @post | new.getHitPoint() == 3
     */
    public Skullcab(int pixelLeftX, int pixelBottomY, Sprite[] sprites) throws GameObjectException {
        super(sprites, 12);
        addHandler(new VerticalPlantMovementHandler(this, new Vector<>(pixelLeftX, pixelBottomY).pixelToActual(), 0.5));
        setHitPoints(3);
    }

    /**
     * Variable registering the eat timer of this Skullcab.
     */
    private GameTimer eatTimer = new GameTimer(0.6 , false) {
        @Override
        public void onTick() {
            getEatTimer().stop();
            hasTakenDamage = false;
        }
    };

    /**
     * Returns the eat timer of the Skullcab
     */
    public GameTimer getEatTimer(){
        return eatTimer;
    }

    /**
     * Returns whether the Skullcab has taken damage
     */
    public boolean hasTakenDamage() {
        return hasTakenDamage;
    }

    /**
     * Sets whether the Skullcab has taken damage
     */
    public void setHasTakenDamage(boolean hasTakenDamage) {
        this.hasTakenDamage = hasTakenDamage;
    }

    private boolean hasTakenDamage;

    /**
     * No doc required.
     * @param dt the advanced time since last tick.
     */
    @Override
    public void update(double dt) {
        super.update(dt);
        getEatTimer().update(dt);
    }

    /**
     * Checks whether this Skullcab can have the given orientation as its orientation
     *
     * @param orientation The orientation to check
     *
     * @return | orientation.isVertical()
     */
    @Override
    public boolean canHaveAsOrientation(OrientationState orientation) {
        return orientation.isVertical();
    }
}
