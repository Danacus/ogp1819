package jumpingalien.model.shark;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.Shark;
import jumpingalien.model.handler.CollisionHandler;
import jumpingalien.model.world.GeologicalFeature;
import jumpingalien.model.world.Tile;
import jumpingalien.util.exception.IllegalPositionException;
import jumpingalien.model.gameobject.GameObject;

import jumpingalien.model.handler.DynamicMovementHandler;
import jumpingalien.util.GameTimer;
import jumpingalien.util.Vector;

import java.util.Arrays;

/**
 * A class for handling the movement of a Shark
 *
 * @invar | getGameObject() instanceof Shark
 */
public class SharkMovementHandler extends DynamicMovementHandler {
    /**
     * Creates a new Shark Movement Handler
     *
     * @param shark     The Shark
     * @param initPos   The initial position of the shark
     *
     * @effect | setPosition(initPos)
     *         | setVelocity(Vector.ACTUAL_ZERO)
     *         | setAcceleration(new Vector<>(-1.5, 0.0))
     * @post   | new.isResting() == false
     * @effect \ getSwitchTimer().start()
     */
    public SharkMovementHandler(Shark shark, Vector<Double> initPos) throws IllegalPositionException {
        super(shark);
        setPosition(initPos);
        setVelocity(Vector.ACTUAL_ZERO);
        setAcceleration(new Vector<>(-1.5, 0.0));
        setResting(false);

        restTimer = new GameTimer(1) {
            @Override
            public void onTick() {
                setResting(false);
                if (canJump()) {
                    setVelocity(getVelocity().setY(2.0));
                }
                getGameObject().setOrientation(getGameObject().getOrientation().inverted());
                restTimer.stop();
                switchTimer.reset();
                switchTimer.start();
            }
        };

        switchTimer = new GameTimer(0.5) {
            @Override
            public void onTick() {
                setResting(true);
                switchTimer.stop();
                restTimer.reset();
                restTimer.start();
            }
        };

        switchTimer.start();
    }

    /**
     * Updates the Shark movement
     *
     * @param dt The time that has passed since the last update.
     */
    @Override
    public void pixelUpdate(double dt) {
        restTimer.update(dt);
        switchTimer.update(dt);

        if (!isResting()) {
            setAcceleration(new Vector<>(1.5 * getGameObject().getOrientation().getIndex(), getAcceleration().getY()));
        } else {
            setVelocity(new Vector<>(0.0, getVelocity().getY()));
            setAcceleration(new Vector<>(0.0, getAcceleration().getY()));
        }

        super.pixelUpdate(dt);
    }


    /**
     * Tries to move the Shark to the given position
     * If unsuccessful, onBounceX and/or onBounceY will be invoked
     * Will also check if the top bounds of the shark intersect with water
     * an enable/disable gravity accordingly and it will
     * check if the shark overlaps with impassable terrain
     *
     * @param newPos The position you want to move to
     */
    @Override
    public void moveTo(Vector<Double> newPos) {
        Vector<Double> oldPos = getPosition();
        super.moveTo(newPos);

        if (!getGameObject().hasHandler(CollisionHandler.class))
            return;

        CollisionHandler collisionHandler = getGameObject().getHandler(CollisionHandler.class);

        Tile[][] tiles = collisionHandler.getIntersectingTiles(((Shark)getGameObject()).getTopBounds());
        if (Arrays.stream(tiles).anyMatch(line -> Arrays.stream(line).anyMatch(tile -> tile.getGeologicalFeature() == GeologicalFeature.WATER))) {
            disableGravity();
            if (getVelocity().getY() < 0)
                setVelocity(getVelocity().setY(0.0));
        } else {
            enableGravity();
        }

        Tile[][] outerTiles = collisionHandler.getIntersectingTiles(getGameObject().getOuterBounds());
        onImpassableTerrain = Arrays.stream(outerTiles).anyMatch(line -> Arrays.stream(line).anyMatch(Tile::isSolid));
    }

    private GameTimer switchTimer;
    private GameTimer restTimer;

    /**
     * Returns whether the shark is resting
     */
    @Basic
    public boolean isResting() {
        return resting;
    }

    /**
     * Sets whether the shark is resting or not
     *
     * @param resting   The new value
     *
     * @post | isResting() == resting
     */
    public void setResting(boolean resting) {
        this.resting = resting;
    }

    /**
     * Variable registering if the shark is resting
     */
    private boolean resting;

    /**
     * Checks whether the shark can jump
     *
     * @return | result == isOnImpassableTerrain() || getGameObject().getHandler(SharkCollisionHandler.class).intersectsWater();
     */
    public boolean canJump() {
        return isOnImpassableTerrain() || getGameObject().getHandler(SharkCollisionHandler.class).intersectsWater();
    }

    /**
     * Returns whether the shark is on impassable terrain
     */
    @Basic
    public boolean isOnImpassableTerrain() {
        return onImpassableTerrain;
    }

    /**
     * Variable registering if the shark is on impassable terrain
     */
    private boolean onImpassableTerrain;

    /**
     * Returns the maximum velocity
     *
     * @return result == new Vector<>(3.0, Double.POSITIVE_INFINITY);
     */
    @Override
    public Vector<Double> getMaxVelocity() {
        return new Vector<>(3.0, Double.POSITIVE_INFINITY);
    }

    /**
     * Returns the minimum velocity
     *
     * @return | result == Vector.ACTUAL_ZERO;
     */
    @Override
    public Vector<Double> getMinVelocity() {
        return Vector.ACTUAL_ZERO;
    }

    /**
     * Event when movement is blocked horizontally
     *
     * @param delta The difference between the current position and the new position that could not be applied
     *
     * @effect | setVelocity(getVelocity().setX(0.0));
     * @effect | setAcceleration(getAcceleration().setX(0.0));
     */
    @Override
    public void onBounceX(Vector<Double> delta) {
        setVelocity(getVelocity().setX(0.0));
        setAcceleration(getAcceleration().setX(0.0));
    }

    /**
     * Event when movement is blocked vertically
     *
     * @param delta The difference between the current position and the new position that could not be applied
     *
     * @effect | setVelocity(getVelocity().setY(0.0));
     * @effect | setAcceleration(getAcceleration().setY(0.0));
     */
    @Override
    public void onBounceY(Vector<Double> delta) {
        setVelocity(getVelocity().setY(0.0));
        setAcceleration(getAcceleration().setY(0.0));
    }
}
