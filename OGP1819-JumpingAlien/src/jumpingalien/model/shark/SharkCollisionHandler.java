package jumpingalien.model.shark;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Model;
import jumpingalien.model.Mazub;
import jumpingalien.model.Shark;
import jumpingalien.model.Slime;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.gameobject.ICollidable;
import jumpingalien.model.handler.MovementHandler;
import jumpingalien.model.handler.WorldCollisionHandler;
import jumpingalien.util.GameTimer;
import jumpingalien.model.world.GeologicalFeature;
import jumpingalien.model.world.Tile;

/**
 * A class for handling the collision of a shark
 *
 * @invar | getGameObject() instanceof Shark
 */
public class SharkCollisionHandler extends WorldCollisionHandler {

    /**
     * Creates a new Shark Collision Handler
     *
     * @param shark The shark
     *
     * @effect | super(shark)
     * @effect | getOutOfWaterTimer().start()
     */
    public SharkCollisionHandler(Shark shark) {
        super(shark);
        outOfWaterTimer = new GameTimer(0.2, false) {
            @Override
            public void onTick() {
                getGameObject().takeDamage(6);
            }
        };
        outOfWaterTimer.start();
    }

    @Basic
    @Model
    public GameTimer getOutOfWaterTimer() {
        return outOfWaterTimer;
    }

    private GameTimer outOfWaterTimer;

    @Override
    public void update(double dt){
        super.update(dt);
        outOfWaterTimer.update(dt);
    }

    @Override
    public void onCollision(ICollidable other) {

    }

    /**
     * Event when the shark enters collision with another collidable
     *
     * @param other The other collidable
     *
     * @effect | if (other instanceof Tile && other.getGeologicalFeature() == WATER)
     *         |    outOfWaterTimer.reset();
     *         |    outOfWaterTimer.stop();
     *
     * @post   | if (other instanceof Tile && other.getGeologicalFeature() == WATER)
     *         |    new.getWaterCount() = getWaterCount() + 1
     *
     * @effect | if (other instanceof Mazub)
     *         |    getGameObject().takeDamage(50)
     *
     * @effect | if (other instanceof Slime)
     *         |    getGameObject.takeDamage(-1)
     */
    @Override
    public void onCollisionEnter(ICollidable other) {
        if (other instanceof Tile) {
            if (((Tile) other).getGeologicalFeature() == GeologicalFeature.WATER) {
                waterCount++;
                outOfWaterTimer.reset();
                outOfWaterTimer.stop();
            }
        }

        if (other instanceof Mazub) {
            getGameObject().takeDamage(50);
        }

        if (other instanceof Slime) {
            getGameObject().takeDamage(-10);
        }
    }

    /**
     * Event when the shark exits collision with another collidable
     *
     * @param other The other collidable
     *
     * @effect | if (other instanceof Tile && other.getGeologicalFeature() == WATER && getWaterCount() == 1)
     *         |    outOfWaterTimer.reset();
     *         |    outOfWaterTimer.start();
     *
     * @post   | if (other instanceof Tile && other.getGeologicalFeature() == WATER)
     *         |    new.getWaterCount() = getWaterCount() - 1
     */
    @Override
    public void onCollisionExit(ICollidable other) {
        if (other instanceof Tile) {
            if (((Tile) other).getGeologicalFeature() == GeologicalFeature.WATER) {
                waterCount--;
                if (!intersectsWater()) {
                    outOfWaterTimer.reset();
                    outOfWaterTimer.start();
                }
            }
        }
    }

    /**
     * Returns whether the shark intersects water
     *
     * @return | getWaterCount() > 0
     */
    public boolean intersectsWater() {
        return waterCount > 0;
    }

    /**
     * Returns the amount of water tiles colliding with this shark
     */
    @Model
    @Basic
    private int getWaterCount() {
        return waterCount;
    }

    /**
     * Variable registering the amount of water tiles that intersect this shark
     */
    private int waterCount;
}
