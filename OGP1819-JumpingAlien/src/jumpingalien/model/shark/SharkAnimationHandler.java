package jumpingalien.model.shark;

import jumpingalien.model.Shark;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.util.exception.InvalidSpriteException;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.handler.AnimationHandler;
import jumpingalien.util.Sprite;

/**
 * A class for handling the sprites of a Shark
 *
 * @invar | getGameObject() instanceof Shark
 */
public class SharkAnimationHandler extends AnimationHandler {
    /**
     * Creates a nea Shark Animation Handler
     *
     * @param shark    The shark
     * @param sprites  The sprites for the shark
     */
    public SharkAnimationHandler(Shark shark, Sprite... sprites) throws InvalidSpriteException {
        super(shark, sprites);
    }

    /**
     * Checks whether this shark can have the given sprites as its sprites
     *
     * @param  sprites  The sprites to check
     *
     * @return | result == super.canHaveAsSprites(sprites) && sprites.length == 3
     */
    @Override
    public boolean canHaveAsSprites(Sprite[] sprites) {
        return super.canHaveAsSprites(sprites) && sprites.length == 3;
    }

    /**
     * Returns the current sprite for the shark
     */
    @Override
    public Sprite getCurrentSprite() {
        if (getGameObject().hasHandler(SharkMovementHandler.class))
            if (getGameObject().getHandler(SharkMovementHandler.class).isResting())
                return getSprites()[0];
        switch (getGameObject().getOrientation()) {
            case Left:
                return getSprites()[1];
            case Right:
                return getSprites()[2];
            default:
                return getSprites()[0];
        }
    }
}
