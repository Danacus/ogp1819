package jumpingalien.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Model;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.plant.PlantAnimationHandler;
import jumpingalien.model.plant.PlantCollisionHandler;
import jumpingalien.model.plant.PlantMovementHandler;
import jumpingalien.model.world.Collision;
import jumpingalien.util.Direction;
import jumpingalien.util.Sprite;
import jumpingalien.util.Vector;
import jumpingalien.util.exception.GameObjectException;

/**
 * A class for plants
 *
 * @invar | hasHandler(PlantAnimationHandler.class)
 * @invar | hasHandler(PlantCollisionHandler.class)
 * @invar | hasHandler(PlantMovementHandler.class)
 */
public abstract class Plant extends GameObject {
    /**
     * Creates a new Plant
     *
     * @param sprites sprite array for the new plant.
     * @param lifeTime the amount of seconds it takes before the plant dies
     *
     * @effect | addHandler(new PlantAnimationHandler(this, sprites))
     * @effect | addHandler(new PlantCollisionHandler(this))
     * @effect | setHitPoints(1)
     * @post | new.lifeTime == lifeTime
     */
    public Plant(Sprite[] sprites, double lifeTime) throws GameObjectException {
        super();
        addHandler(new PlantAnimationHandler(this, sprites));
        addHandler(new PlantCollisionHandler(this));
        this.lifeTime = lifeTime;
    }

    /**
     * Sets the death counter for this plant
     *
     * @param deathCount The new time
     *
     * @post | getDeathCount() == deathCount
     */
    @Model
    private void setDeathCount(double deathCount) {
        this.deathCount = deathCount;
    }

    /**
     * Returns the death counter of the plant
     */
    @Basic
    public double getDeathCount() {
        return deathCount;
    }

    /**
     * A variable to store a death counter for this plant
     */
    private double deathCount = 0;

    /**
     * Returns the life time of the plant
     */
    @Basic
    @Immutable
    public double getLifeTime() {
        return lifeTime;
    }

    /**
     * A variable to store how long the plant lives
     */
    private final double lifeTime;


    /**
     * Updates the plant
     *
     * @param dt the advanced time since last tick.
     */
    @Override
    public void update(double dt){
        super.update(dt);

        deathCount += dt;
        if(deathCount >= lifeTime){
            kill();
        }
    }

    /**
     * @return | result == Collision.Category.PLANT
     */
    @Basic
    @Immutable
    @Override
    public byte getCategory() {
        return Collision.Category.PLANT;
    }

    /**
     * @return | result == Collision.Mask.PLANT
     */
    @Basic
    @Immutable
    @Override
    public byte getMask() {
        return Collision.Mask.PLANT;
    }

    /**
     * @return | result == false
     */
    @Basic
    @Immutable
    @Override
    public boolean isSolid() {
        return false;
    }
}
