package jumpingalien.model;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.world.SchoolManager;
import jumpingalien.util.DoubleEndedPriorityQueue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;


public class School {

    /**
     * Initialize the school.
     * @param world The world of this school.
     */
    public School(World world) {
        members = new DoubleEndedPriorityQueue<>(Comparator.comparingLong(Slime::getId));
        this.world = world;
    }

    /**
     * Returns the world of this school.
     */
    public World getWorld() {
        return world;
    }

    /**
     *
     * @return Returns the school manager of the world.
     *          | result == getWorld().getSchoolManager()
     */
    public SchoolManager getSchoolManager(){
        return getWorld().getSchoolManager();
    }

    /**
     * Variable registering the world of this school.
     */
    private final World world;

    /**
     * Terminate this school.
     */
    public void terminate() {
        this.terminated = true;
    }

    /**
     * Returns whether the school is terminated.
     */
    public boolean isTerminated() {
        return terminated;
    }

    /**
     * Variable registering whether the school is terminated or not.
     */
    private boolean terminated;

    /**
     * Add the given slime to the priority queue.
     * @param member The slime to add to the priority queue
     * @post | member.setSchool(this)
     */
    public void addMember(Slime member) throws IllegalArgumentException {
        if (isTerminated() || member == null || member.isTerminated() || member.getSchool() != null) {
            throw new IllegalArgumentException("The given object must not be in another school and it must no be terminated");
        }

        members.add(member);
        member.setSchool(this);
    }

    /**
     * Removes the given slime from the priority queue.
     * @throws IllegalArgumentException
     *          | if (!hasMember(member) throw new IllegalArgumentException("The given member is not in this school, so it cannot be removed!")
     */
    public void removeMember(Slime member) throws IllegalArgumentException{
        if (!hasMember(member))
            throw new IllegalArgumentException("The given member is not in this school, so it cannot be removed!");

        members.remove(member);
        member.setSchool(null);
    }

    /**
     * Returns a collection of all slimes in this school.
     */
    @Basic
    public Collection<Slime> getMembers() {
        return new ArrayList<>(members);
    }

    /**
     * Check whether the given slime is a member of this school.
     */
    @Basic
    public boolean hasMember(Slime member) {
        return members.contains(member);
    }

    /**
     * Return the member with the highest id from the priority queue.
     */
    @Basic
    public Slime getMaxMember() {
        return members.getMax();
    }

    /**
     * Return the member with the lowest id from the priority queue.
     */
    @Basic
    public Slime getMinMember() {
        return members.getMin();
    }

    /**
     * Remove the member with the highest id from the priority queue.
     */
    @Basic
    public void removeMaxMember() {
        members.removeMax();
    }

    /**
     * Remove the member with the lowest id from the priority queue.
     */
    @Basic
    public void removeMinMember() {
        members.removeMin();
    }

    /**
     * Variable registering a double ended priority queue that contains all the members of the school.
     */
    private final DoubleEndedPriorityQueue<Slime> members;
}
