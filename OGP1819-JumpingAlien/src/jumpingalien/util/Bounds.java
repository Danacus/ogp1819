package jumpingalien.util;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;

/**
 * A class to store the bounds of a collidable object
 *
 * @invar   This bounds can have its size as its size
 *        | isValidSize(getSize())
 */
@Value
public class Bounds {
    /**
     * Creates a new bounds object
     *
     * @param position The position of the bounds
     * @param size     The size of the bounds
     * @param offset   The offset tht you want to have applied on the given position and size
     *
     * @post  The position will be equal to the given position minus the given offset
     *      | new.getPosition().getX() == position.getX() - offset
     *      | new.getPosition().getY() == position.getY() - offset
     * @post  The size will be equal to the given size plus twice the given offset, twice because we
     *        have to compensate for the offset subtracted from the position.
     *      | new.getSize().getX() == size.getX() - offset
     *      | new.getSize().getY() == size.getY() - offset
     *
     * @throws IllegalArgumentException
     *        The given size is not valid for a bounds object
     *      | !isValidSize(size)
     */
    public Bounds(Vector<Integer> position, Vector<Integer> size, int offset) throws IllegalArgumentException {
        if (!isValidSize(size))
            throw new IllegalArgumentException("Invalid size for bounds!");

        this.position = position.add(new Vector<>(-offset, -offset));
        this.size = size.add(new Vector<>(offset * 2, offset * 2));
    }

    /**
     * Creates a new bounds object
     *
     * @param position The position of the bounds
     * @param size     The size of the bounds
     *
     * @effect  Will create a new bounds object with an offset of 0
     *        | this(position, size, 0)
     */
    public Bounds(Vector<Integer> position, Vector<Integer> size) throws IllegalArgumentException {
        this(position, size, 0);
    }

    /**
     * Creates a new bounds object
     *
     * @param position The position of the bounds
     * @param sprite   The spite you want to use to create the bounds
     * @param offset   The offset the you want to have applied on the given position and size
     *
     * @effect  Will create a new bounds object based on the given sprite, position and offset
     *        | this(position, new Vector<>(sprite.getWidth(), sprite.getHeight()), offset)
     */
    public Bounds(Vector<Integer> position, Sprite sprite, int offset) throws IllegalArgumentException {
        this(position, new Vector<>(sprite.getWidth(), sprite.getHeight()), offset);
    }

    /**
     * Creates a new bounds object
     *
     * @param position The position of the bounds
     * @param sprite   The spite you want to use to create the bounds
     *
     * @effect  Will create a new bounds object based on the given sprite and position with no offset
     *        | this(position, sprite, 0)
     */
    public Bounds(Vector<Integer> position, Sprite sprite) throws IllegalArgumentException {
        this(position, sprite, 0);
    }

    /**
     * Returns the x position of these bounds
     *
     * @return | getPosition().getX()
     */
    public int getX() {
        return getPosition().getX();
    }

    /**
     * Returns the y position of these bounds
     *
     * @return | getPosition().getY()
     */
    public int getY() {
        return getPosition().getY();
    }

    /**
     * Returns the width of these bounds
     *
     * @return | getSize().getX()
     */
    public int getWidth() {
        return getSize().getX();
    }

    /**
     * Returns the height of these bounds
     *
     * @return | getSize().getY()
     */
    public int getHeight() {
        return getSize().getY();
    }

    /**
     * Returns the position of these bounds
     */
    @Basic
    @Immutable
    public Vector<Integer> getPosition() {
        return position;
    }

    /**
     * Returns the size of these bounds
     */
    @Basic
    @Immutable
    public Vector<Integer> getSize() {
        return size;
    }

    /**
     * Checks whether a bounds object can have the given size as its size
     *
     * @param size The size to check
     *
     * @return   True if and only if the given size has no negative components
     *         | result == size.getX() >= 0 && size.getY() >= 0
     */
    public static boolean isValidSize(Vector<Integer> size) {
        return size.getX() >= 0 && size.getY() >= 0;
    }

    /**
     * Variable to store the position of these bounds
     */
    private final Vector<Integer> position;

    /**
     * Variable to store the size for these bounds
     */
    private final Vector<Integer> size;

    /**
     * Checks whether 2 bounds are equal
     *
     * @param obj The other bounds
     *
     * @return   If the type of the given object is not Bounds, false shall be returned
     *         | if (!(obj instanccof Bounds)) then
     *         |    return false
     * @return   True if and only if the given object's position and size are equal to this position and size
     *         | result == obj.getPosition().equals(getPosition()) && obj.getSize().equals(getPosition())
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Bounds))
            return false;
        Bounds other = (Bounds) obj;
        return other.getPosition().equals(getPosition()) && other.getSize().equals(getPosition());
    }

    /**
     * Checks whether these bounds intersect the given bounds
     *
     * @param other The other bounds
     *
     * @return | result ==     getX()               < other.getX() + other.getWidth()
     *         |            && getX() + getWidth()  > other.getX()
     *         |            && getY()               < other.getY() + other.getHeight()
     *         |            && getY() + getHeight() > other.getY();
     */
    public boolean intersects(Bounds other) {
        return     getX()               < other.getX() + other.getWidth()
                && getX() + getWidth()  > other.getX()
                && getY()               < other.getY() + other.getHeight()
                && getY() + getHeight() > other.getY();
    }
}
