package jumpingalien.util.exception;

public class GameObjectException extends RuntimeException {
    public GameObjectException(String message) {
        super(message);
    }
}
