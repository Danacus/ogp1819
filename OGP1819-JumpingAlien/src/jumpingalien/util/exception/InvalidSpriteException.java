package jumpingalien.util.exception;

public class InvalidSpriteException extends GameObjectException {
	/**
	 * Create a new invalid sprite exception involving the given message.
	 *
	 * @param  message
	 *         The message for this new illegal-position exception.
	 * @effect Do a super call to the exception class.
	 *       | super(message)
	 */
	public InvalidSpriteException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;
}
