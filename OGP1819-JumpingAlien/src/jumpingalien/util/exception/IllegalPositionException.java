package jumpingalien.util.exception;

public class IllegalPositionException extends GameObjectException {
    /**
     * Create a new illegal position exception involving the given message.
     *
     * @param  message
     *         The message for this new illegal position exception.
     * @effect Do a super call to the exception class.
     *       | super(message)
     */
    public IllegalPositionException(String message) {
        super(message);
    }


    private static final long serialVersionUID = 1L;

}
