package jumpingalien.util;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Model;

/**
 * A class used to repeat something on a fixed given interval
 *
 * @invar   This timer can have its interval as its interval
 *        | canHaveAsInterval(getInterval())
 */
public abstract class GameTimer {
    /**
     * Creates a new game timer
     *
     * @param interval   The interval on which onTick will be invoked
     * @param tickFirst  Should onTick be invoked immediately after starting the timer?
     *
     * @post  The interval of this game timer will be equal to the given interval
     *      | new.getInterval() == interval
     * @post  The tickFirst boolean will be set to the given tickFirst
     *      | new.isTickFirst() == tickFirst
     *
     * @throws IllegalArgumentException
     *         This game timer cannot have the given interval as its interval
     *       | !canHaveAsInterval(interval)
     */
    public GameTimer(double interval, boolean tickFirst) throws IllegalArgumentException {
        if (!canHaveAsInterval(interval))
            throw new IllegalArgumentException("Invalid interval! Must be a double that is greater than 0");
        this.interval = interval;
        this.tickFirst = tickFirst;
    }

    /**
     * Creates a new game timer
     *
     * @param interval  The interval on which onTick will be invoked
     *
     * @effect The other constructor will be invoked with tickFirst equal to false
     *       | this(interval, false)
     *
     * @throws IllegalArgumentException
     *         This game timer cannot have the given interval as its interval
     *       | !canHaveAsInterval(interval)
     */
    public GameTimer(double interval) throws IllegalArgumentException {
        this(interval, false);
    }

    /**
     * Starts the timer
     *
     * @effect | if (isTickFirst()) then onTick()
     * @post | new,isRunning() == true
     */
    public void start() {
        if (tickFirst)
            onTick();
        isRunning = true;
    }

    /**
     * Stops the timer
     *
     * @post | new.isRunning() == false
     */
    public void stop() {
        isRunning = false;
    }

    /**
     * Resets the timer
     *
     * @post | new.getTime() == 0
     */
    public void reset() {
        time = 0;
    }

    /**
     * Will update the timer
     *
     * @param dt The delta time
     *
     * @post   | new.getTime() == (getTime() + dt) % getInterval()
     * @effect | for each i in IntStream.range(0, ((getTime() + dt) / interval)):
     *         |    onTick()
     */
    public void update(double dt) {
        if (!isRunning)
            return;
        time += dt;

        while (getTime() >= getInterval()) {
            time -= interval;

            onTick();
        }
    }

    /**
     * Returns the current time of the timer
     */
    @Model
    @Basic
    public double getTime() {
        return time;
    }

    /**
     * Variable to store the time of this timer
     */
    private double time;

    /**
     * Returns the interval of this timer
     */
    @Basic
    @Immutable
    public double getInterval() {
        return interval;
    }

    /**
     * Checks whether this timer can have the given interval as its interval
     *
     * @param interval An interval
     * @return | !Double.isNan() && interval > 0
     */
    public boolean canHaveAsInterval(double interval) {
        return !Double.isNaN(interval) && interval > 0;
    }

    /**
     * Variable to store the interval of this timer
     */
    private final double interval;

    /**
     * Returns whether the timer is currently running
     */
    @Basic
    public boolean isRunning() {
        return isRunning;
    }

    /**
     * Variable to store whether the timer is running
     */
    private boolean isRunning;

    /**
     * Returns whether this timer will call onTick immediately when started
     */
    @Basic
    @Immutable
    public boolean isTickFirst() {
        return tickFirst;
    }

    /**
     * Variable to store whether or not onTick is invoked when calling start
     */
    private final boolean tickFirst;

    /**
     * Event invoked when the interval has passed in time
     */
    public abstract void onTick();
}
