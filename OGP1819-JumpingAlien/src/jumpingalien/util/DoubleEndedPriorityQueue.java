package jumpingalien.util;

import be.kuleuven.cs.som.annotate.Basic;

import java.util.*;

/**
 * Class used to create double ended priority queue
 *
 * @param <E> The type of the objects in the queue
 *
 * @invar   The sizes of the 2 heaps will always be equal
 *        | minPq.size() == maxPq.size()
 * @invar   An object is either part of both heaps, or it is part of neither of the heaps.
 *        | for each possible object o:
 *        |     (minPq.contains(o) && maxPq.contains(o)) || (!minPq.contains(o) && !maxPq.contains(o))
 */
public class DoubleEndedPriorityQueue<E> implements Collection<E> {

    public DoubleEndedPriorityQueue() {
        minPq = new PriorityQueue<>();
        maxPq = new PriorityQueue<>(Collections.reverseOrder());
    }

    public DoubleEndedPriorityQueue(Comparator<E> comparator) {
        minPq = new PriorityQueue<>(comparator);
        maxPq = new PriorityQueue<>(comparator.reversed());
    }

    private final PriorityQueue<E> minPq;
    private final PriorityQueue<E> maxPq;

    @Override
    @Basic
    public int size() {
        return minPq.size();
    }

    @Override
    @Basic
    public boolean isEmpty() {
        return minPq.isEmpty();
    }

    @Override
    @Basic
    public boolean contains(Object o) {
        return minPq.contains(o);
    }

    @Override
    public Iterator<E> iterator() {
        return minPq.iterator();
    }

    @Override
    public Object[] toArray() {
        return minPq.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return minPq.toArray(ts);
    }

    @Override
    public boolean add(E e) {
        return minPq.add(e) && maxPq.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return minPq.remove(o) && maxPq.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return minPq.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        return minPq.addAll(collection) && maxPq.addAll(collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return minPq.removeAll(collection) && maxPq.removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return minPq.retainAll(collection) && maxPq.retainAll(collection);
    }

    @Override
    public void clear() {
        minPq.clear();
        maxPq.clear();
    }

    @Basic
    public E getMin() {
        return minPq.peek();
    }

    @Basic
    public E getMax() {
        return maxPq.peek();
    }

    @Basic
    public E removeMin() {
        E value = minPq.peek();
        maxPq.remove(minPq.poll());
        return value;
    }

    @Basic
    public E removeMax() {
        E value = maxPq.peek();
        minPq.remove(maxPq.poll());
        return value;
    }
}
