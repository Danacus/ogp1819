package jumpingalien.util;

import be.kuleuven.cs.som.annotate.Basic;
import jumpingalien.model.gameobject.GameObject;

public class GeologicalEffect extends GameTimer {
    public GeologicalEffect(GameObject gameObject, double interval, int damage, boolean immediateDamage) {
        super(interval, immediateDamage);
        this.gameObject = gameObject;
        this.damage = damage;
    }

    private int damage;
    private GameObject gameObject;
    private int tileCount;

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    public boolean isHit() {
        return hit;
    }

    private boolean hit;

    /**
     * @post | new.getTileCount() == getTileCount() + 1
     */
    public void addTile() { tileCount++; }

    /**
     * @post | new.getTileCount() == getTileCount() - 1
     */
    public void removeTile() { tileCount--; }

    @Basic
    public int getTileCount() {
        return tileCount;
    }
    @Override
    public void update(double dt) {
        if (!hit) {
             reset();
             hit = true;
             start();
        }

        super.update(dt);
    }

    /**
     * @effect | gameObject.takeDamage(damage)
     */
    @Override
    public void onTick() {
        gameObject.takeDamage(damage);
    }
}
