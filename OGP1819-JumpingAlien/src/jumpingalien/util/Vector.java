package jumpingalien.util;

import be.kuleuven.cs.som.annotate.*;


@Value
@SuppressWarnings("unchecked")
public class Vector<T extends Number> {

	/**
	 * Initialize this new vector with given x and y.
	 *
	 * @param x The x for this new vector.
	 * @param y The y for this new vector.
	 * @post The x of this new vector is equal to the given x.
	 *      | new.getX() == x
	 * @post The y of this new vector is equal to the given y.
	 *      | new.getY() == y
	 */
	public Vector(T x, T y) {
	    if (!(x.getClass() == Integer.class || x.getClass() == Double.class))
	    	throw new IllegalArgumentException("Invalid type for Vector, must be an Integer or a Double");
		this.x = x;
		this.y = y;
	}

	/**
	 * Return the x of this vector.
	 */
	@Basic
    @Raw
	@Immutable
	public T getX() {
		return this.x;
	}

    /**
     * Create a new vector with the given x value.
     *
     * @param x The x for this new vector.
     * @return The x of this new vector is equal to the given x.
     *      | result.getX() == x
     */
    public Vector<T> setX(T x) {
        return new Vector<>(x, getY());
    }

	/**
	 * Variable registering the x of this vector.
	 */
	protected final T x;


	/**
	 * Return the y of this vector.
	 */
	@Basic
    @Raw
	@Immutable
	public T getY() {
		return this.y;
	}

	/**
	 * Create a new vector with the given y value.
	 *
	 * @param y The y for this new vector.
	 * @return The y of the new vector is equal to the given y.
	 *      | result.getY() == y
	 */
	public Vector<T> setY(T y) {
	    return new Vector<>(getX(), y);
	}

	/**
	 * Variable registering the y of this vector.
	 */
	protected final T y;

    /**
     * Computes the sum of this vector and the given vector
     *
     * @param other The other vector to add
     * @return The x component of the resulting vector is equal to the sum
     *         of this x coordinate and the other x coordinate. The y
     *         component of the resulting vector is equal to the sum of this
     *         y coordinate and the other y coordinate.
     *       | result.getX() == this.getX() + other.getX()
     *       | result.getY() == this.getY() + other.getY()
     */
	public Vector<T> add(Vector<T> other) throws IllegalArgumentException {
		if (other == null)
			throw new IllegalArgumentException("Not a vector");
		if (getX() instanceof Double) {
			return new Vector<>(
					(T) (Double) (this.getX().doubleValue() + other.getX().doubleValue()),
					(T) (Double) (this.getY().doubleValue() + other.getY().doubleValue())
			);
		} else if (getX() instanceof Integer) {
			return new Vector<>(
					(T) (Integer) (this.getX().intValue() + other.getX().intValue()),
					(T) (Integer) (this.getY().intValue() + other.getY().intValue())
			);
		} else {
			throw new IllegalArgumentException("Invalid Vector type");
		}
	}

	public Vector<T> scl(T factor) throws IllegalArgumentException {
		if (getX() instanceof Integer && factor instanceof Integer) {
			return new Vector<>(
					(T) (Integer) (this.getX().intValue() * factor.intValue()),
					(T) (Integer) (this.getY().intValue() * factor.intValue())
			);
		} else if (getX() instanceof  Double && factor instanceof Double) {
			return new Vector<>(
					(T) (Double) (this.getX().doubleValue() * factor.doubleValue()),
					(T) (Double) (this.getY().doubleValue() * factor.doubleValue())
			);
		} else {
			throw new IllegalArgumentException("Invalid Vector or factor type");
		}
	}

	/**
	 * Creates a new vector based on the current one,
	 * with the coordinates converted from pixel space to actual space.
	 *
	 * @return a new vector with the coordinates converted from pixel space to actual space.
	 * | result.x == this.getX() / Vector.getPPM()
	 * | result.y == this.getY() / Vector.getPPM()
	 */
	public Vector<Double> pixelToActual() throws IllegalArgumentException {
		if (!(getX() instanceof Integer)) throw new IllegalArgumentException("Invalid Vector type: this Vector must have Integer as type");
		double newX = (double)getX().intValue() / getPPM();
		double newY = (double)getY().intValue() / getPPM();
		return new Vector<>(newX, newY);
	}

	/**
	 * Creates a new vector based on the current one,
	 * with the coordinates converted from actual space to pixel space.
	 *
	 * @return a new vector with the coordinates converted from actual space to pixel space.
	 * | result.x == this.getX() / Vector.getPPM()
	 * | result.y == this.getY() / Vector.getPPM()
	 */
	public Vector<Integer> actualToPixel() throws IllegalArgumentException {
		if (!(getX() instanceof Double)) throw new IllegalArgumentException("Invalid Vector type: this Vector must have Double as type");
		int newX = (int)Math.round(getX().doubleValue() * getPPM());
		int newY = (int)Math.round(getY().doubleValue() * getPPM());
		return new Vector<>(newX, newY);
	}

	public T distanceTo(Vector other) throws IllegalArgumentException {
		if (other == null)
			throw new IllegalArgumentException("Vector cannot be null");
		if (getX() instanceof Integer && other.getX() instanceof Integer) {
			return (T)Integer.valueOf((int)Math.sqrt(
					Math.pow((double)(getX().intValue() - other.getX().intValue()), 2) +
					Math.pow((double)(getY().intValue() - other.getY().intValue()), 2)
			));
		} else if (getX() instanceof Double && other.getX() instanceof Double) {
			return (T)Double.valueOf((int)Math.sqrt(
					Math.pow(getX().doubleValue() - other.getX().doubleValue(), 2) +
					Math.pow(getY().doubleValue() - other.getY().doubleValue(), 2)
			));
		} else {
			throw new IllegalArgumentException("Incompatible Vector types");
		}
	}

	public double magnitude() {
		if (getX() instanceof Integer) {
			return Math.sqrt(
					Math.pow((double)(getX().intValue()), 2) +
					Math.pow((double)(getY().intValue()), 2)
			);
		} else {
			return Math.sqrt(
					Math.pow(getX().doubleValue(), 2) +
					Math.pow(getY().doubleValue(), 2)
			);
		}
	}

	/**
	 * Variable registering the amount of pixels per meter.
	 */
	private static final int PPM = 100;

	/**
	 * @return The amount of pixels per meter.
	 * | result == 100;
	 */
	@Immutable
	public static int getPPM() {
		return PPM;
	}

	/**
	 * Returns a new Vector with a magnitude of zero
	 */
	public static Vector<Integer> PIXEL_ZERO = new Vector<>(0, 0);
	public static Vector<Double> ACTUAL_ZERO = new Vector<>(0.0, 0.0);

	/**
	 * Returns a new Vector with a magnitude of infinity
	 */
	public static Vector<Integer> PIXEL_INFINITY = new Vector<>(Integer.MAX_VALUE, Integer.MAX_VALUE);
	public static Vector<Double> ACTUAL_INFINITY = new Vector<>(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);

	/**
	 * Returns an array with two Ts representing this vector.
	 *
	 * @return an array with two Ts
	 *      | result[0] == getX()
	 *      | result[1] == getY()
	 */
	public double[] asDoubleArray() {
		return new double[]{(double) getX(), (double) getY()};
	}

	/**
	 * Returns an array with two integers representing this vector.
	 *
	 * @return an array with two integer
	 *      | result[0] == (int)getX()
	 *      | result[1] == (int)getY()
	 */
	public int[] asIntegerArray() {
		return new int[]{(int) getX(), (int) getY()};
	}

	/**
	 * Returns a new Vector from the given array
	 *
	 * @param arr An array with 2 Ts representing the x and y values
     *
	 * @return A new Vector
	 * 		| result.getX() == arr[0]
	 * 		| result.getY() == arr[1]
	 */
	public static Vector<Double> fromDoubleArray(double[] arr) {
		if (arr == null || arr.length != 2)
			throw new IllegalArgumentException("Cannot create Vector: Invalid input array");
		return new Vector<>(arr[0], arr[1]);
	}

	/**
	 * Returns a new Vector from the given array
	 *
	 * @param arr An array with 2 integers representing the x and y values
	 *
	 * @return A new Vector
	 * 		| result.getX() == arr[0]
	 * 		| result.getY() == arr[1]
	 */
	public static Vector<Integer> fromIntegerArray(int[] arr) {
		if (arr == null || arr.length != 2)
			throw new IllegalArgumentException("Cannot create Vector: Invalid input array");
		return new Vector<>(arr[0], arr[1]);
	}

	/**
	 * Computes the vector based on the current vector, but
	 * with the absolute value of each component
	 *
	 * @return The vector with absolute values
	 * 		| result.getX() == Math.abs(this.getX())
	 * 		| result.getY() == Math.abs(this.getY())
	 */
	public Vector<T> abs() {
		if (getX() instanceof Double) {
			return new Vector<>(
					(T) (Double) Math.abs(getX().doubleValue()),
					(T) (Double) Math.abs(getY().doubleValue())
			);
		} else if (getX() instanceof Integer) {
			return new Vector<>(
					(T) (Integer) Math.abs(getX().intValue()),
					(T) (Integer) Math.abs(getY().intValue())
			);
		} else {
			throw new RuntimeException("Am impossible exception occurred, not sure how we got here");
		}
	}

    /**
     * Returns a textual representation of this vector.
     *
     * @return A string consisting of the coordinates of this vector.
     *      | result.equals("(" + getX() + ", " + getY() + ")")
     */
    @Override
    public String toString() {
        return "(" + getX() + ", " + getY() + ")";
    }

    /**
     * Checks whether the given object is equal to this vector.
     *
     * @param other The other object
     * @return True if and only if the other object is not null,
     *         the class of the other object is Vector and the
     *         coordinates of the other vector are equal to the
     *         coordinates of this vector.
     */
    @Override
    public boolean equals(Object other) {
	    if (other == null)
	        return false;
	    if (this.getClass() != other.getClass())
	        return false;

	    return (this.getX().equals(((Vector) other).getX()))
                && (this.getY().equals(((Vector) other).getY()));
    }

    /**
     * Returns the hash code for this vector
     */
    @Override
    public int hashCode() {
        return getX().hashCode() + getY().hashCode();
    }
}
