package jumpingalien.util;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * A map that hashes classes in a hierarchy tree at their top most superclass that extends the root class
 * Useful when storing objects of multiple classes with more complex hierarchy trees, while
 * still providing near constant time get an object.
 *
 * @param <E> The common type of all classes in the tree
 */
public class ClassTreeMap<E> extends LinkedHashMap<Class<? extends E>, E> {

    /**
     * Creates a new ClassTreeMap
     *
     * @param rootClass The root of your hierarchy tree, this should be an abstract class that provides
     *                  all basic functionality for all the objects in your tree..
     *                  Every class in the tree must extend this root class.
     *
     * @effect | super()
     * @post | new.rootClass == rootClass
     */
    public ClassTreeMap(Class<E> rootClass) {
        super();
        this.rootClass = rootClass;
    }

    private final Class<E> rootClass;

    /**
     * Gets the root class of the hierarchy tree
     */
    @Basic
    @Immutable
    public Class<E> getRootClass() {
        return rootClass;
    }

    /**
     * Puts the given Handler in the Map at  at the given hash.
     *
     * @param key The class name at which the given value should be hashed
     * @param value The object to be added to the map
     *
     * @pre The immediate superclass of the given class name (hash), must be the root class
     *      | key.getSuperClass() == this.getRootClass()
     * @post The given object will be hashed at the given hash in the HashMap
     *      | new.get(key) == value
     *      | new.containsKey(key) == true
     */
    @Override
    public E put(Class<? extends E> key, E value) {
        assert key.getSuperclass() == this.rootClass;
        return super.put(key, value);
    }

    /**
     * Adds an object to the Map
     *
     * @param value The object to be added to the map
     * @param <T> The type of the given object, must be a class type that extends the root class
     *
     * @effect The object will be added to the Map
     *      | add(value.getClass(), value)
     */
    public <T extends E> void add(T value) {
        add((Class<? extends E>)value.getClass(), value);
    }

    /**
     * Adds an object to the Map
     *
     * @param key A class name, the proposed hash
     * @param value The object to be added to the Map
     * @param <T> The type of the given object, must be a class type that extends the root class
     *
     * @effect If the immediate superclass of the given class name (hash) is the root class,
     *         the given object will be put in the HashMap with the given class name as hash.
     *         Otherwise, this method will be called recursively with the superclass of the given
     *         class name as class name (this is by definition a class name that extends the root class
     *         since the current class name extends the root class, but it's superclass is not the root class,
     *         so it's a valid parameter). The same object to be added to the map will be passed as second parameter.
     *       | if (key.getSuperclass() == this.getRootClass()) then
     *       |      setHandler(key, value)
     *       | else
     *       |      addHandler(key.getSuperclass(), value)
     */
    private  <T extends E> void add(Class<? extends E> key, T value) {
        if (key.getSuperclass() == this.getRootClass())
            put(key, value);
        else
            add((Class<? extends E>)key.getSuperclass(), value);
    }

    /**
     * Gets the object of a given class name
     *
     * @param key The class name of the object you would like to get
     *
     * @return Returns the object of the requested type. This method will recursively call itself with the superclass
     *         of the given class name until the top most class that immediately extends the root class is reached.
     *         If the requested type is not found, null will be returned.
     *       | if (handlerClass.getSuperclass() == Handler.class) then
     *       |      result == super.get(key)
     *       | else
     *       |      result == get(key.getSuperclass())
     */
    @Override
    public E get(Object key) {
        try {
            Class<? extends E> className = (Class<? extends E>) key;
            if (className.getSuperclass() == this.getRootClass())
                return className.cast(super.get(className));
            else
                return className.cast(get(className.getSuperclass()));
        } catch (ClassCastException e) {
            return null;
        }
    }

    /**
     * Checks whether there is an object with the given class name as type in the Map.
     *
     * @param key The class name of the object
     * @return True if and only if get(key) is not equal to null.
     *       | result == get(key) != null
     */
    @Override
    public boolean containsKey(Object key) {
        return get(key) != null;
    }
}
