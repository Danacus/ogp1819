package jumpingalien.facade;

import jumpingalien.model.*;
import jumpingalien.util.exception.GameObjectException;
import jumpingalien.util.exception.IllegalPositionException;
import jumpingalien.model.gameobject.GameObject;
import jumpingalien.model.gameobject.OrientationState;
import jumpingalien.model.handler.AnimationHandler;
import jumpingalien.model.handler.MovementHandler;
import jumpingalien.model.mazub.*;
import jumpingalien.model.world.SchoolManager;
import jumpingalien.util.*;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public class Facade implements IFacade {

	@Override
	public boolean isTeamSolution() {
		return true;
	}

	@Override
	public boolean isLateTeamSplit() {
		return false;
	}

	@Override
	public boolean hasImplementedWorldWindow() {
		return true;
	}

	@Override
	public Mazub createMazub(int pixelLeftX, int pixelBottomY, Sprite... sprites) throws ModelException {
		try {
			return new Mazub(pixelLeftX, pixelBottomY, sprites);
		}catch (GameObjectException e){
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public double[] getActualPosition(Mazub alien) throws ModelException {
	    return alien.getHandler(MovementHandler.class).getPosition().asDoubleArray();
	}

	@Override
	public void changeActualPosition(Mazub alien, double[] newPosition) throws ModelException {
		try {
			alien.getHandler(MovementHandler.class).setPosition(Vector.fromDoubleArray(newPosition));
		} catch (IllegalPositionException | IllegalArgumentException e) {
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public void changeActualPosition(Object gameObject, double[] newPosition) throws ModelException {
		try {
			((GameObject) gameObject).getHandler(MovementHandler.class).setPosition(Vector.fromDoubleArray(newPosition));
		} catch (IllegalPositionException e) {
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public int[] getPixelPosition(Mazub alien) throws ModelException {
	    return alien.getHandler(MovementHandler.class).getPixelPosition().asIntegerArray();
	}

	@Override
	public int getOrientation(Mazub alien) throws ModelException {
		return 	alien.getOrientation().getIndex();
	}

	@Override
	public double[] getVelocity(Mazub alien) throws ModelException {
	    return alien.getHandler(MovementHandler.class).getVelocity().asDoubleArray();
	}

	@Override
	public double[] getAcceleration(Mazub alien) throws ModelException {
		return alien.getHandler(MovementHandler.class).getAcceleration().asDoubleArray();
	}

	@Override
	public Sprite[] getSprites(Mazub alien) throws ModelException, ShouldNotImplementException {
		return alien.getHandler(AnimationHandler.class).getSprites();
	}

	@Override
	public Sprite getCurrentSprite(Mazub alien) throws ModelException {
		return alien.getHandler(MazubAnimationHandler.class).getCurrentSprite();
	}

	@Override
	public boolean isMoving(Mazub alien) throws ModelException {
	    return alien.getOrientation() != OrientationState.Front;
	}

	@Override
	public void startMoveLeft(Mazub alien) throws ModelException {
		if (isMoving(alien) || isDeadGameObject(alien))
			throw new ModelException("Already moving or dead!");
		alien.getHandler(MazubAnimationHandler.class).setSpriteInd((((alien.getHandler(AnimationHandler.class).getSprites().length-1)-8)/2)+9);
		alien.getHandler(MazubInputHandler.class).startMoveLeft();
	}

	@Override
	public void startMoveRight(Mazub alien) throws ModelException {
		if (isMoving(alien) || isDeadGameObject(alien))
			throw new ModelException("Already moving or dead!");
		alien.getHandler(MazubAnimationHandler.class).setSpriteInd(8);
	    alien.getHandler(MazubInputHandler.class).startMoveRight();
	}

	@Override
	public void endMove(Mazub alien) throws ModelException {
		if (!isMoving(alien))
			throw new ModelException("Not moving!");
	    alien.getHandler(MazubInputHandler.class).endMove();
	    alien.getHandler(MazubAnimationHandler.class).resetAnimation();
	}

	@Override
	public boolean isJumping(Mazub alien) throws ModelException {
	    return alien.getHandler(MazubMovementHandler.class).isJumping();
	}

	@Override
	public void startJump(Mazub alien) throws ModelException {
		alien.getHandler(MazubInputHandler.class).startJump();
	}

	@Override
	public void endJump(Mazub alien) throws ModelException {
		alien.getHandler(MazubInputHandler.class).endJump();
		alien.getHandler(MazubAnimationHandler.class).resetAnimation();
	}

	@Override
	public boolean isDucking(Mazub alien) throws ModelException {
		return alien.getHandler(MazubMovementHandler.class).isDucking();
	}

	@Override
	public void startDuck(Mazub alien) throws ModelException {
		alien.getHandler(MazubInputHandler.class).startDuck();
		alien.getHandler(MazubAnimationHandler.class).resetAnimation();
	}

	@Override
	public void endDuck(Mazub alien) throws ModelException {
		alien.getHandler(MazubInputHandler.class).endDuck();
		alien.getHandler(MazubAnimationHandler.class).resetAnimation();
	}

	@Override
	public World createWorld(int tileSize, int nbTilesX, int nbTilesY, int[] targetTileCoordinate, int visibleWindowWidth, int visibleWindowHeight, int... geologicalFeatures) throws ModelException {
		try {
			return new World(Math.abs(tileSize), Math.abs(nbTilesX), Math.abs(nbTilesY), targetTileCoordinate, Math.abs(visibleWindowWidth), Math.abs(visibleWindowHeight), geologicalFeatures);
		} catch (IllegalArgumentException e) {
			throw new ModelException(e);
		}
	}

	@Override
	public void terminateGameObject(Object gameObject) throws ModelException {
		((GameObject) gameObject).terminate();
	}

	@Override
	public boolean isTerminatedGameObject(Object gameObject) throws ModelException {
		return ((GameObject) gameObject).isTerminated();
	}

	@Override
	public boolean isDeadGameObject(Object gameObject) throws ModelException {
		return ((GameObject) gameObject).isDead();
	}

	@Override
	public double[] getActualPosition(Object gameObject) throws ModelException {
		return ((GameObject) gameObject).getHandler(MovementHandler.class).getPosition().asDoubleArray();
	}

	@Override
	public int[] getPixelPosition(Object gameObject) throws ModelException {
		return ((GameObject) gameObject).getHandler(MovementHandler.class).getPixelPosition().asIntegerArray();
	}

	@Override
	public int getOrientation(Object gameObject) throws ModelException {
	    return ((GameObject) gameObject).getOrientation().getIndex();
	}

	@Override
	public double[] getVelocity(Object gameObject) throws ModelException {
		return ((GameObject) gameObject).getHandler(MovementHandler.class).getVelocity().asDoubleArray();
	}

    @Override
    public double[] getAcceleration(Object gameObject) throws ModelException {
        return ((GameObject) gameObject).getHandler(MovementHandler.class).getAcceleration().asDoubleArray();
    }

    @Override
	public World getWorld(Object object) throws ModelException {
		if (object instanceof GameObject)
			return ((GameObject) object).getWorld();
		else if (object instanceof School)
			return ((School) object).getWorld();
		return null;
	}

	@Override
	public int getHitPoints(Object object) throws ModelException {
		return ((GameObject) object).getHitPoints();
	}

	@Override
	public Sprite[] getSprites(Object gameObject) throws ModelException {
		return ((GameObject)gameObject).getHandler(AnimationHandler.class).getSprites();
	}

	@Override
	public Sprite getCurrentSprite(Object gameObject) throws ModelException {
		return ((GameObject)gameObject).getHandler(AnimationHandler.class).getCurrentSprite();
	}

	@Override
	public void advanceTime(Object gameObject, double dt) throws ModelException {
		try {
			if (gameObject instanceof GameObject)
				((GameObject) gameObject).update(dt);
		} catch (IllegalArgumentException e) {
			throw new ModelException(e.getMessage());
		}
	}

    @Override
    public Sneezewort createSneezewort(int pixelLeftX, int pixelBottomY, Sprite... sprites) throws ModelException {
		try {
			return new Sneezewort(pixelLeftX, pixelBottomY, sprites);
		}catch (GameObjectException e){
			throw new ModelException(e.getMessage());
		}
    }

    @Override
    public Skullcab createSkullcab(int pixelLeftX, int pixelBottomY, Sprite... sprites) throws ModelException {
		try {
			return new Skullcab(pixelLeftX, pixelBottomY, sprites);
		} catch (GameObjectException e) {
			throw new ModelException(e.getMessage());
		}
    }

    @Override
    public Slime createSlime(long id, int pixelLeftX, int pixelBottomY, School school, Sprite... sprites) throws ModelException {
		try {
			return new Slime(id, pixelLeftX, pixelBottomY, school, sprites);
		}catch (GameObjectException e){
			throw new ModelException("Invalid slime ID!");
		}
	}

    @Override
    public long getIdentification(Slime slime) throws ModelException {
        return slime.getId();
    }

	@Override
	public void cleanAllSlimeIds() {
		SchoolManager.cleanAllId();
	}

	@Override
    public School createSchool(World world) throws ModelException {
		School school = new School(world);
		if (world != null)
			try {
				world.getSchoolManager().addSchool(school);
			}catch(IllegalArgumentException e){
				throw new ModelException(e.getMessage());
			}
		return school;
    }

	@Override
	public void terminateSchool(School school) throws ModelException {
		school.terminate();
	}

	@Override
    public boolean hasAsSlime(School school, Slime slime) throws ModelException {
        return school.hasMember(slime);
    }

    @Override
    public Collection<? extends Slime> getAllSlimes(School school) {
        return school.getMembers();
    }

    @Override
    public void addAsSlime(School school, Slime slime) throws ModelException {
		try {
			school.addMember(slime);
		}catch (IllegalArgumentException e){
			throw new ModelException(e.getMessage());
		}
    }

    @Override
    public void removeAsSlime(School school, Slime slime) throws ModelException {
		try {
			school.removeMember(slime);
		}catch (IllegalArgumentException e){
			throw new ModelException(e.getMessage());
		}

    }

    @Override
    public void switchSchool(School newSchool, Slime slime) throws ModelException {
        try {
			slime.switchSchool(newSchool);
		} catch (IllegalArgumentException | GameObjectException e) {
        	throw new ModelException(e.getMessage());
		}
    }

    @Override
    public School getSchool(Slime slime) throws ModelException {
        return slime.getSchool();
    }

    @Override
    public Shark createShark(int pixelLeftX, int pixelBottomY, Sprite... sprites) throws ModelException {
		try {
			return new Shark(pixelLeftX, pixelBottomY, sprites);
		} catch (GameObjectException e) {
			throw new ModelException(e.getMessage());
		}
    }

    @Override
	public void advanceWorldTime(World world, double dt) {
		world.update(dt);
	}

	@Override
	public Set<School> getAllSchools(World world) throws ModelException {
		return world.getSchoolManager().getSchools();
	}

	@Override
	public boolean didPlayerWin(World world) throws ModelException {
		if(world.getMazub() == null) return false;
		return world.isGameOver() && !world.getMazub().isDead();
	}

	@Override
	public boolean isGameOver(World world) throws ModelException {
		return world.isGameOver();
	}

	@Override
	public void startGame(World world) throws ModelException {
		world.startGame();
	}

	@Override
	public void setTargetTileCoordinate(World world, int[] tileCoordinate) throws ModelException {
		world.setTargetTileIndex(Vector.fromIntegerArray(tileCoordinate));
	}

	@Override
	public int[] getTargetTileCoordinate(World world) throws ModelException {
		if (world.getTargetTile() == null)
			throw new ModelException("Target tile is not found");
		return world.getTargetTile().getCoordinates().asIntegerArray();
	}

	@Override
	public void removeGameObject(Object object, World world) throws ModelException {
		world.removeGameObject((GameObject)object);
	}

	@Override
	public void addGameObject(Object object, World world) throws ModelException {
		try {
			world.addGameObject((GameObject) object);
		} catch (IllegalArgumentException e) {
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public Mazub getMazub(World world) throws ModelException {
		return world.getMazub();
	}

	@Override
	public Set<Object> getAllGameObjects(World world) throws ModelException {
		return world.getGameObjects().stream().map(gameObject -> (Object)gameObject).collect(Collectors.toSet());
	}

	@Override
	public boolean hasAsGameObject(Object object, World world) throws ModelException {
		return world.hasAsGameObject((GameObject)object);
	}

	@Override
	public int[] getVisibleWindowPosition(World world) throws ModelException {
		return world.getWindowManager().getVisibleWindowPosition().asIntegerArray();
	}

	@Override
	public int[] getVisibleWindowDimension(World world) throws ModelException {
		return world.getWindowManager().getVisibleWindow().asIntegerArray();
	}

	@Override
	public void setGeologicalFeature(World world, int pixelX, int pixelY, int geologicalFeature) throws ModelException {
		world.setGeologicalFeature(new Vector<>(pixelX, pixelY), geologicalFeature);
	}

	@Override
	public int getGeologicalFeature(World world, int pixelX, int pixelY) throws ModelException {
		return world.getGeologicalFeature(new Vector<>(pixelX, pixelY)).getValue();
	}

	@Override
	public int getTileLength(World world) throws ModelException {
		return world.getTileSize();
	}

	@Override
	public int[] getSizeInPixels(World world) throws ModelException {
		return world.getWorldPixelDimensions().asIntegerArray();
	}

	@Override
	public void terminateWorld(World world) throws ModelException {
		world.terminate();
	}

}
