package jumpingalien.tests;

import jumpingalien.util.ClassTreeMap;
import jumpingalien.util.DoubleEndedPriorityQueue;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UtilTest {
    @Test
    public void DoubleEndedPriorityQueue_FullTest() {
        DoubleEndedPriorityQueue<Integer> DEPQ = new DoubleEndedPriorityQueue<>(Comparator.comparingInt(integer -> integer));
        DEPQ.add(5);
        DEPQ.add(2);
        DEPQ.add(12);
        DEPQ.add(13);
        DEPQ.add(1);
        DEPQ.add(9);
        DEPQ.add(15);
        DEPQ.add(12);
        DEPQ.add(0);
        assertEquals(0, DEPQ.getMin());
        assertEquals(15, DEPQ.removeMax());
        assertEquals(8, DEPQ.size());
        assertEquals(0, DEPQ.removeMin());
        assertEquals(13, DEPQ.getMax());
        assertEquals(7, DEPQ.size());
    }

    @Test
    public void ClassTreeMap_LegalCases() {
        ClassTreeMap<Number> CTM = new ClassTreeMap<>(Number.class);
        assertEquals(Number.class, CTM.getRootClass());
        CTM.add(5);
        assertEquals(5, CTM.get(Integer.class));
        CTM.put(Float.class, 0.5f);
        assertEquals(0.5f, CTM.get(Float.class));
        assertTrue(CTM.containsKey(Integer.class));
        assertNull(CTM.get(Double.class));
    }
}
