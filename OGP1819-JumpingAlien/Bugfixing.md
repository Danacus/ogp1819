# Bugfixing

## Tests

- EatingSingleSkullcab  
    TODO, geen zin  
    Timer nodig, blijven eten om de 0.6 sec, zeer vervelende test  
- MazubLandingOnImpassableTerrain  
    NO FIX  
    Waarschijnlijk fout, verwacht dat mazub land tussen 0.49 en 0.51,  
    tegenspraak met opdracht die zegt dat bovenste laag pixels mag overlappen.  
    In praktijk: 0.485, te laag, maar aanpassingen aan collision breken spawning  
- Sharks   
    TODO   
    Veel bugs, no fucking clue waarom  
- SlimeBouncingAgainstMazub  
    TODO  
- SimpleScenario  
    Heeft EatingSingleSkullcab nodig, werkt als alle andere tests werken  
- createShark_LegalCase  
    TODO  
    Bug met initiele sprite  
- MazubDucking  
    TODO  
    Vreemde bug, is Mazub al op de grond?   
- endMove_LegalCase  
    TODO  
    Orientation bepaald movement, maar ook sprites. Mazub draait pas naar voren na enkele seconden.  
    Orientation veranderd onmiddelijk bij endMove --> foute orientation voor de test  
- MazubPartiallyOutsideBoundaries  
    TODO, Idk  
- LoosingScenario  
    TODO  
- MazubMovingAndJumping  
    TODO   
    Acceleration moet op 0 gezet worden als max velocity bereikt, is dit al niet het geval?  

